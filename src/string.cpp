#include <whatwg/url/string.hpp>

#include <whatwg/url/character_encoding.hpp>

#include <ostream>

namespace whatwg::url {

void
replace_all(byte_sequence_t& s, byte_t const c1, byte_t const c2)
{
    auto i = s.find_first_of(c1);
    while (i != s.npos) {
        s[i] = c2;

        i = s.find_first_of(c1, i);
    }
}

std::vector<byte_sequence_view_t>
split(byte_sequence_view_t const s, byte_t const sep)
{
    std::vector<byte_sequence_view_t> result;

    if (s.empty()) return result;

    size_t from = 0;

    while (true) {
        auto const to = s.find(sep, from);
        if (to == s.npos) {
            result.push_back(s.substr(from));
            break;
        }
        else {
            result.push_back(s.substr(from, to - from));
            from = to + 1;
        }
    }

    return result;
}

// LCOV_EXCL_START
std::ostream&
operator<<(std::ostream& os, string_view_t const s)
{
    os << encode_utf8(s);

    return os;
}
// LCOV_EXCL_STOP

} // namespace whatwg::url
