#include <whatwg/url/url_search_params.hpp>

#include <whatwg/url/character_encoding.hpp>
#include <whatwg/url/url.hpp>

#include <algorithm>

namespace whatwg::url {

template<typename It>
static auto
find(It const begin, It const end, byte_sequence_view_t const name)
{
    return std::find_if(begin, end,
        [&](auto const& pair) {
            return pair.first == name;
        });
}

url_search_params::url_search_params() = default;

url_search_params::
url_search_params(byte_sequence_view_t init)
{
    if (!init.empty() && starts_with(init, '?')) {
        init.remove_prefix(1);
    }

    m_list = parse_form_urlencoded(init);
}

url_search_params::
url_search_params(std::vector<key_value_pair_t> const& init)
{
    m_list = init;
}

url_search_params::
url_search_params(std::map<byte_sequence_t, byte_sequence_t> const& init)
{
    for (auto const& [key, value]: init) {
        m_list.emplace_back(key, value);
    }
}

std::optional<byte_sequence_t> url_search_params::
get(byte_sequence_view_t const name) const
{
    auto const it = std::find_if(m_list.begin(), m_list.end(),
        [&](auto const& pair) {
            return pair.first == name;
        });

    if (it == m_list.end()) {
        return std::nullopt;
    }
    else {
        return it->second;
    }
}

std::vector<byte_sequence_t> url_search_params::
get_all(byte_sequence_view_t const name) const
{
    std::vector<byte_sequence_t> result;

    auto it = find(m_list.begin(), m_list.end(), name);
    while (it != m_list.end()) {
        result.push_back(it->second);

        it = find(it + 1, m_list.end(), name);
    }

    return result;
}

bool url_search_params::
has(byte_sequence_view_t const name) const
{
    auto const it = find(m_list.begin(), m_list.end(), name);

    return it != m_list.end();
}

byte_sequence_t url_search_params::
to_string() const
{
    return serialize_form_urlencoded(m_list);
}

void url_search_params::
set(byte_sequence_view_t const name, byte_sequence_view_t const value)
{
    auto it = find(m_list.begin(), m_list.end(), name);
    if (it != m_list.end()) {
        it->second = value;

        m_list.erase(
            std::remove_if(it + 1, m_list.end(), // LCOV_EXCL_LINE
                [&](auto const& pair) {
                    return pair.first == name;
                }),
            m_list.end() // LCOV_EXCL_LINE
        );
    }
    else {
        append(name, value);
    }

    update();
}

void url_search_params::
append(byte_sequence_view_t const name, byte_sequence_view_t const value)
{
    m_list.emplace_back(name, value);

    update();
}

void url_search_params::
remove(byte_sequence_view_t const name)
{
    m_list.erase(
        std::remove_if(m_list.begin(), m_list.end(), // LCOV_EXCL_LINE
            [&](auto const& pair) {
                return pair.first == name;
            }),
        m_list.end()
    );

    update();
}

void url_search_params::
sort()
{
    std::sort(m_list.begin(), m_list.end(),
        [](auto const& lhs, auto const& rhs) {
            return lhs.first < rhs.first;
        });

    update();
}

void url_search_params::
update()
{
    if (!m_url) return; // spec doesn't mention this

    auto const& query = serialize_form_urlencoded(m_list);

    m_url->search(query); // url.search setter will handle empty string situation
}

void url_search_params::
clear()
{
    m_list.clear();
}

void url_search_params::
set_url(url& url_)
{
    m_url = &url_;
}

} // namespace whatwg::url
