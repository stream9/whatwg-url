#include <whatwg/url/slim/string_component.hpp>

#include <whatwg/url/slim/url_record.hpp>

namespace whatwg::url::slim {

/*
 * const_string_component
 */
const_string_component::
const_string_component(url_record const& url, segment_id const segment)
    : m_url { url }
    , m_segment { segment }
{}

const_string_component::
const_string_component(string_component const& other)
    : m_url { other.m_url }
    , m_segment { other.m_segment }
{}

const_string_component::
operator bool() const
{
    return !m_url.is_null(m_segment);
}

bool const_string_component::
empty() const
{
    return m_url.is_empty(m_segment);
}

const_string_component::
operator byte_sequence_view_t() const
{
    return m_url.to_string_view(m_segment);
}

/*
 * string_component
 */
string_component::
string_component(url_record& url, segment_id const segment)
    : m_url { url }
    , m_segment { segment }
{}

string_component& string_component::
operator=(string_component const& other)
{
    return *this = static_cast<const_string_component>(other);
}

string_component& string_component::
operator=(const_string_component const& other)
{
    if (!other) {
        reset();
    }
    else {
        m_url.assign(m_segment, static_cast<byte_sequence_view_t>(other));
    }

    return *this;
}

string_component& string_component::
operator=(byte_sequence_view_t const s)
{
    m_url.assign(m_segment, s);
    return *this;
}

string_component& string_component::
operator=(string_view_t const s)
{
    m_url.assign(m_segment, s);
    return *this;
}

void string_component::
append(byte_t const c)
{
    m_url.append(m_segment, c);
}

void string_component::
append(code_point_t const c)
{
    m_url.append(m_segment, c);
}

void string_component::
append(byte_sequence_view_t const s)
{
    m_url.append(m_segment, s);
}

void string_component::
append(string_view_t const s)
{
    m_url.append(m_segment, s);
}

void string_component::
prepend(byte_t const c)
{
    m_url.prepend(m_segment, c);
}

void string_component::
prepend(byte_sequence_view_t const s)
{
    m_url.prepend(m_segment, s);
}

void string_component::
prepend(code_point_t const c)
{
    m_url.prepend(m_segment, c);
}

void string_component::
prepend(string_view_t const s)
{
    m_url.prepend(m_segment, s);
}

void string_component::
insert(size_type const index, byte_sequence_view_t const s)
{
    m_url.insert(m_segment, index, s);
}

void string_component::
erase(size_type const index, size_type const length)
{
    m_url.erase(m_segment, index, length);
}

void string_component::
clear()
{
    m_url.clear(m_segment);
}

string_component::
operator bool() const
{
    return !m_url.is_null(m_segment);
}

bool string_component::
empty() const
{
    return m_url.is_empty(m_segment);
}

void string_component::
reset()
{
    m_url.reset(m_segment);
}

string_component::
operator byte_sequence_view_t() const
{
    return m_url.to_string_view(m_segment);
}

/*
 * free function
 */
void
assign(string_component comp, byte_sequence_view_t const s)
{
    comp = s;
}

void
assign(string_component comp, string_view_t const s)
{
    comp = s;
}

void
append(string_component comp, byte_t const c)
{
    comp.append(c);
}

void
append(string_component comp, byte_sequence_view_t const s)
{
    comp.append(s);
}

void
append(string_component comp, code_point_t const c)
{
    comp.append(c);
}

void
append(string_component comp, string_view_t const s)
{
    comp.append(s);
}

void
prepend(string_component comp, byte_t const c)
{
    comp.prepend(c);
}

void
prepend(string_component comp, byte_sequence_view_t const s)
{
    comp.prepend(s);
}

void
prepend(string_component comp, code_point_t const c)
{
    comp.prepend(c);
}

void
prepend(string_component comp, string_view_t const s)
{
    comp.prepend(s);
}

bool
is_null(const_string_component const s)
{
    return !s;
}

bool
is_empty(const_string_component const s)
{
    return s.empty();
}

byte_sequence_view_t
value_or(const_string_component const comp,
         byte_sequence_view_t const def_value)
{
    return comp ? comp : def_value;
}

std::ostream&
operator<<(std::ostream& os, const_string_component const& c)
{
    return os << c.operator byte_sequence_view_t();
}

bool
operator==(const_string_component const& lhs, byte_sequence_view_t const rhs)
{
    return lhs.operator byte_sequence_view_t() == rhs;
}

bool
operator!=(const_string_component const& lhs, byte_sequence_view_t const rhs)
{
    return !(lhs == rhs);
}

} // namespace whatwg::url::slim
