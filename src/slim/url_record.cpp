#include <whatwg/url/slim/url_record.hpp>

#include <whatwg/url/slim/string_component.hpp>
#include <whatwg/url/slim/host_component.hpp>
#include <whatwg/url/slim/port_component.hpp>
#include <whatwg/url/slim/path_component.hpp>

#include <whatwg/url/utility.hpp>

namespace whatwg::url::slim {

template<typename T, size_t N>
constexpr int
length(std::array<T, N>)
{
    return static_cast<int>(N);
}

template<typename T>
auto
segment_length_impl(T const s)
{
    auto const len = length(s);

    using result_t = segment_t::size_type;
    assert(len <= std::numeric_limits<result_t>::max());

    return static_cast<result_t>(len);
}

auto
segment_length(byte_sequence_view_t const s)
{
    return segment_length_impl(s);
}

auto
segment_length(string_view_t const s)
{
    return segment_length_impl(s);
}

static bool
is_valid_source_string(byte_sequence_view_t const s)
{
    return s.size() < INT32_MAX;
}

static bool
is_valid_source_string(string_view_t const s)
{
    return s.size() < INT32_MAX;
}

static bool
is_valid_delimiter(byte_sequence_view_t const s)
{
    return s.size() < INT8_MAX;
}

template<size_t N>
std::ostream&
operator<<(std::ostream& os, std::array<segment_t, N> const& arr)
{
    for (auto const& s: arr) {
        os << s << ", ";
    }

    return os;
}

/*
 * url_record
 */
url_record::
url_record()
    : m_spec {}
    , m_segments {}
    , m_flags { static_cast<uint32_t>(flag::dirty) }
{
    clear(segment_id::scheme);
    clear(segment_id::username);
    clear(segment_id::password);
}

string_component url_record::
scheme()
{
    return { *this, segment_id::scheme };
}

const_string_component url_record::
scheme() const
{
    return { *this, segment_id::scheme };
}

string_component url_record::
username()
{
    return { *this, segment_id::username };
}

const_string_component url_record::
username() const
{
    return { *this, segment_id::username };
}

string_component url_record::
password()
{
    return { *this, segment_id::password };
}

const_string_component url_record::
password() const
{
    return { *this, segment_id::password };
}

host_component url_record::
host()
{
    return { *this };
}

const_host_component url_record::
host() const
{
    return { *this };
}

port_component url_record::
port()
{
    return { *this };
}

const_port_component url_record::
port() const
{
    return { *this };
}

path_component url_record::
path()
{
    return { *this };
}

const_path_component url_record::
path() const
{
    return { *this };
}

string_component url_record::
query()
{
    return { *this, segment_id::query };
}

const_string_component url_record::
query() const
{
    return { *this, segment_id::query };
}

string_component url_record::
fragment()
{
    return { *this, segment_id::fragment };
}

const_string_component url_record::
fragment() const
{
    return { *this, segment_id::fragment };
}

flag_proxy<uint32_t> url_record::
cannot_be_a_base_url()
{
    return {
        m_flags,
        static_cast<uint32_t>(flag::cannot_be_a_base_url)
    };
}

flag_proxy<uint32_t const> url_record::
cannot_be_a_base_url() const
{
    return {
        m_flags,
        static_cast<uint32_t>(flag::cannot_be_a_base_url)
    };
}

void* url_record::
object() const
{
    return nullptr; //TODO lookup object store
}

void url_record::
reset_flag(flag const f)
{
    m_flags &= ~static_cast<uint32_t>(f);
}

bool url_record::
has_flag(flag const f) const
{
    return m_flags & static_cast<uint32_t>(f);
}

url_record::
operator byte_sequence_view_t() const
{
    if (has_flag(flag::dirty)) {
        const_cast<url_record*>(this)->adjust_delimiters();
        const_cast<url_record*>(this)->reset_flag(flag::dirty);
    }

    return m_spec;
}

segment_t& url_record::
segment(segment_id const sid)
{
    auto& s = m_segments[to_size(sid)];
    if (!s) {
        clear(sid);
    }

    return s;
}

bool url_record::
has_authority() const
{
    using id = segment_id;

    return (!is_null(id::username) && !is_empty(id::username))
        || (!is_null(id::password) && !is_empty(id::password))
        || !is_null(segment_id::host)
        || !is_null(segment_id::port);
}

void url_record::
assign(segment_id const sid, byte_sequence_view_t const src)
{
    assert(sid.is_valid());
    assert(is_valid_source_string(src));

    auto const src_length = segment_length(src);

    auto& [index, length] = segment(sid);

    m_spec.replace(to_size(index), to_size(length), src);

    auto const delta = src_length - length;
    length = src_length;

    adjust_index(sid + 1, delta);
    set_flag(flag::dirty);
}

void url_record::
assign(segment_id const sid, string_view_t const src)
{
    assert(sid.is_valid());
    assert(is_valid_source_string(src));

    auto& seg = segment(sid);

    auto src_it = src.begin();
    auto const src_end = src.end();

    auto dest_it = m_spec.begin() + seg.index;
    auto const dest_end = dest_it + seg.length;

    for (; dest_it != dest_end && src_it != src_end; ++src_it, ++dest_it) {
        *dest_it = to_byte(*src_it);
    }

    if (src_it == src_end) {
        m_spec.erase(dest_it, dest_end);
    }
    else if (dest_it == dest_end) {
        auto const rest_len = src_end - src_it;
        dest_it = m_spec.insert(dest_it, to_size(rest_len), ' ');

        for (; src_it != src_end; ++src_it, ++dest_it) {
            *dest_it = to_byte(*src_it);
        }
    }

    auto const src_length = segment_length(src);

    auto const delta = src_length - seg.length;
    seg.length = src_length;

    adjust_index(sid + 1, delta);
    set_flag(flag::dirty);
}

void url_record::
append(segment_id const sid, byte_t const c)
{
    auto& [index, length] = segment(sid);

    m_spec.insert(to_size(index + length), 1, c);

    ++length;
    adjust_index(sid + 1, 1);
    set_flag(flag::dirty);
}

void url_record::
append(segment_id const sid, byte_sequence_view_t const src)
{
    assert(sid.is_valid());
    assert(is_valid_source_string(src));

    auto const src_length = segment_length(src);

    auto& [index, length] = segment(sid);

    m_spec.insert(to_size(index + length), src);

    length += src_length;
    adjust_index(sid + 1, src_length);
    set_flag(flag::dirty);
}

void url_record::
append(segment_id const sid, code_point_t const c)
{
    append(sid, to_byte(c));
}

void url_record::
append(segment_id const sid, string_view_t const src)
{
    assert(sid.is_valid());
    assert(is_valid_source_string(src));

    auto const src_length = segment_length(src);

    auto& [index, length] = segment(sid);

    auto it = m_spec.begin() + index + length;
    it = m_spec.insert(it, to_size(src_length), ' ');

    for (auto const ch: src) {
        *it = to_byte(ch);
        ++it;
    }

    length += src_length;
    adjust_index(sid + 1, src_length);
    set_flag(flag::dirty);
}

void url_record::
prepend(segment_id const sid, byte_t const c)
{
    assert(sid.is_valid());

    auto& [index, length] = segment(sid);

    m_spec.insert(to_size(index), 1, c);

    ++length;
    adjust_index(sid + 1, 1);
    set_flag(flag::dirty);
}

void url_record::
prepend(segment_id const sid, byte_sequence_view_t const src)
{
    assert(sid.is_valid());
    assert(is_valid_source_string(src));

    auto const src_length = segment_length(src);

    auto& [index, length] = segment(sid);

    m_spec.insert(to_size(index), src);

    length += src_length;
    adjust_index(sid + 1, src_length);
    set_flag(flag::dirty);
}

void url_record::
prepend(segment_id const sid, code_point_t const c)
{
    prepend(sid, to_byte(c));
}

void url_record::
prepend(segment_id const sid, string_view_t const src)
{
    assert(sid.is_valid());
    assert(is_valid_source_string(src));

    auto const src_length = segment_length(src);

    auto& [index, length] = segment(sid);

    auto it = m_spec.begin() + index;
    it = m_spec.insert(it, to_size(src_length), ' ');

    for (auto const ch: src) {
        *it = to_byte(ch);
        ++it;
    }

    length += src_length;
    adjust_index(sid + 1, src_length);
    set_flag(flag::dirty);
}

void url_record::
insert(segment_id const sid,
       size_type const index,
       byte_sequence_view_t const s)
{
    assert(sid.is_valid());
    assert(index >= 0);
    assert(is_valid_source_string(s));

    auto& seg = segment(sid);
    assert(index <= seg.length);

    m_spec.insert(to_size(seg.index + index), s);

    auto const s_length = segment_length(s);

    seg.length += s_length;
    adjust_index(sid + 1, s_length);
    set_flag(flag::dirty);
}

void url_record::
erase(segment_id const sid,
      size_type const index,
      size_type const length)
{
    assert(sid.is_valid());
    assert(index >= 0);
    assert(length >= 0);

    auto& seg = m_segments[to_size(sid)];
    assert(!seg.is_null());

    assert(index < seg.length);
    assert(index + length <= seg.length);

    m_spec.erase(to_size(seg.index + index), to_size(length));

    seg.length -= length;
    adjust_index(sid + 1, -length);
    set_flag(flag::dirty);
}

void url_record::
clear(segment_id const sid)
{
    assert(sid.is_valid());

    auto& seg = m_segments[to_size(sid)];

    if (seg) {
        m_spec.erase(to_size(seg.index), to_size(seg.length));
        adjust_index(sid + 1, -seg.length);
    }

    seg.length = 0;
    set_flag(flag::dirty);
}

void url_record::
reset(segment_id const sid)
{
    assert(sid.is_valid());

    auto& s = m_segments[to_size(sid)];
    if (s.is_null()) return;

    m_spec.erase(to_size(s.index), to_size(s.length));
    adjust_index(sid + 1, -s.length);
    s.length = -1;

    set_flag(flag::dirty);
}

void url_record::
adjust_index(segment_id const sid, difference_type const delta)
{
    if (!sid.is_valid()) return;

    auto const len = length(m_segments);

    for (int i = sid; i < len; ++i) {
        auto& [index, _] = m_segments[to_size(i)];

        index += delta;
    }
}

void url_record::
adjust_delimiters()
{
    adjust_suffix_delimiter(segment_id::scheme, ":");

    adjust_authority_delimiter();

    if (!is_null(segment_id::password) && !is_empty(segment_id::password)) {
        add_suffix_delimiter(segment_id::username, ":");
        add_suffix_delimiter(segment_id::password, "@");
    }
    else if (!is_null(segment_id::username) && !is_empty(segment_id::username)) {
        adjust_suffix_delimiter(segment_id::username, "@");
        remove_suffix_delimiter(segment_id::password);
    }
    else {
        remove_suffix_delimiter(segment_id::username);
        remove_suffix_delimiter(segment_id::password);
    }

    adjust_prefix_delimiter(segment_id::port, ":");
    adjust_prefix_delimiter(segment_id::query, "?");
    adjust_prefix_delimiter(segment_id::fragment, "#");
}

void url_record::
add_suffix_delimiter(segment_id const sid, byte_sequence_view_t const delim)
{
    assert(sid.is_valid());
    assert(is_valid_delimiter(delim));

    auto const& cur = m_segments[to_size(sid)];
    auto const delim_len = suffix_delimiter_length(sid);

    auto const delim_idx = cur ? cur.index + cur.length : cur.index;
    m_spec.replace(to_size(delim_idx), to_size(delim_len), delim);

    auto const len = segment_length(delim);

    adjust_index(sid + 1, len - delim_len);
}

void url_record::
remove_suffix_delimiter(segment_id const sid)
{
    assert(sid.is_valid());

    auto const& cur = m_segments[to_size(sid)];
    auto const delim_len = suffix_delimiter_length(sid);

    if (delim_len != 0) {
        auto const delim_idx = cur.index;
        m_spec.erase(to_size(delim_idx), to_size(delim_len));

        adjust_index(sid + 1, -delim_len);
    }
}

void url_record::
adjust_suffix_delimiter(segment_id const sid, byte_sequence_view_t const delim)
{
    assert(sid.is_valid());
    assert(is_valid_delimiter(delim));

    if (!is_null(sid)) {
        add_suffix_delimiter(sid, delim);
    }
    else {
        remove_suffix_delimiter(sid);
    }
}

void url_record::
set_flag(flag const f)
{
    m_flags |= static_cast<uint32_t>(f);
}

void url_record::
adjust_prefix_delimiter(segment_id const sid, byte_sequence_view_t const delim)
{
    assert(sid.is_valid());
    assert(is_valid_delimiter(delim));

    auto const& cur = m_segments[to_size(sid)];
    auto const cur_delim_len = prefix_delimiter_length(sid);
    auto const delim_idx = cur.index - cur_delim_len;

    if (cur.is_null()) { // erase delimiter
        if (cur_delim_len != 0) {
            m_spec.erase(to_size(delim_idx), to_size(cur_delim_len));

            adjust_index(sid, -cur_delim_len);
        }
    }
    else { // add/replace delimiter
        m_spec.replace(to_size(delim_idx), to_size(cur_delim_len), delim);

        auto const delim_len = segment_length(delim);
        adjust_index(sid, delim_len - cur_delim_len);
    }
}

void url_record::
adjust_authority_delimiter()
{
    auto const& scheme = m_segments[segment_id::scheme];
    auto const& auth = m_segments[segment_id::username];
    auto const delim = "//"_sv;

    auto const delim_idx = scheme.is_null() ? 0 :
        (scheme.index + scheme.length + static_cast<int>(sizeof(':')));

    auto const cur_delim_len = auth.index - delim_idx;

    if (has_authority() || this->scheme() == "file") { // add / replace delimiter
        m_spec.replace(to_size(delim_idx), to_size(cur_delim_len), delim);

        auto const delim_len = segment_length(delim);

        adjust_index(segment_id::username, delim_len - cur_delim_len);
    }
    else { // erase delimiter
        m_spec.erase(to_size(delim_idx), to_size(cur_delim_len));
        adjust_index(segment_id::username, -cur_delim_len);
    }
}

int url_record::
suffix_delimiter_length(segment_id const sid) const
{
    assert(sid.is_valid());

    auto const& cur = m_segments[to_size(sid)];

    auto const delim_idx =
        cur.is_null() ? cur.index : cur.index + cur.length;

    auto const segments_len = static_cast<int>(m_segments.size());

    auto next_idx = segments_len;
    if (sid != segments_len - 1) {
        next_idx = m_segments[to_size(sid + 1)].index;
    }

    return next_idx - delim_idx;
}

int url_record::
prefix_delimiter_length(segment_id const sid) const
{
    assert(sid.is_valid());

    auto const& cur = m_segments[to_size(sid)];

    auto delim_idx = 0;
    if (sid != 0) {
        auto const& prev = m_segments[to_size(sid - 1)];

        if (prev.is_null()) {
            delim_idx = prev.index;
        }
        else {
            delim_idx = prev.index + prev.length;
        }
    }

    return cur.index - delim_idx;
}

bool url_record::
is_null(segment_id const sid) const
{
    assert(sid.is_valid());

    return m_segments[to_size(sid)].is_null();
}

bool url_record::
is_empty(segment_id const sid) const
{
    assert(sid.is_valid());

    return m_segments[to_size(sid)].length == 0;
}

byte_sequence_view_t url_record::
to_string_view(segment_id const sid) const
{
    assert(sid.is_valid());

    auto const& segment = m_segments[to_size(sid)];

    if (!segment.is_null()) {
        return {
            &m_spec[to_size(segment.index)],
            static_cast<size_t>(segment.length)
        };
    }
    else {
        return {};
    }
}

/*
 * free function
 */
byte_sequence_view_t
serialize(url_record const& u, bool const exclude_fragment/*= false*/)
{
    byte_sequence_view_t const uv { u };

    if (!exclude_fragment) {
        return uv;
    }
    else {
        auto const fragment = u.fragment();
        if (!fragment) {
            return uv;
        }
        else {
            auto const begin = uv.begin();
            auto const end =
                static_cast<byte_sequence_view_t>(fragment).begin() - 1; // omit '/'

            return { begin,
                static_cast<byte_sequence_view_t::size_type>(end - begin) };
        }
    }
}

std::ostream&
operator<<(std::ostream& os, url_record const& u)
{
    return os << static_cast<byte_sequence_view_t>(u);
}

bool
operator==(url_record const& lhs, byte_sequence_view_t const rhs)
{
    return static_cast<byte_sequence_view_t>(lhs) == rhs;
}

} // namespace whatwg::url::slim
