#include <whatwg/url/slim/port_component.hpp>

#include <whatwg/url//misc.hpp>

#include <charconv>

namespace whatwg::url::slim {

/*
 * const_port_component
 */
const_port_component::
const_port_component(url_record const& u)
    : m_comp { u, segment_id::port }
{}

const_port_component::
const_port_component(port_component const& other)
    : m_comp { other.m_comp }
{}

const_port_component::
operator bool() const
{
    return !!m_comp;
}

uint16_t const_port_component::
operator*() const
{
    assert(m_comp);

    byte_sequence_view_t const sv = m_comp;
    uint16_t result;

    auto const rv = std::from_chars(sv.begin(), sv.end(), result, 10);
    assert(rv.ec == std::errc());

    return result;
}

/*
 * port_component
 */
port_component::
port_component(url_record& u)
    : m_comp { u, segment_id::port }
{}

port_component& port_component::
operator=(port_component const& other)
{
    return *this = static_cast<const_port_component>(other);
}

port_component& port_component::
operator=(const_port_component const& other)
{
    if (other) {
        *this = *other;
    }
    else {
        m_comp.reset();
    }

    return *this;
}

port_component& port_component::
operator=(uint16_t const p)
{
    m_comp = serialize_integer(p);
    return *this;
}

port_component::
operator bool() const
{
    return !!m_comp;
}

uint16_t port_component::
operator*() const
{
    assert(m_comp);

    byte_sequence_view_t const sv = m_comp;
    uint16_t result;

    auto const rv = std::from_chars(sv.begin(), sv.end(), result, 10);
    assert(rv.ec == std::errc());

    return result;
}

void port_component::
reset()
{
    m_comp.reset();
}

std::ostream&
operator<<(std::ostream& os, const_port_component const& p)
{
    if (p) {
        os << *p;
    }
    else {
        os << "null";
    }

    return os;
}

bool
operator==(const_port_component const& lhs,
           const_port_component const& rhs)
{
    if (lhs && rhs) {
        return *lhs == *rhs;
    }
    else if (!lhs && !rhs) {
        return true;
    }
    else {
        return false;
    }
}

bool
operator==(const_port_component const& lhs, uint16_t const rhs)
{
    if (!lhs) {
        return false;
    }
    else {
        return *lhs == rhs;
    }
}

} // namespace whatwg::url::slim
