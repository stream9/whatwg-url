#include <whatwg/url/slim/segment.hpp>

#include <ostream>

namespace whatwg::url::slim {

// LCOV_EXCL_START
std::ostream&
operator<<(std::ostream& os, segment_t const& s)
{
    os << "{ " << s.index << ", " << s.length << " }";
    return os;
}
// LCOV_EXCL_STOP

} // namespace whatwg::url::slim

