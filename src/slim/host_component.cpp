#include <whatwg/url/slim/host_component.hpp>

#include <whatwg/url//host.hpp>

namespace whatwg::url::slim {

/*
 * const_host_component
 */
const_host_component::
const_host_component(url_record const& u)
    : m_comp { u, segment_id::host }
{}

const_host_component::
const_host_component(host_component const& other)
    : m_comp { other.m_comp }
{}

bool const_host_component::
is_empty() const
{
    return m_comp && static_cast<byte_sequence_view_t>(m_comp).empty();
}

bool const_host_component::
is_null() const
{
    return !m_comp;
}

const_host_component::
operator bool() const
{
    return static_cast<bool>(m_comp);
}

const_host_component::
operator byte_sequence_view_t() const
{
    return m_comp;
}

/*
 * host_component
 */
host_component::
host_component(url_record& u)
    : m_comp { u, segment_id::host }
{}

host_component& host_component::
operator=(const_host_component const& other)
{
    m_comp = other.m_comp;
    return *this;
}

host_component& host_component::
operator=(host_t const& h)
{
    m_comp = serialize_host(h);
    return *this;
}

bool host_component::
is_empty() const
{
    return m_comp && static_cast<byte_sequence_view_t>(m_comp).empty();
}

bool host_component::
is_null() const
{
    return !m_comp;
}

void host_component::
set_empty_string()
{
    m_comp.clear();
}

host_component::
operator bool() const
{
    return static_cast<bool>(m_comp);
}

host_component::
operator byte_sequence_view_t() const
{
    return m_comp;
}

/*
 * free function
 */
void
assign(host_component comp, host_t const& h)
{
    comp = h;
}

byte_sequence_t
serialize_host(const_host_component const h)
{
    return std::string { h };
}

std::ostream&
operator<<(std::ostream& os, const_host_component const& h)
{
    return os << static_cast<byte_sequence_view_t>(h);
}

bool
operator==(const_host_component const& lhs, byte_sequence_view_t const rhs)
{
    return static_cast<byte_sequence_view_t>(lhs) == rhs;
}

} // namespace whatwg::url::slim
