#include <whatwg/url/slim/path_component.hpp>

#include <whatwg/url/slim/url_record.hpp>

#include <algorithm>

namespace whatwg::url::slim {

static size_t
find_nth_of(byte_sequence_view_t const sv,
            int const n,
            char const ch)
{
    int i = 0;
    size_t idx = 0;
    for (; i <= n; ++i, ++idx) {
        idx = sv.find_first_of(ch, idx);
        if (idx == sv.npos) break;
    }

    return i == (n + 1) ? (idx - 1) : sv.npos;
}

template<typename T>
typename T::size_type
path_size(T const& path)
{
    if (path.is_hierarchical()) {
        byte_sequence_view_t const sv { path };
        if (sv.empty()) return 0;

        return static_cast<typename T::size_type>(
            std::count(sv.begin(), sv.end(), '/'));
    }
    else {
        return 1;
    }
}

template<typename PathComponentT>
byte_sequence_view_t
at(PathComponentT const& path, typename PathComponentT::size_type const index)
{
    byte_sequence_view_t const sv { path };

    if (sv.empty()) return "";

    if (path.is_hierarchical()) {
        auto from = find_nth_of(sv, index, '/');
        assert(from != sv.npos);
        ++from;

        auto to = sv.find_first_of('/', from);
        if (to == sv.npos) {
            to = sv.size();
        }

        return sv.substr(from, to - from);
    }
    else {
        assert(index == 0);

        return sv;
    }
}

/*
 * const_path_component
 */
const_path_component::
const_path_component(url_record const& u)
    : m_comp { u, segment_id::path }
{}

const_path_component::
const_path_component(path_component const& other)
    : m_comp { other.m_comp }
{}

const_path_component::size_type const_path_component::
size() const
{
    return path_size(*this);
}

bool const_path_component::
empty() const
{
    return size() == 0;
}

bool const_path_component::
is_hierarchical() const
{
    return !m_comp.url().cannot_be_a_base_url();
}

byte_sequence_view_t const_path_component::
operator[](size_type const index) const
{
    return at(*this, index);
}

const_path_component::
operator byte_sequence_view_t() const
{
    return static_cast<byte_sequence_view_t>(m_comp);
}

/*
 * path_component
 */
path_component::
path_component(url_record& u)
    : m_comp { u, segment_id::path }
{}

path_component& path_component::
operator=(path_component const& other)
{
    return *this = static_cast<const_path_component>(other);
}

path_component& path_component::
operator=(const_path_component const& other)
{
    return *this = static_cast<byte_sequence_view_t>(other);
}

path_component& path_component::
operator=(byte_sequence_view_t const s)
{
    assign(s);
    return *this;
}

path_component::size_type path_component::
size() const
{
    return path_size(*this);
}

bool path_component::
empty() const
{
    return size() == 0;
}

bool path_component::
is_hierarchical() const
{
    return !m_comp.url().cannot_be_a_base_url();
}

void path_component::
assign(byte_sequence_view_t const s)
{
    m_comp = s;
}

void path_component::
pop_front()
{
    if (empty()) return;

    if (is_hierarchical()) {
        auto const sv = static_cast<byte_sequence_view_t>(m_comp);

        auto second_idx = sv.find_first_of('/', 1); // 0 is first '/'
        if (second_idx == sv.npos) {
            second_idx = sv.size();
        }

        m_comp.erase(0,
            static_cast<string_component::size_type>(second_idx));
    }
    else {
        m_comp.clear();
    }
}

void path_component::
pop_back()
{
    if (empty()) return;

    if (is_hierarchical()) {
        auto const sv = static_cast<byte_sequence_view_t>(m_comp);

        auto const last_idx = sv.find_last_of('/');
        assert(last_idx != sv.npos);

        m_comp.erase(
            static_cast<string_component::size_type>(last_idx),
            static_cast<string_component::size_type>(sv.size() - last_idx));
    }
    else {
        m_comp.clear();
    }

}

void path_component::
append(byte_sequence_view_t const segment)
{
    if (is_hierarchical()) {
        m_comp.append('/');
    }

    m_comp.append(segment);
}

void path_component::
append(string_view_t const segment)
{
    if (is_hierarchical()) {
        m_comp.append('/');
    }

    m_comp.append(segment);
}

void path_component::
append(size_type const index, string_view_t const s)
{
    assert(!is_hierarchical());
    assert(index == 0);

    m_comp.append(s);
}

void path_component::
clear()
{
    m_comp.clear();
}

byte_sequence_view_t path_component::
operator[](size_type const index) const
{
    return at(*this, index);
}

path_component::
operator byte_sequence_view_t() const
{
    return static_cast<byte_sequence_view_t>(m_comp);
}

void
assign(path_component path, byte_sequence_view_t const s)
{
    path.assign(s);
}

void
append(path_component path, byte_sequence_view_t const s)
{
    path.append(s);
}

void
append(path_component path, string_view_t const s)
{
    path.append(s);
}

void
append(path_component path,
       path_component::size_type const index, string_view_t const s)
{
    path.append(index, s);
}

void
pop_front(path_component path)
{
    path.pop_front();
}

byte_sequence_view_t
join_path(const_path_component const path, byte_t)
{
    return static_cast<byte_sequence_view_t>(path);
}

std::ostream&
operator<<(std::ostream& os, const_path_component const& path)
{
    return os << path.operator byte_sequence_view_t();
}

bool
operator==(const_path_component const& lhs, byte_sequence_view_t const rhs)
{
    return lhs.operator byte_sequence_view_t() == rhs;
}

} // namespace whatwg::url::slim
