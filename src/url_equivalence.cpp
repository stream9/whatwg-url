#include <whatwg/url/url_equivalence.hpp>

#include <whatwg/url/url_serializing.hpp>

namespace whatwg::url {

bool
equals(url_record const& lhs,
       url_record const& rhs, bool const exclude_fragment/*= false*/)
{
    auto const& serialized_lhs = serialize_url(lhs, exclude_fragment);
    auto const& serialized_rhs = serialize_url(rhs, exclude_fragment);

    return serialized_lhs == serialized_rhs;
}

} // namespace whatwg::url
