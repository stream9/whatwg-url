#include <whatwg/url/conversion.hpp>

#include <whatwg/infra/code_point.hpp>

#include <algorithm>
#include <cassert>
#include <charconv>

namespace whatwg::url {

char
number_to_upper_hex_digit(int const i)
{
    assert(0 <= i && i <= 15);

    if (0 <= i && i <= 9) {
        return static_cast<char>('0' + i);
    }
    else {
        assert(10 <= i && i <= 15);
        return static_cast<char>('A' + (i - 10));
    }
}

int
decimal_digit_to_number(code_point_t const c)
{
    assert('0' <= c && c <= '9');

    return static_cast<int>(c - '0');
}

int
hex_digit_to_number(code_point_t const c)
{
    if ('0' <= c && c <= '9') {
        return static_cast<int>(c - '0');
    }
    else if ('a' <= c && c <= 'f') {
        return static_cast<int>(c - 'a' + 10);
    }
    else if ('A' <= c && c <= 'F') {
        return static_cast<int>(c - 'A' + 10);
    }
    else {
        assert(false); // LCOV_EXCL_LINE
    }
}

std::optional<int64_t>
digits_to_number(byte_sequence_view_t const input, int const radix)
{
    int64_t value;
    auto const result =
        std::from_chars(input.begin(), input.end(), value, radix);

    if (result.ec != std::errc()) {
        return std::nullopt; // LCOV_EXCL_LINE
    }

    return value;
}

std::optional<int64_t>
decimal_digits_to_number(string_view_t const input)
{
    int64_t result = 0;

    for (auto c: input) {
        if (!infra::is_ascii_digit(c) || result > INT64_MAX / 10) {
            return std::nullopt;
        }

        result = result * 10 + decimal_digit_to_number(c);
    }

    return result;
}

} // namespace whatwg::url
