#include <whatwg/url/url.hpp>

#include <whatwg/url/basic_url_parser.hpp>
#include <whatwg/url/form_urlencoded.hpp>
#include <whatwg/url/misc.hpp>
#include <whatwg/url/origin.hpp>
#include <whatwg/url/url_serializing.hpp>

#include <boost/format.hpp>

namespace whatwg::url {

static void
format_error(byte_sequence_t& msg,
             std::error_code const err,
             string_view_t,
             size_t)
{
    msg.append(err.message());
}

/*
 * url
 */

url::
url(byte_sequence_view_t const spec)
{
    init(spec, std::nullopt);
}

url::
url(byte_sequence_view_t const spec, byte_sequence_view_t const base)
{
    init(spec, base);
}

void url::
init(byte_sequence_view_t const url,
    std::optional<byte_sequence_view_t> const base)
{
    basic_url_parser parser;
    std::optional<url_record> parsed_base;
    byte_sequence_t error_msg;

    parser.set_validation_error_handler(
        [&](auto const err, auto const input, auto const index) {
            format_error(error_msg, err, input, index);
        });

    if (base) {
        parsed_base = parser(*base);
        if (!parsed_base) {
            error_msg = "parse error on base URL: " + error_msg;
            throw type_error(error_msg);
        }
        error_msg.clear();
    }

    auto const parsed_url = parser(url, parsed_base ? &*parsed_base : nullptr);
    if (!parsed_url) {
        error_msg = "parse error: " + error_msg;
        throw type_error(error_msg);
    }

    m_url = std::move(*parsed_url);

    auto const query = value_or(m_url.query(), "");

    m_query_object = url_search_params(query);
    m_query_object.set_url(*this);
}

byte_sequence_t url::
href() const
{
   return byte_sequence_t { serialize_url(m_url) };
}


byte_sequence_t url::
protocol() const
{
    byte_sequence_t result { m_url.scheme() };
    result.push_back(':');

    return result;
}

byte_sequence_t url::
username() const
{
    return byte_sequence_t { m_url.username() };
}

byte_sequence_t url::
password() const
{
    return byte_sequence_t { m_url.password() };
}

byte_sequence_t url::
host() const
{
    if (m_url.host().is_null()) return "";

    if (!m_url.port()) return serialize_host(m_url.host());

    return serialize_host(m_url.host())
         + ":" + serialize_integer(*m_url.port());
}

byte_sequence_t url::
hostname() const
{
    if (m_url.host().is_null()) return "";

    return serialize_host(m_url.host());
}

byte_sequence_t url::
port() const
{
    if (!m_url.port()) return "";

    return serialize_integer(*m_url.port());
}

byte_sequence_t url::
pathname() const
{
    if (m_url.cannot_be_a_base_url()) {
        return byte_sequence_t { m_url.path()[0] };
    }

    if (m_url.path().empty()) return "";

    return byte_sequence_t { join_path(m_url.path(), '/') };
}

byte_sequence_t url::
search() const
{
    //if (!m_url.query() || m_url.query()->empty()) return "";
    if (is_null(m_url.query()) || is_empty(m_url.query())) return "";

    byte_sequence_t result = "?";
    append(result, *m_url.query());

    return result;
}

byte_sequence_t url::
hash() const
{
    //if (!m_url.fragment() || m_url.fragment()->empty()) return "";
    if (is_null(m_url.fragment()) || is_empty(m_url.fragment())) return "";

    byte_sequence_t result = "#";
    append(result, *m_url.fragment());

    return result;
}

byte_sequence_t url::
origin() const
{
    namespace u = url;

    u::origin o { m_url };
    return o.serialize();
}

url_search_params& url::
search_params()
{
    return m_query_object;
}

url_search_params const& url::
search_params() const
{
    return m_query_object;
}

byte_sequence_t url::
to_string() const
{
    return byte_sequence_t { serialize_url(m_url) };
}

void url::
href(byte_sequence_view_t const value)
{
    auto const parsed_url = basic_url_parse(value);
    if (!parsed_url) {
        throw type_error();
    }

    m_url = std::move(*parsed_url);

    auto const& query = m_url.query();
    if (query) {
        m_query_object.m_list = parse_form_urlencoded(*query);
    }
}

void url::
protocol(byte_sequence_view_t const value)
{
    byte_sequence_t v { value };
    v.push_back(':');

    basic_url_parse(
        v,
        m_url,
        basic_url_parser::state::scheme_start);
}

void url::
username(byte_sequence_view_t const value)
{
    if (cannot_have_a_username_password_port(m_url)) return;

    set_username(m_url, value);
}

void url::
password(byte_sequence_view_t const value)
{
    if (cannot_have_a_username_password_port(m_url)) return;

    set_password(m_url, value);
}

void url::
host(byte_sequence_view_t const value)
{
    if (m_url.cannot_be_a_base_url()) return;

    basic_url_parse(
        value,
        m_url,
        basic_url_parser::state::host
    );
}

void url::
hostname(byte_sequence_view_t const value)
{
    if (m_url.cannot_be_a_base_url()) return;

    basic_url_parse(
        value,
        m_url,
        basic_url_parser::state::hostname
    );
}

void url::
port(byte_sequence_view_t const value)
{
    if (cannot_have_a_username_password_port(m_url)) return;

    if (value.empty()) {
        m_url.port().reset();
    }
    else {
        basic_url_parse(
            value,
            m_url,
            basic_url_parser::state::port
        );
    }
}

void url::
pathname(byte_sequence_view_t const value)
{
    if (m_url.cannot_be_a_base_url()) return;

    m_url.path().clear();

    basic_url_parse(
        value,
        m_url,
        basic_url_parser::state::path_start
    );
}

void url::
search(byte_sequence_view_t value)
{
    if (value.empty()) {
        m_url.query().reset();
        m_query_object.clear();
        return;
    }

    if (value.front() == '?') {
        value.remove_prefix(1);
    }

    m_url.query() = "";

    basic_url_parse( // LCOV_EXCL_LINE
        value,
        m_url,
        basic_url_parser::state::query
    );

    m_query_object.m_list = parse_form_urlencoded(*m_url.query());
}

void url::
hash(byte_sequence_view_t value)
{
    if (value.empty()) {
        m_url.fragment().reset();
        return;
    }

    if (value.front() == '#') {
        value.remove_prefix(1);
    }

    m_url.fragment() = "";

    basic_url_parse(
        value,
        m_url,
        basic_url_parser::state::fragment
    );
}

} // namespace whatwg::url
