#include <whatwg/url/host.hpp>

#include <whatwg/url/classification.hpp>
#include <whatwg/url/conversion.hpp>
#include <whatwg/url/error.hpp>
#include <whatwg/url/idna.hpp>
#include <whatwg/url/lambda.hpp>
#include <whatwg/url/misc.hpp>
#include <whatwg/url/percent_encoding.hpp>
#include <whatwg/url/range.hpp>
#include <whatwg/url/string.hpp>
#include <whatwg/url/utility.hpp>

#include <whatwg/infra/code_point.hpp>

#include <cassert>
#include <cmath>
#include <variant>
#include <vector>

//#define VERBOSE

#ifdef VERBOSE
#include <iostream>
#endif

namespace whatwg::url {

static int
first_index_of_longest_consecutive_zeros(ipv6_address_t const& address)
{
    assert(address.size() == 8); // LCOV_EXCL_LINE

    auto index = -1, max = 1;
    auto [it, end] = range(address);

    while (it != end) {
        if (*it != 0) {
            ++it;
            continue;
        }

        // count consecutive zeros
        auto count = 1;
        auto it2 = it;
        while (++it2 != end) {
            if (*it2 != 0) break;

            ++count;
        }

        if (count > max) {
            max = count;
            index = static_cast<int>(std::distance(address.begin(), it));
        }

        it += count;
    }

    return index;
}

static byte_sequence_t
to_shortest_lower_hex_digits(int const i)
{
    assert(i >= 0);

    static char const tbl[] = "0123456789abcdef";
    byte_sequence_t result;

    for (auto d = i; d != 0; d /= 0x10) {
        result.push_back(tbl[d & 0x0f]);
    }

    if (result.empty()) {
        result.push_back('0');
    }

    std::reverse(result.begin(), result.end());
    return result;
}

/*
 * host_t
 */
bool host_t::
is_null() const
{
    return std::holds_alternative<null_host_t>(*this);
}

bool host_t::
is_empty() const
{
    return std::visit(overloaded {
        [](domain_t const& h) {
            return h.empty();
        },
        [](opaque_host_t const& h) {
            return h.empty();
        },
        [](empty_host_t const&) {
            return true;
        },
        [](auto&&) {
            return false;
        }
    }, *this);
}

bool host_t::
is_domain() const
{
    return std::holds_alternative<domain_t>(*this);
}

bool host_t::
is_ipv4_address() const
{
    return std::holds_alternative<ipv4_address_t>(*this);
}

bool host_t::
is_ipv6_address() const
{
    return std::holds_alternative<ipv6_address_t>(*this);
}

bool host_t::
is_opaque_host() const
{
    return std::holds_alternative<opaque_host_t>(*this);
}

bool host_t::
is_localhost() const
{
    // spec say only "localhost" but check IPv4, IPv6 anyway
    return std::visit(overloaded {
        [](domain_t const& s) {
            return s == "localhost";
        },
        [](ipv4_address_t const& a) {
            return a == 127 * 256 * 256 * 256 + 1; // 127.0,0,1
        },
        [](ipv6_address_t const& a) { // ::1
            for (size_t i = 0; i < 7; ++i) {
                if (a[i] != 0) return false;
            }
            return a[7] == 1;
        },
        [](auto&&) { return false; }
    }, *this);
}

byte_sequence_view_t host_t::
domain() const
{
    return std::get<domain_t>(*this);
}

ipv4_address_t host_t::
ipv4_address() const
{
    return std::get<ipv4_address_t>(*this);
}

ipv6_address_t host_t::
ipv6_address() const
{
    return std::get<ipv6_address_t>(*this);
}

byte_sequence_view_t host_t::
opaque_host() const
{
    return std::get<opaque_host_t>(*this);
}

void host_t::
set_empty_string()
{
    *this = empty_host_t();
}

/*
 * host_parser
 */
host_parser::host_parser() = default;

host_parser::
host_parser(error_handler_t const h) : m_error_handler { h } {}

std::optional<host_t> host_parser::
operator()(string_view_t const input, bool is_not_special/*= false*/)
{
    m_input = input;
    return parse_host(input, is_not_special);
}

std::optional<host_t> host_parser::
parse_host(string_view_t input, bool const is_not_special/*= false*/) const
{
#ifdef VERBOSE
    std::cout << "parse_host(" << input << ", " << is_not_special << ")\n";
#endif
    assert(is_not_special || !input.empty());

    auto constexpr failure = std::nullopt;

    if (starts_with(input, U'[')) {
        if (!ends_with(input, U']')) {
            validation_error(errc::unmatch_bracket);
            return failure;
        }

        input.remove_prefix(1);
        input.remove_suffix(1);

        return parse_ipv6(input);
    }

    if (is_not_special) {
        return parse_opaque_host(input);
    }

    auto const domain = decode_utf8_without_bom(string_percent_decode(input));

    auto const ascii_domain = domain_to_ascii(domain);
    if (!ascii_domain) {
        validation_error(ascii_domain.error());
        return failure;
    }

    if (contains(*ascii_domain, is_forbidden_host_code_point<byte_t>)) {
        validation_error(errc::invalid_code_point);
        return failure;
    }

    return parse_ipv4(*ascii_domain);
}

std::optional<int64_t> host_parser::
parse_ipv4_number(byte_sequence_view_t input, bool& validation_error_flag) const
{
#ifdef VERBOSE
    std::cout << "parse_ipv4_number(" << input <<  ")\n";
#endif
    auto constexpr failure = std::nullopt;

    auto r = 10;

    if (input.size() >= 2 && istarts_with(input, "0x")) {
        validation_error_flag = true;
        input.remove_prefix(2);
        r = 16;
    }
    else if (input.size() >= 2 && starts_with(input, '0')) {
        validation_error_flag = true;
        input.remove_prefix(1);
        r = 8;
    }

    if (input.empty()) return 0;

    auto is_not_radix_digit = [&](auto const c) {
        switch (r) {
            case 10:
                return !infra::is_ascii_digit(c);
            case 16:
                return !infra::is_ascii_hex_digit(c);
            case 8:
                return !('0' <= c && c <= '7');
            default:
                assert(false);
        }
    };

    if (contains(input, is_not_radix_digit)) {
        return failure;
    }

    return digits_to_number(input, r);
}

std::optional<host_t> host_parser::
parse_ipv4(byte_sequence_view_t const input) const
{
#ifdef VERBOSE
    std::cout << "parse_ipv4(" << input <<  ")\n";
#endif
    auto constexpr failure = std::nullopt;

    auto validation_error_flag = false;
    auto parts = split(input, '.');
    assert(parts.size() >= 1);

    if (parts.back().empty()) {
        validation_error_flag = true;
        if (parts.size() > 1) {
            parts.pop_back();
        }
    }

    if (parts.size() > 4) return domain_t { input };

    std::vector<int64_t> numbers;

    for (auto const part: parts) {
        if (part.empty()) return domain_t { input };

        auto const n = parse_ipv4_number(part, validation_error_flag);
        if (!n) return domain_t { input };

        numbers.push_back(*n);
    }

    if (validation_error_flag) {
        validation_error(errc::invalid_ipv4_number);
    }

    auto is_greater_than_255 = [](auto const v) { return v > 255; };
    if (any_of(numbers, is_greater_than_255)) {
        validation_error(errc::invalid_ipv4_number);
    }

    if (any_but_last_item_of(numbers, is_greater_than_255)) {
        return failure;
    }

    auto max_value = [&]() {
        return static_cast<int64_t>(std::pow(256, 5 - numbers.size()));
    };

    if (numbers.back() >= max_value()) {
        validation_error(errc::invalid_ipv4_number);
        return failure;
    }

    auto ipv4 = static_cast<ipv4_address_t>(numbers.back());
    numbers.pop_back();

    assert(numbers.size() <= 4);
    auto counter = 0;
    for (auto const n: numbers) {
        ipv4 += static_cast<ipv4_address_t>(n) *
                static_cast<ipv4_address_t>(std::pow(256, 3 - counter));
        ++counter;
    }

    return ipv4;
}

std::optional<host_t> host_parser::
parse_ipv6(string_view_t const input) const
{
#ifdef VERBOSE
    std::cout << "parse_ipv6(" << input <<  ")\n";
#endif
    auto constexpr null = -1;
    auto constexpr failure = std::nullopt;

    ipv6_address_t address {};
    auto piece_index = 0;
    auto compress = null;
    auto pointer = input.begin();

    auto c = [&]() {
        return pointer != input.end() ? *pointer : eof;
    };
    auto remaining = [&]() {
        return string_view_t { pointer + 1, input.end() };
    };

    if (c() == ':') {
        if (!starts_with(remaining(), U':')) {
            validation_error(errc::no_second_colon);
            return failure;
        }
        pointer += 2;

        ++piece_index;
        compress = piece_index;
    }

    while (pointer != input.end()) {
        if (piece_index == 8) {
            validation_error(errc::wrong_number_of_ipv6_pieces);
            return failure;
        }
        if (c() == ':') {
            if (compress != null) {
                validation_error(errc::second_compress_segment);
                return failure;
            }

            ++pointer;
            ++piece_index;
            compress = piece_index;
            continue;
        }

        auto value = 0, length = 0;
        while (length < 4 && infra::is_ascii_hex_digit(c())) {
            value = value * 0x10 + hex_digit_to_number(c());
            ++pointer;
            ++length;
        }

        if (c() == '.') {
            if (length == 0) {
                validation_error(errc::invalid_period);
                return failure;
            }
            pointer -= length;

            if (piece_index > 6) {
                validation_error(errc::wrong_number_of_ipv6_pieces);
                return failure;
            }

            auto numbers_seen = 0;
            while (c() != eof) {
                auto ipv4_piece = null;
                if (numbers_seen > 0) {
                    if (c() == '.' && numbers_seen < 4) {
                        ++pointer;
                    }
                    else {
                        validation_error(errc::wrong_number_of_ipv4_pieces);
                        return failure;
                    }
                }

                if (!infra::is_ascii_digit(c())) {
                    validation_error(errc::invalid_ipv4_char);
                    return failure;
                }

                while (infra::is_ascii_digit(c())) {
                    auto number = decimal_digit_to_number(c());

                    if (ipv4_piece == null) {
                        ipv4_piece = number;
                    }
                    else if (ipv4_piece == 0){
                        validation_error(errc::wrong_number_of_ipv4_pieces);
                        return failure;
                    }
                    else {
                        ipv4_piece = ipv4_piece * 10 + number;
                    }

                    if (ipv4_piece > 255) {
                        validation_error(errc::too_big_ipv4_piece);
                        return failure;
                    }

                    ++pointer;
                }
                assert(ipv4_piece != null);

                address[to_size(piece_index)] =
                    static_cast<ipv6_piece_t>(
                        address[to_size(piece_index)] * 0x100 + ipv4_piece);
                ++numbers_seen;

                if (numbers_seen == 2 || numbers_seen == 4) {
                    ++piece_index;
                }
            }

            if (numbers_seen != 4) {
                validation_error(errc::wrong_number_of_ipv4_pieces);
                return failure;
            }
            break;
        } // if (c() == '.')
        else if (c() == ':') {
            ++pointer;
            if (c() == eof) {
                validation_error(errc::ipv6_end_with_colon);
                return failure;
            }
        }
        else if (c() != eof) {
            validation_error(errc::premature_eof);
            return failure;
        }

        address[to_size(piece_index)] = static_cast<ipv6_piece_t>(value);
        ++piece_index;
    }

    if (compress != null) {
        auto swaps = piece_index - compress;
        piece_index = 7;

        while (piece_index != 0 && swaps > 0) {
            std::swap(address[to_size(piece_index)],
                      address[to_size(compress + swaps - 1)]);
            --piece_index;
            --swaps;
        }
    }
    else if (compress == null && piece_index != 8){
        validation_error(errc::wrong_number_of_ipv6_pieces);
        return failure;
    }

    return address;
}

std::optional<host_t> host_parser::
parse_opaque_host(string_view_t const input) const
{
    auto constexpr failure = std::nullopt;

    auto is_invalid_code_point = [](auto const c) {
        return c != '%' && is_forbidden_host_code_point(c);
    };

    if (contains(input, is_invalid_code_point)) {
        validation_error(errc::invalid_code_point);
        return failure;
    }

    string_t output;
    for (auto const c: input) {
        append(output,
            utf8_percent_encode(c,
                is_c0_control_percent_encode_char<code_point_t>));
    }

    opaque_host_t host;
    assign(host, output);

    return host;
}

void host_parser::
validation_error(std::error_code const& e) const
{
    if (m_error_handler) {
        m_error_handler(e, m_input, 0);
    }
}

std::optional<host_t>
parse_host(string_view_t const input, bool const is_not_special/*= false*/)
{
    host_parser parser;
    return parser(input, is_not_special);
}

byte_sequence_t
serialize_host(host_t const& host)
{
    assert(!host.is_null()); // spec doesn't have this assumption but necessary

    return std::visit(overloaded {
        [](ipv4_address_t const address) {
            return serialize_ipv4(address);
        },
        [](ipv6_address_t const& address) {
            byte_sequence_t result = "[";

            append(result, serialize_ipv6(address));
            append(result, ']');

            return result;
        },
        [](domain_t const& host) -> byte_sequence_t { return host; },
        [](opaque_host_t const& host) -> byte_sequence_t { return host; },
        [](empty_host_t) -> byte_sequence_t { return ""; },
        [](null_host_t) -> byte_sequence_t { assert(false); }
    }, host);
}

byte_sequence_t
serialize_ipv4(ipv4_address_t const address)
{
    byte_sequence_t output;

    auto n = address;

    for (auto i = 1; i <= 4; ++i) {
        prepend(output, serialize_integer(n % 256));

        if (i != 4) {
            prepend(output, '.');
        }

        n = static_cast<ipv4_address_t>(std::floor(n / 256));
    }

    return output;
}

byte_sequence_t
serialize_ipv6(ipv6_address_t const& address)
{
    byte_sequence_t output;

    auto const compress = first_index_of_longest_consecutive_zeros(address);

    auto ignore0 = false;

    for (auto piece_index = 0; piece_index <= 7; ++piece_index) {
        if (ignore0 && address[to_size(piece_index)] == 0) {
            continue;
        }
        else if (ignore0) {
            ignore0 = false;
        }

        if (compress == piece_index) {
            if (piece_index == 0) {
                append(output, "::");
            }
            else {
                append(output, ":");
            }

            ignore0 = true;
            continue;
        }

        append(output,
               to_shortest_lower_hex_digits(address[to_size(piece_index)]));

        if (piece_index != 7) {
            append(output, ':');
        }
    }

    return output;
}

} // namespace whatwg::url
