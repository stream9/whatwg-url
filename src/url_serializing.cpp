#include <whatwg/url/url_serializing.hpp>

#include <whatwg/url/misc.hpp>

#include <array>
#include <type_traits>

namespace whatwg::url {

template<typename T>
struct has_custom_serializer
{
    template<typename U>
    static auto check(U const& u) -> decltype(serialize(u, true), char(0));

    static int check(...);

    static const bool value = (sizeof(check(T())) == sizeof(char));
};

template<typename UrlRecord,
    typename = std::enable_if_t<
        has_custom_serializer<UrlRecord>::value >>
auto
do_serialize_url(UrlRecord const& url, bool const exclude_fragment)
{
    return serialize(url, exclude_fragment);
}

template<typename UrlRecord,
    typename = std::enable_if_t<
        !has_custom_serializer<UrlRecord>::value >>
byte_sequence_t
do_serialize_url(UrlRecord const& url, bool const exclude_fragment)
{
    byte_sequence_t output { url.scheme() };

    append(output, ':');

    if (!url.host().is_null()) {
        append(output, "//");

        if (includes_credentials(url)) {
            append(output, url.username());

            if (!url.password().empty()) {
                append(output, ':');
                append(output, url.password());
            }

            append(output, '@');
        }

        append(output, serialize_host(url.host()));

        if (url.port()) {
            append(output, ':');
            append(output, serialize_integer(*url.port()));
        }
    }
    else if (url.scheme() == "file") {
        append(output, "//");
    }

    if (url.cannot_be_a_base_url()) {
        append(output, url.path()[0]);
    }
    else {
        append(output, join_path(url.path(), '/'));
    }

    if (url.query()) {
        append(output, '?');
        append(output, *url.query());
    }

    if (!exclude_fragment && url.fragment()) {
        append(output, '#');
        append(output, *url.fragment());
    }

    return output;
}

byte_sequence_t
serialize_url(url_record const& url, bool const exclude_fragment/* = false*/)
{
    return byte_sequence_t { do_serialize_url(url, exclude_fragment) };
}

} // namespace whatwg::url
