#include <whatwg/url/error.hpp>

#include <sstream>

namespace whatwg::url {

static std::string
unknown_error(int const code)
{
    std::ostringstream oss;
    oss << "unknown error: " << code;

    return oss.str();
}

class url_category_impl : public std::error_category
{
public:
    char const* name() const noexcept override
    {
        return "URL";
    }

    std::string message(int code) const override;
};

std::error_category const&
url_category()
{
    static url_category_impl impl;

    return impl;
}

std::string url_category_impl::
message(int const code) const
{
    using e = errc;

    switch (static_cast<e>(code)) {
        // basic_url_parser
        case e::input_contains_leading_or_trailing_c0_control_or_space:
            return "input contains leading or trailing C0 control or space";
        case e::input_contains_ascii_tab_or_newline:
            return "input contains ASCII tab or newline";
        case e::illegal_character:
            return "illegal character";
        case e::file_url_doesnt_start_with_double_slash:
            return "file URL doesn't start with double slash";
        case e::no_scheme_without_base_url:
            return "no scheme without base URL";
        case e::special_relative_url:
            return "relative URL with special scheme";
        case e::relative_starts_with_backslash:
            return "relative URL starts with backslash";
        case e::authority_doesnt_start_with_slash:
            return "authority doesn't start with slash";
        case e::contains_user_info:
            return "URL has user information";
        case e::empty_host_with_user_info:
            return "empty host with user information";
        case e::empty_host_with_port:
            return "empty host with port";
        case e::empty_host_with_user_info_or_port:
            return "empty host with user information or port";
        case e::empty_host_with_special_scheme:
            return "empty host with special scheme";
        case e::invalid_port:
            return "invalid port";
        case e::file_url_starts_with_backslash:
            return "file URL starts with backslash";
        case e::file_url_starts_with_windows_drive_letter:
            return "file URL starts with Windows drive letter";
        case e::special_url_has_backslash_in_path:
            return "special URL has backslash in its path";
        case e::file_url_has_host_and_windows_drive_letter:
            return "file URL has host and Windows drive letter";
        case e::file_url_has_empty_path_elements:
            return "file URL has empty path elements";
        case e::invalid_percent_encoded_char:
            return "invalid percent encoded character";
        case e::null_in_fragment:
            return "null in fragment";
        // host_parser
        case e::unmatch_bracket:
            return "unmatch bracket";
        case e::invalid_code_point:
            return "invalid code point";
        case e::invalid_ipv4_number:
            return "invalid IPV4 number";
        case e::no_second_colon:
            return "no second colon";
        case e::second_compress_segment:
            return "multiple compress segment";
        case e::invalid_period:
            return "invalid period";
        case e::invalid_ipv4_char:
            return "invalid IPv4 character";
        case e::too_big_ipv4_piece:
            return "too big IPv4 piece";
        case e::wrong_number_of_ipv4_pieces:
            return "wrong number of IPv4 piece";
        case e::ipv6_end_with_colon:
            return "IPv6 ends with colon";
        case e::premature_eof:
            return "premature eof";
        case e::wrong_number_of_ipv6_pieces:
            return "wrong number of IPv6 pieces";
        default:
            return unknown_error(code);
    }
}

} // namespace whatwg::url
