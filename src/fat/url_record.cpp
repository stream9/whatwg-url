#include <whatwg/url/fat/url_record.hpp>

#include <whatwg/url/character_encoding.hpp>

namespace whatwg::url::fat {

using url::append; // bring in shadowed overloads

void
append(path_t& path, byte_sequence_view_t const s)
{
    path.emplace_back(s);
}

void
append(path_t& path, string_view_t const s)
{
    append(path, isomorphic_encode(s));
}

void
append(path_t& path, size_t const index, string_view_t const s)
{
    append(path[index], s);
}

void
pop_front(path_t& path)
{
    if (path.empty()) return;
    path.erase(path.begin());
}

bool
is_null(std::optional<byte_sequence_t> const& s)
{
    return !s;
}

bool
is_empty(std::optional<byte_sequence_t> const& s)
{
    assert(s);
    return s->empty();
}

byte_sequence_view_t
value_or(std::optional<byte_sequence_t> const& str,
         byte_sequence_view_t const def_value)
{
    return str ? *str : def_value;
}

byte_sequence_t
join_path(path_t const& path, byte_t const separator)
{
    byte_sequence_t result;

    for (auto const& s: path) {
        append(result, separator);
        append(result, s);
    }

    return result;
}

} // namespace whatwg::url::fat
