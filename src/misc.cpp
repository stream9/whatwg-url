#include <whatwg/url/misc.hpp>

#include <whatwg/url/string.hpp>

#include <whatwg/infra/code_point.hpp>

#include <cassert>
#include <cctype>

namespace whatwg::url {

// 1. Infrastructure
byte_sequence_t
serialize_integer(int const i)
{
    return std::to_string(i);
}

bool
is_special_scheme(string_view_t const s)
{
    return s == U"ftp" || s == U"file" || s == U"gopher" ||
           s == U"http" || s == U"https" || s == U"ws" || s == U"wss";
}

bool
is_special_scheme(byte_sequence_view_t const s)
{
    return s == "ftp" || s == "file" || s == "gopher" ||
           s == "http" || s == "https" || s == "ws" || s == "wss";
}

std::optional<uint16_t>
default_port(byte_sequence_view_t const scheme)
{
    if (scheme == "ftp") {
        return 21;
    }
    if (scheme == "file") {
        return std::nullopt;
    }
    if (scheme == "gopher") {
        return 70;
    }
    if (scheme == "http") {
        return 80;
    }
    if (scheme == "https") {
        return 443;
    }
    if (scheme == "ws") {
        return 80;
    }
    if (scheme == "wss") {
        return 443;
    }

    return std::nullopt;
}

bool
is_special(url_record const& url)
{
    return is_special_scheme(url.scheme());
}

bool
includes_credentials(url_record const& url)
{
    return !url.username().empty() || !url.password().empty();
}

bool
cannot_have_a_username_password_port(url_record const& url)
{
    auto const& host = url.host();
    return host.is_null() || host.is_empty() || url.cannot_be_a_base_url() ||
           url.scheme() == "file";
}

template<typename StringView>
bool
is_windows_drive_letter(StringView const s)
{
    if (s.size() < 2) return false;

    return infra::is_ascii_alpha(s[0]) &&
           (s[1] == ':' || s[1] == '|');
}

bool
is_windows_drive_letter(string_view_t const s)
{
    return is_windows_drive_letter<string_view_t>(s);
}

template<typename StringView>
bool
is_normalized_windows_drive_letter(StringView const s)
{
    return is_windows_drive_letter(s) && s[1] == ':';
}

bool
is_normalized_windows_drive_letter(byte_sequence_view_t const s)
{
    return is_normalized_windows_drive_letter<byte_sequence_view_t>(s);
}

template<typename StringView>
bool
starts_with_windows_drive_letter(StringView const s)
{
    return (s.size() >= 2)
        && (is_windows_drive_letter(s.substr(0, 2)))
        && (s.size() == 2 || s[2] == '/' || s[2] == '\\' || s[2] == '?' ||
            s[2] == '#');
}

bool
starts_with_windows_drive_letter(string_view_t const s)
{
    return starts_with_windows_drive_letter<string_view_t>(s);
}

void
shorten_urls_path(url_record& url)
{
    auto&& path = url.path();

    if (path.empty()) return;

    if (url.scheme() == "file" && path.size() == 1 &&
                   is_normalized_windows_drive_letter(path[0]))
    {
        return;
    }

    path.pop_back();
}

} // namespace whatwg::url
