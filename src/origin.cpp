#include <whatwg/url/origin.hpp>

#include <whatwg/url/basic_url_parser.hpp>
#include <whatwg/url/misc.hpp>

namespace whatwg::url {

origin::
origin(url_record const& url)
{
    init(url);
}

byte_sequence_t origin::
serialize() const
{
    if (is_opaque()) {
        return "null";
    }
    else {
        byte_sequence_t result { m_scheme };
        append(result, "://");
        append(result, m_host);
        if (!m_port.empty()) {
            append(result, ':');
            append(result, m_port);
        }

        return result;
    }
}

void origin::
init(url_record const& url)
{
    auto const& scheme = url.scheme();

    if (scheme == "blob") {
        auto const path = url.path();
        auto const inner = path.empty() ? "" : path[0];

        auto const& inner_url = basic_url_parse(inner);
        if (!inner_url) {
            return; // opaque
        }
        else {
            init(*inner_url);
            return;
        }
    }
    else if (scheme == "ftp" || scheme == "gopher" || scheme == "http" ||
             scheme == "https" || scheme == "ws" || scheme == "wss")
    {
        m_scheme = scheme;
        m_host = serialize_host(url.host());
        if (url.port()) {
            m_port = serialize_integer(*url.port());
        }
        return;
    }
    else if (scheme == "file") {
        return; // opaque
    }
    else {
        return; // opaque
    }
}

} // namespace whatwg::url
