#include <whatwg/url/basic_url_parser.hpp>

#include <whatwg/url/character_encoding.hpp>
#include <whatwg/url/classification.hpp>
#include <whatwg/url/conversion.hpp>
#include <whatwg/url/ctype.hpp>
#include <whatwg/url/error.hpp>
#include <whatwg/url/host.hpp>
#include <whatwg/url/misc.hpp>
#include <whatwg/url/percent_encoding.hpp>
#include <whatwg/url/string.hpp>
#include <whatwg/url/utility.hpp>

#include <whatwg/infra/code_point.hpp>

#include <whatwg/encoding/encoding.hpp>

#include <boost/algorithm/string/trim.hpp>

//#define VERBOSE

#ifdef VERBOSE
#include <iostream>
#endif

namespace whatwg::url {

using namespace std::literals;

using result_t = basic_url_parser::result_t;
using state_t = basic_url_parser::state;

auto constexpr success = result_t::success;
auto constexpr failure = result_t::failure;
auto constexpr go_on = result_t::go_on;

static bool
is_double_dot_path_segment(string_view_t const s)
{
    using boost::iequals;

    return s == ".." || iequals(s, U".%2e")
                     || iequals(s, U"%2e.") || iequals(s, U"%2e%2e");
}

static bool
is_single_dot_path_segment(string_view_t const s)
{
    return s == "." || boost::iequals(s, U"%2e");
}

static bool
starts_with_two_ascii_hex_digits(string_view_t const s)
{
    if (s.size() < 2) return false;
    return infra::is_ascii_hex_digit(s[0]) && infra::is_ascii_hex_digit(s[1]);
}

static bool
remove_leading_or_trailing_c0_control_or_space(string_t& input)
{
    auto const before = input.size();

    boost::trim_if(input, [](auto c) {
        return infra::is_c0_control_or_space(c); }
    );

    return input.size() != before;
}

static bool
remove_all_ascii_tab_or_newline(string_t& input)
{
    auto const before = input.size();

    input.erase(
        std::remove_if( // LCOV_EXCL_LINE
            input.begin(), input.end(),
            [](auto c) { return infra::is_ascii_tab_or_newline(c); }
        ),
        input.end()
    );

    return input.size() != before;
}

/*
 * basic_url_parser
 */
basic_url_parser::
basic_url_parser()
{
    if (!std::has_facet<std::ctype<code_point_t>>(std::locale())) {
        std::locale loc { std::locale(), new std::ctype<code_point_t> };
        std::locale::global(loc);
    }
}

void basic_url_parser::
set_validation_error_handler(error_handler_t handler)
{
    m_error_handler = std::move(handler);
}

std::optional<url_record> basic_url_parser::
operator()(byte_sequence_view_t const input,
           url_record const* const base/*= nullptr*/,
           std::optional<byte_sequence_view_t> const encoding_override/*= {}*/)
{
    m_input = decode_utf8_without_bom(input);
    m_base = base;

    url_record result;
    m_url = &result;

    if (remove_leading_or_trailing_c0_control_or_space(m_input)) {
        validation_error(errc::input_contains_leading_or_trailing_c0_control_or_space);
    }

    if (remove_all_ascii_tab_or_newline(m_input)) {
        validation_error(errc::input_contains_ascii_tab_or_newline);
    }

    if (encoding_override) {
        auto* const e = encoding::get_an_encoding(*encoding_override);
        if (e) {
            m_encoding = &encoding::get_an_output_encoding(*e);
        }
    }

    if (!m_encoding) {
        m_encoding = encoding::get_an_encoding("UTF-8");
    }
    assert(m_encoding);

    m_state = state::scheme_start;

    if (run() == success) {
        return result;
    }
    else {
        return std::nullopt;
    }
}

void basic_url_parser::
operator()(byte_sequence_view_t const input,
           url_record& url,
           state const state_override,
           url_record const* const base/*= nullptr*/,
           std::optional<byte_sequence_view_t> const encoding_override/*= {}*/)
{
    m_input = decode_utf8_without_bom(input);
    m_base = base;

    m_url = &url;

    if (remove_all_ascii_tab_or_newline(m_input)) {
        validation_error(errc::input_contains_ascii_tab_or_newline);
    }

    m_state_override = m_state = state_override;

    if (encoding_override) {
        auto* const e = encoding::get_an_encoding(*encoding_override);
        if (e) {
            m_encoding = &encoding::get_an_output_encoding(*e);
        }
    }

    if (!m_encoding) {
        m_encoding = encoding::get_an_encoding("UTF-8");
    }
    assert(m_encoding);

    run();
}

result_t basic_url_parser::
run()
{
    assert(m_url);

    m_pointer = m_input.begin();

    auto c = [&]() {
        return m_pointer != m_input.end() ?  *m_pointer : eof;
    };

    while (true) {
#ifdef VERBOSE
        std::cout << "state: " << m_state
                  << ", c: ";
        if (c() == eof) {
            std::cout << "<eof>";
        }
        else {
            std::cout << static_cast<char>(c());
        }
        std::cout << "\n";
#endif
        auto func = lookup_state_handler(m_state);
        auto const result = (this->*func)(c());
        if (result != go_on) return result;

        if (c() == eof) {
            break;
        }
        else {
            ++m_pointer;
        }
    }

    return success;
}

result_t basic_url_parser::
scheme_start(code_point_t const c)
{
    if (infra::is_ascii_alpha(c)) {
        append(m_buffer, infra::to_ascii_lower(c));
        m_state = state::scheme;
    }
    else if (!m_state_override) {
        m_state = state::no_scheme;
        --m_pointer;
    }
    else {
        validation_error(errc::illegal_character);
        return failure;
    }

    return go_on;
}

result_t basic_url_parser::
scheme(code_point_t const c)
{
    if (infra::is_ascii_alphanumeric(c) || c == '+' || c == '-' || c == '.') {
        append(m_buffer, infra::to_ascii_lower(c));
    }
    else if (c == ':') {
        if (m_state_override) {
            if (is_special(*m_url) && !is_special_scheme(m_buffer)) {
                return success;
            }
            if (!is_special(*m_url) && is_special_scheme(m_buffer)) {
                return success;
            }
            if ((includes_credentials(*m_url) || m_url->port()) &&
                m_buffer == "file")
            {
                return success;
            }
            if (m_url->scheme() == "file" &&
                (m_url->host().is_empty() || m_url->host().is_null()))
            {
                return success;
            }
        }

        assign(m_url->scheme(), m_buffer);

        if (m_state_override) {
            if (m_url->port() &&
                *m_url->port() == default_port(m_url->scheme()))
            {
                m_url->port().reset();
            }
            return success;
        }

        m_buffer.clear();

        if (m_url->scheme() == "file") {
            if (!starts_with(remaining(), U"//")) {
                validation_error(errc::file_url_doesnt_start_with_double_slash);
            }

            m_state = state::file;
        }
        else if (is_special(*m_url)
                        && m_base && m_base->scheme() == m_url->scheme())
        {
            assert(!m_base->cannot_be_a_base_url());
            m_state = state::special_relative_or_authority;
        }
        else if (is_special(*m_url)) {
            m_state = state::special_authority_slashes;
        }
        else if (starts_with(remaining(), U'/')) {
            m_state = state::path_or_authority;
            ++m_pointer;
        }
        else {
            m_url->cannot_be_a_base_url() = true;
            append(m_url->path(), "");
            m_state = state::cannot_be_a_base_url_path;
        }
    }
    else if (!m_state_override) {
        m_buffer.clear();
        m_state = state::no_scheme;
        m_pointer = m_input.begin() - 1; // need to be one char before begin, because it will be inclimented in run()
    }
    else {
        validation_error(errc::illegal_character);
        return failure;
    }

    return go_on;
}

result_t basic_url_parser::
no_scheme(code_point_t const c)
{
    if (!m_base || (m_base->cannot_be_a_base_url() && c != '#')) {
        validation_error(errc::no_scheme_without_base_url);
        return failure;
    }
    else if (m_base && m_base->cannot_be_a_base_url() && c == '#') {
        m_url->scheme() = m_base->scheme();
        m_url->path() = m_base->path();
        m_url->query() = m_base->query();
        m_url->fragment() = "";
        m_url->cannot_be_a_base_url() = true;

        m_state = state::fragment;
    }
    else if (m_base && m_base->scheme() != "file"){
        m_state = state::relative;
        --m_pointer;
    }
    else {
        m_state = state::file;
        --m_pointer;
    }

    return go_on;
}

result_t basic_url_parser::
special_relative_or_authority(code_point_t const c)
{
    if (c == '/' && starts_with(remaining(), U'/')) {
        m_state = state::special_authority_ignore_slashes;
        ++m_pointer;
    }
    else {
        validation_error(errc::special_relative_url);
        m_state = state::relative;
        --m_pointer;
    }

    return go_on;
}

result_t basic_url_parser::
path_or_authority(code_point_t const c)
{
    if (c == '/') {
        m_state = state::authority;
    }
    else {
        m_state = state::path;
        --m_pointer;
    }

    return go_on;
}

result_t basic_url_parser::
relative(code_point_t const c)
{
    assert(m_base);

    m_url->scheme() = m_base->scheme();

    switch (c) {
        case eof:
            m_url->username() = m_base->username();
            m_url->password() = m_base->password();
            m_url->host() = m_base->host();
            m_url->port() = m_base->port();
            m_url->path() = m_base->path();
            m_url->query() = m_base->query();
            break;
        case '/':
            m_state = state::relative_slash;
            break;
        case '?':
            m_url->username() = m_base->username();
            m_url->password() = m_base->password();
            m_url->host() = m_base->host();
            m_url->port() = m_base->port();
            m_url->path() = m_base->path();
            m_url->query() = "";

            m_state = state::query;
            break;
        case '#':
            m_url->username() = m_base->username();
            m_url->password() = m_base->password();
            m_url->host() = m_base->host();
            m_url->port() = m_base->port();
            m_url->path() = m_base->path();
            m_url->query() = m_base->query();
            m_url->fragment() = "";

            m_state = state::fragment;
            break;
        default:
            if (is_special(*m_url) && c == '\\') {
                validation_error(errc::relative_starts_with_backslash);

                m_state = state::relative_slash;
            }
            else {
                m_url->username() = m_base->username();
                m_url->password() = m_base->password();
                m_url->host() = m_base->host();
                m_url->port() = m_base->port();
                m_url->path() = m_base->path();

                if (!m_url->path().empty()) {
                    m_url->path().pop_back();
                }

                m_state = state::path;
                --m_pointer;
            }
            break;
    }

    return go_on;
}

result_t basic_url_parser::
relative_slash(code_point_t const c)
{
    if (is_special(*m_url) && (c == '/' || c == '\\')) {
        if (c == '\\') {
            validation_error(errc::relative_starts_with_backslash);
        }

        m_state = state::special_authority_ignore_slashes;
    }
    else if (c == '/') {
        m_state = state::authority;
    }
    else {
        m_url->username() = m_base->username();
        m_url->password() = m_base->password();
        m_url->host() = m_base->host();
        m_url->port() = m_base->port();

        m_state = state::path;
        --m_pointer;
    }

    return go_on;
}

result_t basic_url_parser::
special_authority_slashes(code_point_t const c)
{
    if (c == '/' && starts_with(remaining(), U'/')) {
        m_state = state::special_authority_ignore_slashes;
        ++m_pointer;
    }
    else {
        validation_error(errc::authority_doesnt_start_with_slash);
        m_state = state::special_authority_ignore_slashes;
        --m_pointer;
    }

    return go_on;
}

result_t basic_url_parser::
special_authority_ignore_slashes(code_point_t const c)
{
    if (c != '/' && c != '\\') {
        m_state = state::authority;
        --m_pointer;
    }
    else {
        validation_error(errc::authority_doesnt_start_with_slash);
    }

    return go_on;
}

result_t basic_url_parser::
authority(code_point_t const c)
{
    if (c == '@') {
        validation_error(errc::contains_user_info);
        if (m_at_flag) {
            prepend(m_buffer, U"%40");
        }
        m_at_flag = true;

        for (auto const code_point: m_buffer) {
            if (code_point == ':' && !m_password_token_seen_flag) {
                m_password_token_seen_flag = true;
                continue;
            }

            auto const encoded_ascii_chars = utf8_percent_encode(
                            code_point, is_userinfo_percent_encode_char);

            if (m_password_token_seen_flag) {
                append(m_url->password(), encoded_ascii_chars);
            }
            else {
                append(m_url->username(), encoded_ascii_chars);
            }
        }

        m_buffer.clear();
    }
    else if ((c == eof || c == '/' || c == '?' || c == '#')
              || (is_special(*m_url) && c == '\\'))
    {
        if (m_at_flag && m_buffer.empty()) {
            validation_error(errc::empty_host_with_user_info);
            return failure;
        }

        m_pointer -= to_diff(m_buffer.size() + 1);
        m_buffer.clear();

        m_state = state::host;
    }
    else {
        append(m_buffer, c);
    }

    return go_on;
}

result_t basic_url_parser::
hostname(code_point_t const c)
{
    if (m_state_override && m_url->scheme() == "file") {
        --m_pointer;
        m_state = state::file_host;
    }
    else if (c == ':' && !m_bracket_flag) {
        if (m_buffer.empty()) {
            validation_error(errc::empty_host_with_port);
            return failure;
        }

        host_parser parse_host { m_error_handler };
        auto const host = parse_host(m_buffer, !is_special(*m_url));
        if (!host) {
            return failure;
        }

        m_url->host() = *host;
        m_buffer.clear();
        m_state = state::port;

        if (m_state_override && *m_state_override == state::hostname) {
            return success;
        }
    }
    else if ((c == eof || c == '/' || c == '?' || c == '#') ||
             (is_special(*m_url) && c == '\\'))
    {
        --m_pointer;

        if (is_special(*m_url) && m_buffer.empty()) {
            validation_error(errc::empty_host_with_special_scheme);
            return failure;
        }
        else if (m_state_override && m_buffer.empty() &&
                 (includes_credentials(*m_url) || m_url->port()))
        {
            validation_error(errc::empty_host_with_user_info_or_port);
            return success;
        }

        host_parser parse_host { m_error_handler };
        auto const host = parse_host(m_buffer, !is_special(*m_url));
        if (!host) {
            return failure;
        }

        m_url->host() = *host;
        m_buffer.clear();

        m_state = state::path_start;
        if (m_state_override) return success;
    }
    else {
        if (c == '[') {
            m_bracket_flag = true;
        }
        else if (c == ']') {
            m_bracket_flag = false;
        }

        append(m_buffer, c);
    }

    return go_on;
}

result_t basic_url_parser::
port(code_point_t const c)
{
    if (infra::is_ascii_digit(c)) {
        append(m_buffer, c);
    }
    else if ((c == eof || c == '/' || c == '?' || c == '#')
             || (is_special(*m_url) && c == '\\')
             || m_state_override)
    {
        if (!m_buffer.empty()) {
            auto const port = decimal_digits_to_number(m_buffer);
            if (!port || *port > ((1 << 16) - 1)) {
                validation_error(errc::invalid_port);
                return failure;
            }

            if (*port == default_port(m_url->scheme())) {
                m_url->port().reset();
            }
            else {
                m_url->port() = static_cast<uint16_t>(*port);
            }

            m_buffer.clear();
        }

        if (m_state_override) return success;

        m_state = state::path_start;
        --m_pointer;
    }
    else {
        validation_error(errc::illegal_character);
        return failure;
    }

    return go_on;
}

result_t basic_url_parser::
file(code_point_t const c)
{
    m_url->scheme() = "file";

    if (c == '/' || c == '\\') {
        if (c == '\\') validation_error(errc::file_url_starts_with_backslash);

        m_state = state::file_slash;
    }
    else if (m_base && m_base->scheme() == "file") {
        switch (c) {
            case eof:
                m_url->host() = m_base->host();
                m_url->path() = m_base->path();
                m_url->query() = m_base->query();
                break;
            case '?':
                m_url->host() = m_base->host();
                m_url->path() = m_base->path();
                m_url->query() = "";

                m_state = state::query;
                break;
            case '#':
                m_url->host() = m_base->host();
                m_url->path() = m_base->path();
                m_url->query() = m_base->query();
                m_url->fragment() = "";

                m_state = state::fragment;
                break;
            default:
                if (!starts_with_windows_drive_letter(
                        string_view_t(m_pointer, m_input.end())))
                {
                    m_url->host() = m_base->host();
                    m_url->path() = m_base->path();
                    shorten_urls_path(*m_url);
                }
                else {
                    validation_error(errc::file_url_starts_with_windows_drive_letter);
                }

                m_state = state::path;
                --m_pointer;
                break;
        }
    }
    else {
        m_state = state::path;
        --m_pointer;
    }

    return go_on;
}

result_t basic_url_parser::
file_slash(code_point_t const c)
{
    if (c == '/' || c == '\\') {
        if (c == '\\') validation_error(errc::file_url_starts_with_backslash);

        m_state = state::file_host;
    }
    else {
        if (m_base && m_base->scheme() == "file" &&
            !starts_with_windows_drive_letter(
                string_view_t { m_pointer, m_input.end() } ))
        {
            if (!m_base->path().empty() &&
                is_normalized_windows_drive_letter(m_base->path()[0]) )
            {
                append(m_url->path(), m_base->path()[0]);
            }
            else {
                m_url->host() = m_base->host();
            }
        }
        m_state = state::path;
        --m_pointer;
    }

    return go_on;
}

result_t basic_url_parser::
file_host(code_point_t const c)
{
    if (c == eof || c == '/' || c == '\\' || c == '?' || c == '#') {
        --m_pointer;

        if (!m_state_override && is_windows_drive_letter(m_buffer)) {
            m_state = state::path;
        }
        else if (m_buffer.empty()) {
            m_url->host().set_empty_string();
            if (m_state_override) return success;

            m_state = state::path_start;
        }
        else {
            host_parser parse_host { m_error_handler };
            auto host = parse_host(m_buffer, !is_special(*m_url));
            if (!host) return failure;

            if (host->is_localhost()) {
                host->set_empty_string();
            }

            m_url->host() = *host;
            if (m_state_override) return success;

            m_buffer.clear();
            m_state = state::path_start;
        }
    }
    else {
        append(m_buffer, c);
    }

    return go_on;
}

result_t basic_url_parser::
path_start(code_point_t const c)
{
    if (is_special(*m_url)) {
        if (c == '\\') validation_error(errc::special_url_has_backslash_in_path);

        m_state = state::path;

        if (c != '/' && c != '\\') --m_pointer;
    }
    else if (!m_state_override && c == '?') {
        m_url->query() = "";
        m_state = state::query;
    }
    else if (!m_state_override && c == '#') {
        m_url->fragment() = "";
        m_state = state::fragment;
    }
    else if (c != eof) {
        m_state = state::path;

        if (c != '/') --m_pointer;
    }

    return go_on;
}

result_t basic_url_parser::
path(code_point_t const c)
{
    if ((c == eof || c == '/') ||
        (is_special(*m_url) && c == '\\') ||
        (!m_state_override && (c == '?' || c == '#')))
    {
        if (is_special(*m_url) && c == '\\') {
            validation_error(errc::special_url_has_backslash_in_path);
        }

        if (is_double_dot_path_segment(m_buffer)) {
            shorten_urls_path(*m_url);
            if (c != '/' && !(is_special(*m_url) && c == '\\')) {
                append(m_url->path(), "");
            }
        }
        else if (is_single_dot_path_segment(m_buffer) &&
                 (c != '/' && !(is_special(*m_url) && c == '\\')) )
        {
            append(m_url->path(), "");
        }
        else if (!is_single_dot_path_segment(m_buffer)) {
            if (m_url->scheme() == "file" && m_url->path().empty() &&
                is_windows_drive_letter(m_buffer) )
            {
                if (!m_url->host().is_empty() && !m_url->host().is_null()) {
                    validation_error(errc::file_url_has_host_and_windows_drive_letter);
                    m_url->host().set_empty_string();
                }

                m_buffer[1] = ':';
            }

            append(m_url->path(), m_buffer);
        }

        m_buffer.clear();

        if (m_url->scheme() == "file" && (c == eof || c == '?' || c == '#')) {
            auto&& path = m_url->path();

            while (path.size() > 1 && path[0].empty()) {
                validation_error(errc::file_url_has_empty_path_elements);
                pop_front(path);
            }
        }
        if (c == '?') {
            m_url->query() = "";
            m_state = state::query;
        }
        if (c == '#') {
            m_url->fragment() = "";
            m_state = state::fragment;
        }
    }
    else {
        if (!is_url_code_point(c) && c != '%') {
            validation_error(errc::illegal_character);
        }
        if (c == '%' && !starts_with_two_ascii_hex_digits(remaining())) {
            validation_error(errc::invalid_percent_encoded_char);
        }

        append(m_buffer, utf8_percent_encode(c, is_path_percent_encode_char));
    }

    return go_on;
}

result_t basic_url_parser::
cannot_be_a_base_url_path(code_point_t const c)
{
    if (c == '?') {
        m_url->query() = "";
        m_state = state::query;
    }
    else if (c == '#') {
        m_url->fragment() = "";
        m_state = state::fragment;
    }
    else {
        if (c != eof && !is_url_code_point(c) && c != '%') {
            validation_error(errc::illegal_character);
        }

        if (c == '%' && !starts_with_two_ascii_hex_digits(remaining())) {
            validation_error(errc::invalid_percent_encoded_char);
        }

        if (c != eof) {
            assert(!m_url->path().empty());

            append(m_url->path(), 0,
                   utf8_percent_encode(c,
                       is_c0_control_percent_encode_char<code_point_t>));
        }
    }

    return go_on;
}

result_t basic_url_parser::
query(code_point_t const c)
{
    if (m_encoding->name() != "UTF-8") {
        if (!is_special(*m_url) ||
            (m_url->scheme() == "ws" || m_url->scheme() == "wss") )
        {
            m_encoding = encoding::get_an_encoding("UTF-8");
            assert(m_encoding);
        }
    }

    if (!m_state_override && c == '#') {
        m_url->fragment() = "";
        m_state = state::fragment;
    }
    else if (c != eof) {
        if (!is_url_code_point(c) && c != '%') {
            validation_error(errc::illegal_character);
        }

        if (c == '%' && !starts_with_two_ascii_hex_digits(remaining())) {
            validation_error(errc::invalid_percent_encoded_char);
        }

        auto bytes = encode(c, *m_encoding);

        if (starts_with(bytes, "&#") && ends_with(bytes, ";")) {
            bytes.replace(0, 2, "%26%23"); // "&#"

            auto const last = bytes.size() - 1;
            bytes.replace(last, 1, "%3B"); // ";"

            assert(m_url->query());
            append(*m_url->query(), isomorphic_decode(bytes));
        }
        else {
            for (auto const b: bytes) {
                if (b < '!' || b > '~' || b == '"' || b == '#' ||
                    b == '<' || b == '>' ||
                    (b == '\'' && is_special(*m_url)) )
                {
                    assert(m_url->query());
                    append(*m_url->query(), percent_encode(b));
                }
                else {
                    append(*m_url->query(), b);
                }
            }
        }
    }

    return go_on;
}

result_t basic_url_parser::
fragment(code_point_t const c)
{
    switch (c) {
        case eof:
            break;
        case 0x00:
            validation_error(errc::null_in_fragment);
            break;
        default:
            if (!is_url_code_point(c) && c != '%') {
                validation_error(errc::illegal_character);
            }
            if (c == '%' && !starts_with_two_ascii_hex_digits(remaining())) {
                validation_error(errc::invalid_percent_encoded_char);
            }

            assert(m_url->fragment());
            append(*m_url->fragment(),
                   utf8_percent_encode(c, is_fragment_percent_encode_char));
            break;
    }

    return go_on;
}

string_view_t basic_url_parser::
remaining() const
{
    return {
        static_cast<string_t::const_iterator>(m_pointer) + 1, m_input.end()
    };
}

void basic_url_parser::
validation_error(errc const e) const
{
    if (m_error_handler) {
        m_error_handler(
            make_error_code(e), m_input,
            to_size(std::max(0L, m_pointer - m_input.begin()))
        );
    }
}

basic_url_parser::state_handler_t basic_url_parser::
lookup_state_handler(state const s)
{
    static state_handler_t table[] {
        &basic_url_parser::scheme_start,
        &basic_url_parser::scheme,
        &basic_url_parser::no_scheme,
        &basic_url_parser::special_relative_or_authority,
        &basic_url_parser::path_or_authority,
        &basic_url_parser::relative,
        &basic_url_parser::relative_slash,
        &basic_url_parser::special_authority_slashes,
        &basic_url_parser::special_authority_ignore_slashes,
        &basic_url_parser::authority,
        &basic_url_parser::hostname, // state::host
        &basic_url_parser::hostname,
        &basic_url_parser::port,
        &basic_url_parser::file,
        &basic_url_parser::file_slash,
        &basic_url_parser::file_host,
        &basic_url_parser::path_start,
        &basic_url_parser::path,
        &basic_url_parser::cannot_be_a_base_url_path,
        &basic_url_parser::query,
        &basic_url_parser::fragment,
    };

    return table[static_cast<int>(s)];
}

void
set_username(url_record& url, byte_sequence_view_t const username)
{
    url.username().clear();

    auto const username_ = decode_utf8_without_bom(username);
    for (auto const c: username_) {
        append(url.username(),
            utf8_percent_encode(c, is_userinfo_percent_encode_char));
    }
}

void
set_password(url_record& url, byte_sequence_view_t const password)
{
    url.password().clear();

    auto const password_ = decode_utf8_without_bom(password);
    for (auto const c: password_) {
        append(url.password(),
            utf8_percent_encode(c, is_userinfo_percent_encode_char));
    }
}

std::optional<url_record>
basic_url_parse(byte_sequence_view_t input,
                url_record const* base/*= nullptr*/,
                std::optional<byte_sequence_view_t> encoding_override/*= {}*/)
{
    basic_url_parser p;

    return p(input, base, encoding_override);
}

void
basic_url_parse(byte_sequence_view_t input,
                url_record& url,
                basic_url_parser::state state_override,
                url_record const* base/*= nullptr*/,
                std::optional<byte_sequence_view_t> encoding_override/*= {}*/)
{
    basic_url_parser p;

    p(input, url, state_override, base, encoding_override);
}

// LCOV_EXCL_START
std::ostream&
operator<<(std::ostream& os, basic_url_parser::state const s)
{
    switch (s) {
        case state_t::scheme_start:
            os << "scheme_start";
            break;
        case state_t::scheme:
            os << "scheme";
            break;
        case state_t::no_scheme:
            os << "no_scheme";
            break;
        case state_t::special_relative_or_authority:
            os << "special_relative_or_authority";
            break;
        case state_t::path_or_authority:
            os << "path_or_authority";
            break;
        case state_t::relative:
            os << "relative";
            break;
        case state_t::relative_slash:
            os << "relative_slash";
            break;
        case state_t::special_authority_slashes:
            os << "special_authority_slashes";
            break;
        case state_t::special_authority_ignore_slashes:
            os << "special_authority_ignore_slashes";
            break;
        case state_t::authority:
            os << "authority";
            break;
        case state_t::host:
            os << "host";
            break;
        case state_t::hostname:
            os << "hostname";
            break;
        case state_t::port:
            os << "port";
            break;
        case state_t::file:
            os << "file";
            break;
        case state_t::file_slash:
            os << "file_slash";
            break;
        case state_t::file_host:
            os << "file_host";
            break;
        case state_t::path_start:
            os << "path_start";
            break;
        case state_t::path:
            os << "path";
            break;
        case state_t::cannot_be_a_base_url_path:
            os << "cannot_be_a_base_url_path";
            break;
        case state_t::query:
            os << "query";
            break;
        case state_t::fragment:
            os << "fragment";
            break;
        default:
            os << "unknown state";
            break;
    }

    return os;
}
// LCOV_EXCL_STOP

} // namespace whatwg::url
