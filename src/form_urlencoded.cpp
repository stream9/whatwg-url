#include <whatwg/url/form_urlencoded.hpp>

#include <whatwg/url/percent_encoding.hpp>

#include <whatwg/encoding/encoding.hpp>

namespace whatwg::url {

template<typename StringT>
static void
append(form_urlencoded_t& pairs, StringT&& name, StringT&& value)
{
    pairs.emplace_back(std::move(name), std::move(value));
}

static byte_sequence_t
encode_from_utf8(byte_sequence_view_t const s,
                 encoding::encoding const& encoding)
{
    if (encoding.name() == "UTF-8") {
        return byte_sequence_t { s };
    }
    else {
        return encode(decode_utf8_without_bom(s), encoding);
    }
}

form_urlencoded_t
parse_form_urlencoded(byte_sequence_view_t const input)
{
    auto const& sequences = split(input, '&');

    form_urlencoded_t output;

    for (auto const& bytes: sequences) {
        byte_sequence_t name;
        byte_sequence_t value;

        auto const idx = bytes.find('=');
        if (idx != bytes.npos) {
            name = bytes.substr(0, idx);
            value = bytes.substr(idx + 1);
        }
        else {
            name = bytes;
            value = "";
        }

        replace_all(name, '+', ' ');
        replace_all(value, '+', ' ');

        auto const name_string = percent_decode(name);
        auto const value_string = percent_decode(value);

        append(output, name_string, value_string);
    }

    return output;
}

byte_sequence_t
byte_serialize_form_urlencoded(byte_sequence_view_t const input)
{
    byte_sequence_t output;

    for (auto const byte: input) {
        if (byte == ' ') {
            append(output, '+');
        }
        else if (byte == '*' || byte == '-' || byte == '.' ||
                 ('0' <= byte && byte <= '9') ||
                 ('A' <= byte && byte <= 'Z') ||
                 byte == '_' ||
                 ('a' <= byte && byte <= 'z'))
        {
            append(output, byte);
        }
        else {
            append(output, percent_encode(byte));
        }
    }

    return output;
}

byte_sequence_t
serialize_form_urlencoded(form_urlencoded_t const& tuples,
                          encoding::encoding const* encoding_override)
{
    auto* encoding = encoding::get_an_encoding("UTF-8");
    if (encoding_override) {
        encoding = &get_an_output_encoding(*encoding);
    }
    assert(encoding);

    byte_sequence_t output;
    bool first = true;

    for (auto const& tuple: tuples) {
        auto const& name =
            byte_serialize_form_urlencoded(encode_from_utf8(tuple.first, *encoding));
        auto const& value =
            byte_serialize_form_urlencoded(encode_from_utf8(tuple.second, *encoding));

        if (first) {
            first = false;
        }
        else {
            append(output, '&');
        }

        append(output, name);
        append(output, '=');
        append(output, value);
    }

    return output;
}

} // namespace whatwg::url
