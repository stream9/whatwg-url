#include <whatwg/url/idna.hpp>

#include <climits>
#include <cassert>
#include <memory>

#include <unicode/idna.h>

namespace whatwg::url {

static icu::IDNA*
make_idna(bool const strict = false)
{
    UErrorCode error_code = U_ZERO_ERROR;

    // ICU doesn't seem to have CheckHyphens and VerifyDnsLength option
    auto options = UIDNA_CHECK_BIDI | UIDNA_CHECK_CONTEXTJ |
                       UIDNA_NONTRANSITIONAL_TO_ASCII |
                       UIDNA_NONTRANSITIONAL_TO_UNICODE;
    if (strict) {
        options |= UIDNA_USE_STD3_RULES;
    }

    auto* const idna = icu::IDNA::createUTS46Instance(
        static_cast<uint32_t>(options),
        error_code
    );
    assert(U_SUCCESS(error_code));

    return idna;
}

static icu::IDNA&
idna()
{
    static std::unique_ptr<icu::IDNA> instance { make_idna() };
    return *instance;
}

static icu::IDNA&
strict_idna()
{
    static std::unique_ptr<icu::IDNA> instance { make_idna(true) };
    return *instance;
}

static bool
is_error(icu::IDNAInfo const& info)
{
    if (!info.hasErrors()) return false;

    // ignore VerifyDNSLength related error
    auto err = info.getErrors();
    err &= ~static_cast<uint32_t>(UIDNA_ERROR_EMPTY_LABEL);
    err &= ~static_cast<uint32_t>(UIDNA_ERROR_LABEL_TOO_LONG);
    err &= ~static_cast<uint32_t>(UIDNA_ERROR_DOMAIN_NAME_TOO_LONG);

    // ignore hyphen related error
    err &= ~static_cast<uint32_t>(UIDNA_ERROR_LEADING_HYPHEN);
    err &= ~static_cast<uint32_t>(UIDNA_ERROR_TRAILING_HYPHEN);
    err &= ~static_cast<uint32_t>(UIDNA_ERROR_HYPHEN_3_4);

    return err;
}

result<byte_sequence_t>
domain_to_ascii(string_view_t const input, bool const be_strict/*= false*/)
{
    assert(input.size() < INT_MAX);

    auto const u16input = icu::UnicodeString::fromUTF32(
        reinterpret_cast<UChar32 const*>(input.data()),
        static_cast<int32_t>(input.size())
    );

    icu::UnicodeString u16output;
    icu::IDNAInfo info;
    UErrorCode error_code = U_ZERO_ERROR;

    auto& idna_ = be_strict ? strict_idna() : idna();

    idna_.nameToASCII(u16input, u16output, info, error_code);

    if (U_SUCCESS(error_code) && !is_error(info)) {
        byte_sequence_t output;
        u16output.toUTF8String(output);

        return output;
    }
    else {
        // caller has to handle validation error
        if (U_FAILURE(error_code)) {
            return std::error_code { error_code, icu_category() };
        }
        else {
            return std::error_code {
                static_cast<int>(info.getErrors()), idna_category() };
        }
    }
}

result<byte_sequence_t>
domain_to_unicode(byte_sequence_view_t const domain)
{
    assert(domain.size() < INT_MAX);

    icu::StringPiece const name {
        domain.data(), static_cast<int>(domain.size()) };

    byte_sequence_t result;
    icu::StringByteSink<byte_sequence_t> dest { &result };

    icu::IDNAInfo info;
    UErrorCode error_code = U_ZERO_ERROR;

    idna().nameToUnicodeUTF8(name, dest, info, error_code);

    if (U_SUCCESS(error_code) && !is_error(info)) {
        return result;
    }
    else {
        // caller has to handle validation error
        if (U_FAILURE(error_code)) {
            return std::error_code { error_code, icu_category() };
        }
        else {
            return std::error_code {
                static_cast<int>(info.getErrors()), idna_category() };
        }
    }
}

class icu_category_impl : public std::error_category
{
public:
    char const* name() const noexcept override
    {
        return "ICU";
    }

    std::string message(int const code) const override
    {
        return u_errorName(static_cast<UErrorCode>(code));
    }
};

std::error_category const&
icu_category()
{
    static icu_category_impl const impl;

    return impl;
}

class idna_category_impl : public std::error_category
{
public:
    char const* name() const noexcept override
    {
        return "IDNA";
    }

    std::string message(int const code) const override
    {
        std::string msg;

        switch (static_cast<uint32_t>(code)) {
            case UIDNA_ERROR_EMPTY_LABEL:
                msg += "UIDNA_ERROR_EMPTY_LABEL, ";
                break;
            case UIDNA_ERROR_LABEL_TOO_LONG:
                msg += "UIDNA_ERROR_LABEL_TOO_LONG, ";
                break;
            case UIDNA_ERROR_DOMAIN_NAME_TOO_LONG:
                msg += "UIDNA_ERROR_DOMAIN_NAME_TOO_LONG, ";
                break;
            case UIDNA_ERROR_LEADING_HYPHEN:
                msg += "UIDNA_ERROR_LEADING_HYPHEN, ";
                break;
            case UIDNA_ERROR_TRAILING_HYPHEN:
                msg += "UIDNA_ERROR_TRAILING_HYPHEN, ";
                break;
            case UIDNA_ERROR_HYPHEN_3_4:
                msg += "UIDNA_ERROR_HYPHEN_3_4, ";
                break;
            case UIDNA_ERROR_LEADING_COMBINING_MARK:
                msg += "UIDNA_ERROR_LEADING_COMBINING_MARK, ";
                break;
            case UIDNA_ERROR_DISALLOWED:
                msg += "UIDNA_ERROR_DISALLOWED, ";
                break;
            case UIDNA_ERROR_PUNYCODE:
                msg += "UIDNA_ERROR_PUNYCODE, ";
                break;
            case UIDNA_ERROR_LABEL_HAS_DOT:
                msg += "UIDNA_ERROR_LABEL_HAS_DOT, ";
                break;
            case UIDNA_ERROR_INVALID_ACE_LABEL:
                msg += "UIDNA_ERROR_INVALID_ACE_LABEL, ";
                break;
            case UIDNA_ERROR_BIDI:
                msg += "UIDNA_ERROR_BIDI, ";
                break;
            case UIDNA_ERROR_CONTEXTJ:
                msg += "UIDNA_ERROR_CONTEXTJ, ";
                break;
            case UIDNA_ERROR_CONTEXTO_PUNCTUATION:
                msg += "UIDNA_ERROR_CONTEXTO_PUNCTUATION, ";
                break;
            case UIDNA_ERROR_CONTEXTO_DIGITS:
                msg += "UIDNA_ERROR_CONTEXTO_DIGITS, ";
                break;
            default:
                msg += std::to_string(static_cast<uint32_t>(code));
                break;
        }

        return msg;
    }
};

std::error_category const&
idna_category()
{
    static idna_category_impl const impl;

    return impl;
}

} // namespace whatwg::url
