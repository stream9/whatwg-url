#include <whatwg/url/url_rendering.hpp>

#include <whatwg/url/idna.hpp>
#include <whatwg/url/misc.hpp>
#include <whatwg/url/percent_encoding.hpp>

namespace whatwg::url {

byte_sequence_t
render_url(url_record const& url, bool const exclude_fragment/*= false*/)
{
    byte_sequence_t output { url.scheme() };

    append(output, ':');

    if (!url.host().is_null()) {
        append(output, "//");

        // skip username / password

        auto const& unicode_host =
                        domain_to_unicode(serialize_host(url.host()));
        assert(unicode_host);
        append(output, *unicode_host);

        if (url.port()) {
            append(output, ':');
            append(output, serialize_integer(*url.port()));
        }
    }
    else if (url.scheme() == "file") {
        append(output, "//");
    }

    if (url.cannot_be_a_base_url()) {
        append(output, percent_decode(url.path()[0]));
    }
    else {
        append(output, percent_decode(join_path(url.path(), '/')));
    }

    if (url.query()) {
        append(output, '?');
        append(output, percent_decode(*url.query()));
    }

    if (!exclude_fragment && url.fragment()) {
        append(output, '#');
        append(output, percent_decode(*url.fragment()));
    }

    return output;
}

} // namespace whatwg::url
