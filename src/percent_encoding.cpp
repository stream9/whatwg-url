#include <whatwg/url/percent_encoding.hpp>

namespace whatwg::url {

byte_sequence_t
percent_decode(byte_sequence_view_t const bytes)
{
    byte_sequence_t output;

    auto is_not_ascii_hex_digit =
        [](auto const c) {
            return !(('0' <= c && c <= '9') ||
                     ('A' <= c && c <= 'F') ||
                     ('a' <= c && c <= 'f') );
        };

    for (size_t i = 0; i < bytes.size(); ++i) {
        auto const byte = bytes[i];

        if (byte != '%') {
            append(output, byte);
        }
        else {
            auto const next_two_bytes = bytes.substr(i + 1, 2);

            // spec doesn't have this but I think it is necessary
            if (next_two_bytes.size() != 2) {
                append(output, byte);
            }
            else if (infra::contains(next_two_bytes, is_not_ascii_hex_digit)) {
                append(output, byte);
            }
            else {
                // spec require to decode UTF-8 input but logic guarantee
                // next two bytes are ASCII hex digits, so we don't decode.
                auto const byte_point = static_cast<byte_t>(
                      hex_digit_to_number(to_code_point(next_two_bytes[0])) * 16
                    + hex_digit_to_number(to_code_point(next_two_bytes[1]))
                );

                append(output, byte_point);
                i += 2;
            }
        }
    }

    return output;
}

byte_sequence_t
string_percent_decode(string_view_t const input)
{
    auto const& bytes = encode_utf8(input);

    return percent_decode(bytes);
}

} // namespace whatwg::url
