#include <whatwg/url/character_encoding.hpp>

#include <whatwg/url/utility.hpp>

#include <algorithm>
#include <iterator>
#include <cassert>
#include <climits>

#include <whatwg/encoding/encoding.hpp>
#include <whatwg/encoding/stream.hpp>
#include <whatwg/infra/string.hpp>

namespace whatwg::url {

byte_sequence_t
encode(code_point_t const c, encoding::encoding const& encoding)
{
    encoding::stream_t st;
    push(st, c);

    return encoding::encode(st, encoding).to_string();
}

byte_sequence_t
encode(string_t const& s, encoding::encoding const& encoding)
{
    encoding::stream_t st { s };

    return encoding::encode(st, encoding).to_string();
}

byte_sequence_t
encode_utf8(code_point_t const c)
{
    encoding::stream_t s;
    push(s, c);

    return encoding::utf8_encode(s).to_string();
}

byte_sequence_t
encode_utf8(string_view_t const input)
{
    encoding::stream_t s { input };

    return encoding::utf8_encode(s).to_string();
}

string_t
decode_utf8_without_bom(byte_sequence_view_t const input)
{
    encoding::byte_stream_t s { input };

    return encoding::utf8_decode_without_bom(s).to_string();
}

} // namespace whatwg::url

