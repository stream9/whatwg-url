#include <whatwg/url/ctype.hpp>

namespace std {

std::locale::id ctype<char32_t>::id;

} // namespace std
