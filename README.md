# URL
C++ implementation of [URL Standard](https://url.spec.whatwg.org/).  
compliant with specification last updated on [9 August 2018](https://url.spec.whatwg.org/commit-snapshots/23af4fb31f3d946ed85d1eb9db6329578c29511c/)

## Requirement
- C++17 compliant compiler
- cmake
- boost
- ICU
- [JSON library](https://gitlab.com/stream9/json) (only for testing)
 
## Getting started
```shell
$ git clone https://gitlab.com/stream9/url
$ cd url
$ mkdir build
$ cd build
$ cmake ..
$ make
```
## Plan
1. [x] Implement all features specification define
    1. [x] URL representation
    2. [x] URL parsing
    3. [x] URL serializing
    4. [x] URL equivalence
    5. [x] URL origin
    6. [x] URL rendering
    7. [x] application/x-form-urlencoded parsing
    8. [x] application/x-form-urlencoded serializing
    9. [x] URL class
    10. [x] URLSearchParams class
    11. [x] legacy encoding
2. [ ] Thorough testing
    1. [x] test cases from [web-platform-tests](https://github.com/web-platform-tests/wpt/tree/master/url)
    2. [ ] 100% test coverage
        1. [x] statement coverage (almost 100%)
        2. [ ] branch coverage
3. [ ] Optimization
    1. [x] space - naive representation cost almost 300 bytes a pop
    2. [ ] time - don't be too ambitious, measure first
4. [ ] Documentation
