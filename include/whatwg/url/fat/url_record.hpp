#ifndef URL_FAT_URL_RECORD_HPP
#define URL_FAT_URL_RECORD_HPP

#include <whatwg/url/string.hpp>
#include <whatwg/url/host.hpp>

#include <vector>

namespace whatwg::url::fat {

using path_t = std::vector<byte_sequence_t>;

class url_record
{
public:
    auto& scheme() { return m_scheme; }
    auto& scheme() const { return m_scheme; }

    auto& username() { return m_username; }
    auto& username() const { return m_username; }

    auto& password() { return m_password; }
    auto& password() const { return m_password; }

    auto& host() { return m_host; }
    auto& host() const { return m_host; }

    auto& port() { return m_port; }
    auto& port() const { return m_port; }

    auto& path() { return m_path; }
    auto& path() const { return m_path; }

    auto& query() { return m_query; }
    auto& query() const { return m_query; }

    auto& fragment() { return m_fragment; }
    auto& fragment() const { return m_fragment; }

    auto& cannot_be_a_base_url() { return m_cannot_be_a_base_url; }
    auto& cannot_be_a_base_url() const { return m_cannot_be_a_base_url; }

    auto& object() { return m_object; }
    auto& object() const { return m_object; }

private:
    byte_sequence_t                m_scheme;
    byte_sequence_t                m_username;
    byte_sequence_t                m_password;
    host_t                        m_host;
    std::optional<uint16_t>       m_port;
    path_t                        m_path;
    std::optional<byte_sequence_t> m_query;
    std::optional<byte_sequence_t> m_fragment;

    bool m_cannot_be_a_base_url = false;
    void* m_object = nullptr;
};

// utilities
void append(path_t&, byte_sequence_view_t);
void append(path_t&, string_view_t);
void append(path_t&, size_t, string_view_t);

void pop_front(path_t&);

bool is_null(std::optional<byte_sequence_t> const&);
bool is_empty(std::optional<byte_sequence_t> const&);

byte_sequence_view_t
    value_or(std::optional<byte_sequence_t> const&, byte_sequence_view_t);

byte_sequence_t join_path(path_t const&, byte_t separator);

} // namespace whatwg::url::fat

namespace whatwg::url {

using namespace fat;

} // namespace whatwg::url

#endif // URL_FAT_URL_RECORD_HPP
