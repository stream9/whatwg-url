#ifndef URL_IDNA_HPP
#define URL_IDNA_HPP

#include <whatwg/url/string.hpp>
#include <whatwg/url/result.hpp>

#include <optional>
#include <system_error>

namespace whatwg::url {

result<byte_sequence_t>
    domain_to_ascii(string_view_t input, bool be_strict = false);

result<byte_sequence_t>
    domain_to_unicode(byte_sequence_view_t domain);

std::error_category const& icu_category();
std::error_category const& idna_category();

} // namespace whatwg::url

#endif // URL_IDNA_HPP
