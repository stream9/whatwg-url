#ifndef URL_CONVERSION_HPP
#define URL_CONVERSION_HPP

#include <whatwg/url/string.hpp>

#include <cstdint>
#include <optional>

namespace whatwg::url {

char number_to_upper_hex_digit(int);

int decimal_digit_to_number(code_point_t);
int hex_digit_to_number(code_point_t);

std::optional<int64_t> digits_to_number(byte_sequence_view_t, int radix);
std::optional<int64_t> decimal_digits_to_number(string_view_t);

} // namespace whatwg::url

#endif // URL_CONVERSION_HPP
