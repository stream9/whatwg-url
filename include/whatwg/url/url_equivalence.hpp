#ifndef URL_URL_EQUIVALENCE_HPP
#define URL_URL_EQUIVALENCE_HPP

#include <whatwg/url/url_record.hpp>

namespace whatwg::url {

// 4.6 URL equivalence
bool equals(url_record const& lhs,
            url_record const& rhs, bool exclude_fragment = false);
inline bool operator==(url_record const& lhs, url_record const& rhs)
{
    return equals(lhs, rhs, false);
}


} // namespace whatwg::url

#endif // URL_URL_EQUIVALENCE_HPP
