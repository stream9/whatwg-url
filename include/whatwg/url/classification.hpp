#ifndef URL_CLASSIFICATION_HPP
#define URL_CLASSIFICATION_HPP

#include <whatwg/url/char.hpp>

#include <whatwg/infra/code_point.hpp>

namespace whatwg::url {

template<typename CharT>
bool
is_forbidden_host_code_point(CharT const c)
{
    return c == 0x00 || c == '\t' || c == '\r' || c == '\n' || c == ' ' ||
           c == '#' || c == '%' || c == '/' || c == ':' || c == '?' ||
           c == '?' || c == '@' || c == '[' || c == '\\' || c == ']';
}

inline bool
is_url_code_point(code_point_t const c)
{
    return infra::is_ascii_alphanumeric(c) ||
           c == '!' || c == '$' || c == '&' ||
           c == '\'' || c == '(' || c == ')' || c == '*' ||
           c == '+' || c == ',' || c == '-' || c == '.' ||
           c == '/' || c == ':' || c == ';' || c == '=' ||
           c == '?' || c == '@' || c == '_' || c == '~' ||
           (0xa0 <= c && c <= 0xff);
}

} // namespace whatwg::url

#endif // URL_CLASSIFICATION_HPP
