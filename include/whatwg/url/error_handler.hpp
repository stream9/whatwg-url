#ifndef URL_ERROR_HANDLER_HPP
#define URL_ERROR_HANDLER_HPP

#include <whatwg/url/string.hpp>

#include <system_error>
#include <functional>

namespace whatwg::url {

enum class errc;

using error_handler_t =
    std::function<void(
        std::error_code const&,
        string_view_t input,
        size_t index
    )>;

} // namespace whatwg::url

#endif // URL_ERROR_HANDLER_HPP
