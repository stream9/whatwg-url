#ifndef WHATWG_URL_UTILITY_HPP
#define WHATWG_URL_UTILITY_HPP

#include <limits>
#include <type_traits>

namespace whatwg::url {

template<typename T>
size_t
to_size(T const v)
{
    if constexpr (std::is_signed_v<T>) {
        assert(v >= 0);
    }
    return static_cast<size_t>(v);
}

template<typename T>
ptrdiff_t
to_diff(T const v)
{
    if (std::is_same_v<T, size_t>) {
        assert(v <= std::numeric_limits<ptrdiff_t>::max());
    }
    return static_cast<ptrdiff_t>(v);
}

} // namespace whatwg::url

#endif // WHATWG_URL_UTILITY_HPP
