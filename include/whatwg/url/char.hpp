#ifndef URL_CHAR_HPP
#define URL_CHAR_HPP

#include <cassert>

#include <whatwg/infra/byte.hpp>
#include <whatwg/infra/code_point.hpp>

namespace whatwg::url {

/*
 * Character
 */
using byte_t = infra::byte_t;
using infra::code_point_t;

static code_point_t constexpr eof = 0xFFFFFFFF;

inline byte_t
to_byte(code_point_t const c)
{
    assert(c <= 0xFF);
    return static_cast<byte_t>(c);
}

inline byte_t
to_ascii_no_check(code_point_t const c) //TODO remove
{
    return static_cast<byte_t>(c);
}

inline code_point_t
to_code_point(byte_t const c)
{
    return static_cast<code_point_t>(c);
}

} // namespace whatwg::url

#endif // URL_CHAR_HPP
