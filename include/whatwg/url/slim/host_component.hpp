#ifndef URL_SLIM_HOST_COMPONENT_HPP
#define URL_SLIM_HOST_COMPONENT_HPP

#include <whatwg/url/slim/string_component.hpp>

namespace whatwg::url {

class host_t;

} // namespace whatwg::url

namespace whatwg::url::slim {

/* act as host_t */

class url_record;
class host_component;

class const_host_component
{
public:
    const_host_component(url_record const&);

    const_host_component(const_host_component const&) = default;
    const_host_component(host_component const&);

    const_host_component& operator=(const_host_component const&) = delete;

    bool is_empty() const;
    bool is_null() const;

    explicit operator bool() const;

    operator byte_sequence_view_t() const;

private:
    const_string_component m_comp;

    friend class host_component;
};

class host_component
{
public:
    host_component(url_record&);

    host_component(host_component const&) = default;

    host_component& operator=(host_component const&) = default;
    host_component& operator=(const_host_component const&);
    host_component& operator=(host_t const&);

    bool is_empty() const;
    bool is_null() const;

    void set_empty_string();

    explicit operator bool() const;

    operator byte_sequence_view_t() const;

private:
    string_component m_comp;

    friend class const_host_component;
};

void assign(host_component, host_t const&);

byte_sequence_t serialize_host(const_host_component);

std::ostream& operator<<(std::ostream&, const_host_component const&);

bool operator==(const_host_component const&, byte_sequence_view_t);

} // namespace whatwg::url::slim

#endif // URL_SLIM_HOST_COMPONENT_HPP
