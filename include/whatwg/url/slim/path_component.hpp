#ifndef URL_PATH_COMPONENT_HPP
#define URL_PATH_COMPONENT_HPP

#include <whatwg/url/slim/string_component.hpp>

#include <whatwg/url/string.hpp>

#include <cstdint>

namespace whatwg::url::slim {

/* act as std::vector<byte_sequence_t> */

class url_record;
class path_component;

class const_path_component
{
public:
    using size_type = int32_t;

public:
    const_path_component(url_record const&);

    const_path_component(const_path_component const&) = default;
    const_path_component(path_component const&);

    const_path_component& operator=(const_path_component const&) = delete;

    size_type size() const;
    bool empty() const;
    bool is_hierarchical() const;

    byte_sequence_view_t operator[](size_type) const;

    operator byte_sequence_view_t() const;

private:
    const_string_component m_comp;
};

class path_component
{
public:
    using size_type = int32_t;

public:
    path_component(url_record&);

    path_component(path_component const&) = default;

    path_component& operator=(path_component const&);
    path_component& operator=(const_path_component const&);
    path_component& operator=(byte_sequence_view_t);

    size_type size() const;
    bool empty() const;
    bool is_hierarchical() const;

    void assign(byte_sequence_view_t s);

    void pop_front();
    void pop_back();

    void append(byte_sequence_view_t segment);
    void append(string_view_t segment);

    void append(size_type index, string_view_t s);

    void clear();

    byte_sequence_view_t operator[](size_type) const;

    operator byte_sequence_view_t() const;

private:
    string_component m_comp;

    friend class const_path_component;
};

void assign(path_component, byte_sequence_view_t);

void append(path_component, byte_sequence_view_t);
void append(path_component, string_view_t);

void append(path_component, path_component::size_type, string_view_t);

void pop_front(path_component);

byte_sequence_view_t join_path(const_path_component, byte_t separator);

std::ostream& operator<<(std::ostream&, const_path_component const&);

bool operator==(const_path_component const&, byte_sequence_view_t);

} // namespace whatwg::url::slim

#endif // URL_PATH_COMPONENT_HPP
