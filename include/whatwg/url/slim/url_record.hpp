#ifndef URL_SLIM_URL_RECORD_HPP
#define URL_SLIM_URL_RECORD_HPP

#include <whatwg/url/slim/flag_proxy.hpp>
#include <whatwg/url/slim/host_component.hpp>
#include <whatwg/url/slim/path_component.hpp>
#include <whatwg/url/slim/port_component.hpp>
#include <whatwg/url/slim/segment.hpp>
#include <whatwg/url/slim/string_component.hpp>

#include <whatwg/url/string.hpp>
#include <whatwg/url/host.hpp>

namespace whatwg::url::slim {

class const_string_component;
class const_host_component;
class const_port_component;
class const_path_component;
class string_component;
class host_component;
class port_component;
class path_component;

template<typename T> class flag_proxy;

class url_record
{
public:
    using size_type = int32_t;
    using difference_type = int32_t;
public:
    url_record();

    string_component scheme();
    const_string_component scheme() const;

    string_component username();
    const_string_component username() const;

    string_component password();
    const_string_component password() const;

    host_component host();
    const_host_component host() const;

    port_component port();
    const_port_component port() const;

    path_component path();
    const_path_component path() const;

    string_component query();
    const_string_component query() const;

    string_component fragment();
    const_string_component fragment() const;

    flag_proxy<uint32_t> cannot_be_a_base_url();
    flag_proxy<uint32_t const> cannot_be_a_base_url() const;

    void* object() const;

    operator byte_sequence_view_t() const;

private:
    enum class flag : uint32_t;

    segment_t& segment(segment_id);
    bool has_authority() const;

    void assign(segment_id, byte_sequence_view_t);
    void assign(segment_id, string_view_t);

    void append(segment_id, byte_t);
    void append(segment_id, byte_sequence_view_t);
    void append(segment_id, code_point_t);
    void append(segment_id, string_view_t);

    void prepend(segment_id, byte_t);
    void prepend(segment_id, byte_sequence_view_t);
    void prepend(segment_id, code_point_t);
    void prepend(segment_id, string_view_t);

    void insert(segment_id, size_type index, byte_sequence_view_t);

    void erase(segment_id, size_type index, size_type length);
    void clear(segment_id);

    void reset(segment_id);

    void adjust_index(segment_id, size_type delta);

    void adjust_delimiters();
    void add_suffix_delimiter(segment_id, byte_sequence_view_t delim);
    void remove_suffix_delimiter(segment_id);
    void adjust_suffix_delimiter(segment_id, byte_sequence_view_t delim);
    void adjust_prefix_delimiter(segment_id, byte_sequence_view_t delim);
    void adjust_authority_delimiter();

    int suffix_delimiter_length(segment_id) const;
    int prefix_delimiter_length(segment_id) const;

    void set_flag(flag);
    void reset_flag(flag);
    bool has_flag(flag) const;

    bool is_null(segment_id) const;
    bool is_empty(segment_id) const;
    byte_sequence_view_t to_string_view(segment_id) const;

private:
    enum class flag : uint32_t {
        cannot_be_a_base_url = 1 << 0,
        dirty                = 1 << 1,
    };

    byte_sequence_t m_spec;
    std::array<segment_t, 8> m_segments;
    uint32_t m_flags = 0;

    friend class const_string_component;
    friend class string_component;
    friend class test_harness;
};

// utility functions
byte_sequence_view_t serialize(url_record const&, bool exclude_fragment = false);

std::ostream& operator<<(std::ostream&, url_record const&);

bool operator==(url_record const&, byte_sequence_view_t);

} // namespace whatwg::url::slim

namespace whatwg::url {

using namespace slim;

} // namespace whatwg::url

#endif // URL_SLIM_URL_RECORD_HPP
