#ifndef URL_SLIM_FLAG_PROXY_HPP
#define URL_SLIM_FLAG_PROXY_HPP

namespace whatwg::url::slim {

template<typename T>
class flag_proxy
{
public:
    flag_proxy(T& flags, T const field)
        : m_flags { &flags }
        , m_field { field }
    {}

    flag_proxy(flag_proxy const&) = default;
    flag_proxy& operator=(flag_proxy const&) = default;

    flag_proxy& operator=(bool const set)
    {
        if (set) {
            *m_flags |= m_field;
        }
        else {
            *m_flags &= ~m_field;
        }

        return *this;
    }

    operator bool() const
    {
        return *m_flags & m_field;
    }

private:
    T* const m_flags; // non-null
    T const m_field;
};

} // namespace whatwg::url::slim

#endif // URL_SLIM_FLAG_PROXY_HPP
