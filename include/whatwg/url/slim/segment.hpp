#ifndef URL_SLIM_SEGMENT_HPP
#define URL_SLIM_SEGMENT_HPP

#include <cstdint>
#include <iosfwd>

namespace whatwg::url::slim {

class segment_id
{
public:
    enum {
        scheme = 0, username = 1, password = 2, host = 3, port = 4,
        path = 5, query = 6, fragment = 7,
    };
public:

    segment_id(int const id) : m_id { id } {}

    operator int() const { return m_id; }

    bool is_valid() const { return 0 <= m_id && m_id <= 7; }

private:
    int m_id;
};

struct segment_t {
    using size_type = int32_t;

    size_type index = 0;
    size_type length = -1;

    operator bool() const { return !is_null(); }
    bool is_null() const { return length == -1; }
};

std::ostream& operator<<(std::ostream&, segment_t const&);

} // namespace whatwg::url::slim

#endif // URL_SLIM_SEGMENT_HPP
