#ifndef URL_SLIM_STRING_COMPONENT_HPP
#define URL_SLIM_STRING_COMPONENT_HPP

#include <whatwg/url/char.hpp>
#include <whatwg/url/string.hpp>
#include <whatwg/url/slim/segment.hpp>

namespace whatwg::url::slim {

/* act as std::optional<byte_sequence_t> */

class url_record;
class string_component;

class const_string_component
{
public:
    const_string_component(url_record const&, segment_id);

    const_string_component(const_string_component const&) = default;
    const_string_component(string_component const&);

    const_string_component& operator=(const_string_component const&) = delete;

    url_record const& url() const { return m_url; }

    const_string_component const& operator*() const { return *this; }
    explicit operator bool() const;
    bool empty() const;

    operator byte_sequence_view_t() const;

private:
    url_record const& m_url;
    segment_id const m_segment;

    friend class string_component;
};

class string_component
{
public:
    using size_type = int32_t;

public:
    string_component(url_record&, segment_id);

    string_component(string_component const&) = default;

    string_component& operator=(string_component const&);
    string_component& operator=(const_string_component const&);
    string_component& operator=(byte_sequence_view_t);
    string_component& operator=(string_view_t);

    url_record const& url() const { return m_url; }

    void append(byte_t);
    void append(byte_sequence_view_t);
    void append(code_point_t);
    void append(string_view_t);

    void prepend(byte_t);
    void prepend(byte_sequence_view_t);
    void prepend(code_point_t);
    void prepend(string_view_t);

    void insert(size_type index, byte_sequence_view_t s);
    void erase(size_type index, size_type length);

    void clear();

    // mimic std::optional
    string_component const& operator*() const { return *this; }
    explicit operator bool() const;
    bool empty() const;
    void reset();

    operator byte_sequence_view_t() const;

private:
    url_record& m_url;
    segment_id const m_segment;

    friend class const_string_component;
};

void assign(string_component comp, byte_sequence_view_t);
void assign(string_component comp, string_view_t);

void append(string_component comp, byte_t);
void append(string_component comp, byte_sequence_view_t);
void append(string_component comp, code_point_t);
void append(string_component comp, string_view_t);

void prepend(string_component comp, byte_t);
void prepend(string_component comp, byte_sequence_view_t);
void prepend(string_component comp, code_point_t);
void prepend(string_component comp, string_view_t);

bool is_null(const_string_component);
bool is_empty(const_string_component);

byte_sequence_view_t value_or(const_string_component, byte_sequence_view_t);

std::ostream& operator<<(std::ostream&, const_string_component const&);

bool operator==(const_string_component const&, byte_sequence_view_t);
bool operator!=(const_string_component const&, byte_sequence_view_t);

} // namespace whatwg::url::slim

#endif // URL_SLIM_STRING_COMPONENT_HPP
