#ifndef URL_SLIM_PORT_COMPONENT_HPP
#define URL_SLIM_PORT_COMPONENT_HPP

#include <whatwg/url/slim/string_component.hpp>

#include <cstdint>

namespace whatwg::url::slim {

/* act as std::optional<uint16_t> */

class port_component;

class const_port_component
{
public:
    const_port_component(url_record const&);

    const_port_component(const_port_component const&) = default;
    const_port_component(port_component const&);

    const_port_component& operator=(const_port_component const&) = delete;

    explicit operator bool() const;
    uint16_t operator*() const;

private:
    const_string_component m_comp;
};

class port_component
{
public:
    port_component(url_record&);

    port_component(port_component const&) = default;

    port_component& operator=(port_component const&);
    port_component& operator=(const_port_component const&);

    port_component& operator=(uint16_t);

    explicit operator bool() const;
    uint16_t operator*() const;

    void reset();

private:
    string_component m_comp;

    friend class const_port_component;
};

std::ostream& operator<<(std::ostream&, const_port_component const&);

bool operator==(const_port_component const&, const_port_component const&);
bool operator==(const_port_component const&, uint16_t);

} // namespace whatwg::url::slim

#endif // URL_SLIM_PORT_COMPONENT_HPP
