#ifndef URL_HOST_HPP
#define URL_HOST_HPP

#include <whatwg/url/error_handler.hpp>
#include <whatwg/url/string.hpp>

#include <array>
#include <cstdint>
#include <optional>
#include <variant>

namespace whatwg::url {

/*
 * type
 */
struct empty_host_t {};
struct null_host_t {};

struct domain_t : byte_sequence_t
{
    using byte_sequence_t::byte_sequence_t;
    using byte_sequence_t::operator=;
};

using ipv4_address_t = uint32_t;

using ipv6_piece_t = uint16_t;
using ipv6_address_t = std::array<ipv6_piece_t, 8>;

struct opaque_host_t : byte_sequence_t
{
    using byte_sequence_t::byte_sequence_t;
    using byte_sequence_t::operator=;
};

namespace detail {

using host_base_t = std::variant<
    null_host_t,
    domain_t,
    ipv4_address_t,
    ipv6_address_t,
    opaque_host_t,
    empty_host_t
>;

} // namespace detail

class host_t : public detail::host_base_t
{
public:
    using detail::host_base_t::host_base_t;
    using detail::host_base_t::operator=;

    host_t(host_t const&) = default;
    host_t(host_t&&) = default;

    host_t& operator=(host_t const&) = default;
    host_t& operator=(host_t&&) = default;

    // query
    bool is_null() const;
    bool is_empty() const;
    bool is_domain() const;
    bool is_ipv4_address() const;
    bool is_ipv6_address() const;
    bool is_opaque_host() const;
    bool is_localhost() const;

    // accessor
    byte_sequence_view_t domain() const;
    ipv4_address_t      ipv4_address() const;
    ipv6_address_t      ipv6_address() const;
    byte_sequence_view_t opaque_host() const;

    // modifier
    void set_empty_string();
};

// 3.5 Host parsing
class host_parser
{
public:
    host_parser();
    host_parser(error_handler_t);

    std::optional<host_t>
        operator()(string_view_t input,
                   bool is_not_special = false);

    std::optional<host_t> parse_host(string_view_t input,
                                     bool is_not_special = false) const;
    std::optional<int64_t> parse_ipv4_number(byte_sequence_view_t input,
                                             bool& validation_error_flag) const;
    std::optional<host_t> parse_ipv4(byte_sequence_view_t input) const;
    std::optional<host_t> parse_ipv6(string_view_t input) const;
    std::optional<host_t> parse_opaque_host(string_view_t input) const;

private:
    void validation_error(std::error_code const&) const;

private:
    string_view_t m_input;
    error_handler_t m_error_handler;
};

std::optional<host_t> parse_host(string_view_t input,
                                 bool is_not_special = false);

// 3.6 Host serializing
byte_sequence_t serialize_host(host_t const&);
byte_sequence_t serialize_ipv4(ipv4_address_t);
byte_sequence_t serialize_ipv6(ipv6_address_t const&);

} // namespace whatwg::url

/*
 * variant support
 */
namespace std {

template<>
struct variant_size<whatwg::url::host_t>
    : std::variant_size<whatwg::url::detail::host_base_t> {};

template<size_t I>
struct variant_alternative<I, whatwg::url::host_t>
    : std::variant_alternative<I, whatwg::url::detail::host_base_t> {};

} // namespace std

#endif // URL_HOST_HPP
