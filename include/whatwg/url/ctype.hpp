#ifndef URL_CTYPE_HPP
#define URL_CTYPE_HPP

#include <locale>
#include <cassert>

namespace std {

template<>
class ctype<char32_t> : public ctype_base, public locale::facet
{
public:
    using char_type = char32_t;

    static std::locale::id id;

    /*
     * is
     */
    bool is(mask const m, char32_t c) const
    {
        if (c > 0xff) return false;

        auto* const table = this->table();

        return table[static_cast<uint32_t>(c)] & m;
    }

    char32_t const* is(char32_t const* const low,
                       char32_t const* const high,
                       mask* vec) const
    {
        auto* const table = this->table();

        for (auto* ptr = low; ptr != high; ++ptr, ++vec) {
            if (*ptr > 0xff) {
                *vec = 0;
            }
            else {
                *vec = table[static_cast<uint32_t>(*ptr)];
            }
        }

        return high;
    }

    /*
     * scan_[is, not]
     */
    char32_t const* scan_is(mask const m,
                   char32_t const* beg, char32_t const* end) const
    {
        auto* const table = this->table();

        for (auto ptr = beg; ptr != end; ++ptr) {
            if (m != 0 && *ptr > 0xff) continue;

            if (table[static_cast<uint32_t>(*ptr)] == m) {
                return ptr;
            }
        }

        return end;
    }

    char32_t const* scan_not(mask const m,
                   char32_t const* beg, char32_t const* end) const
    {
        auto* const table = this->table();

        for (auto ptr = beg; ptr != end; ++ptr) {
            if (m == 0 && *ptr > 0xff) continue;

            if (table[static_cast<uint32_t>(*ptr)] != m) {
                return ptr;
            }
        }

        return end;
    }

    /*
     * toupper
     */
    char32_t toupper(char32_t const c) const
    {
        if (U'a' <= c && c <= U'z') {
            return U'A' + (c - U'a');
        }
        else {
            return c;
        }
    }

    char32_t const* toupper(char32_t* const beg,
                               char32_t const* const end) const
    {
        for (auto* p = beg; p != end; ++p) {
            if (U'a' <= *p && *p <= U'z') {
                *p = U'A' + *p - U'a';
            }
        }

        return end;
    }

    /*
     * tolower
     */
    char32_t tolower(char32_t const c) const
    {
        if (U'A' <= c && c <= U'Z') {
            return U'a' + c - U'A';
        }
        else {
            return c;
        }
    }

    char32_t const* tolower(char32_t* const beg,
                               char32_t const* const end) const
    {
        for (auto* p = beg; p != end; ++p) {
            if (U'A' <= *p && *p <= U'Z') {
                *p = U'a' + *p - U'A';
            }
        }

        return end;
    }

    /*
     * widen
     */
    char32_t widen(char const c) const
    {
        return static_cast<char32_t>(c);
    }

    char const* widen(char const* const beg,
                         char const* const end,
                         char32_t* dest) const
    {
        for (auto p = beg; p != end; ++p, ++dest) {
            *dest = widen(*p);
        }

        return end;
    }

    /*
     * narrow
     */
    char narrow(char32_t const c, char const dflt) const
    {
        return c <= 0xff ? static_cast<char>(c) : dflt;
    }

    char32_t const* narrow(char32_t const* const beg,
                              char32_t const* const end,
                              char const dflt,
                              char* dest) const
    {
        for (auto p = beg; p != end; ++p, ++dest) {
            *dest = narrow(*p, dflt);
        }

        return end;
    }

private:
    mask const* table() const
    {
        auto* const table = std::ctype<char>::classic_table();
        assert(table);

        return table;
    }
};

} // namespace std

#endif // URL_CTYPE_HPP
