#ifndef URL_URL_HPP
#define URL_URL_HPP

#include <whatwg/url/url_record.hpp>
#include <whatwg/url/url_search_params.hpp>

#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>

namespace whatwg::url {

// 6.1 URL class
class url
{
public:
    url(byte_sequence_view_t spec);
    url(byte_sequence_view_t spec, byte_sequence_view_t base);

    // accessor
    byte_sequence_t href() const;
    byte_sequence_t protocol() const;
    byte_sequence_t username() const;
    byte_sequence_t password() const;
    byte_sequence_t host() const;
    byte_sequence_t hostname() const;
    byte_sequence_t port() const;
    byte_sequence_t pathname() const;
    byte_sequence_t search() const;
    byte_sequence_t hash() const;

    // query
    byte_sequence_t origin() const;
    url_search_params& search_params();
    url_search_params const& search_params() const;
    byte_sequence_t to_string() const;

    // modifier
    void href(byte_sequence_view_t);
    void protocol(byte_sequence_view_t);
    void username(byte_sequence_view_t);
    void password(byte_sequence_view_t);
    void host(byte_sequence_view_t);
    void hostname(byte_sequence_view_t);
    void port(byte_sequence_view_t);
    void pathname(byte_sequence_view_t);
    void search(byte_sequence_view_t);
    void hash(byte_sequence_view_t);

private:
    void init(byte_sequence_view_t spec, std::optional<byte_sequence_view_t> base);

private:
    url_record m_url;
    url_search_params m_query_object;
};

class type_error : public std::runtime_error
{
public:
    type_error() : std::runtime_error { "type_error" } {};
    type_error(byte_sequence_t msg) : std::runtime_error { std::move(msg) } {};
};

} // namespace whatwg::url

#endif // URL_URL_HPP
