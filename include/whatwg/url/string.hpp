#ifndef URL_STRING_HPP
#define URL_STRING_HPP

#include <whatwg/url/char.hpp>

#include <whatwg/infra/byte_sequence.hpp>
#include <whatwg/infra/code_point.hpp>
#include <whatwg/infra/string.hpp>

#include <iosfwd>
#include <vector>

namespace whatwg::url {

/*
 * String
 */
using infra::byte_sequence_t;
using infra::string_t;

using infra::operator""_s;

/*
 * View
 */
using infra::byte_sequence_view_t;
using infra::string_view_t;

using infra::operator""_sv;

/*
 * assign
 */
inline void
assign(byte_sequence_t& lhs, string_view_t const rhs)
{
    byte_sequence_t tmp;
    tmp.reserve(rhs.size());

    for (auto const c: rhs) {
        assert(infra::is_ascii_code_point(c));

        tmp.push_back(to_ascii_no_check(c));
    }

    lhs.swap(tmp);
}

/*
 * append
 */
inline void
append(string_t& s, byte_t const c)
{
    append(s, static_cast<code_point_t>(c));
}

inline void
append(string_t& s1, byte_sequence_view_t const s2)
{
    std::copy(s2.begin(), s2.end(), std::back_inserter(s1));
}

inline void
append(byte_sequence_t& s1, string_view_t const s2)
{
    byte_sequence_t tmp;
    tmp.reserve(s2.size());

    for (auto const c: s2) {
        assert(infra::is_ascii_code_point(c));

        append(tmp, to_ascii_no_check(c));
    }

    s1.append(tmp);
}

/*
 * replace
 */
void replace_all(byte_sequence_t& s, byte_t c1, byte_t c2);

/*
 * split
 */
std::vector<byte_sequence_view_t> split(byte_sequence_view_t, byte_t sep);

/*
 * operator
 */
inline bool
operator==(byte_sequence_view_t const lhs, string_view_t const rhs)
{
    if (lhs.size() != rhs.size()) return false;

    for (size_t i = 0; i < lhs.size(); ++i) {
        if (static_cast<unsigned char>(lhs[i]) != rhs[i]) return false;
    }

    return true;
}

inline bool
operator==(string_view_t const lhs, byte_sequence_view_t const rhs)
{
    return rhs == lhs;
}

std::ostream& operator<<(std::ostream& os, string_view_t s);

} // namespace whatwg::url

#endif // URL_STRING_HPP
