#ifndef URL_RESULT_HPP
#define URL_RESULT_HPP

#include <cassert>
#include <system_error>

namespace whatwg::url {

template<typename T, typename E = std::error_code>
class result
{
public:
    result(T value) : m_value { std::move(value) } {}
    result(E error) : m_error { std::move(error) } {}

    constexpr T value() const { assert(has_value()); return m_value; }
    constexpr E error() const { return m_error; }

    constexpr T value_or(T const v) const
    {
        return has_value() ? value() : v;
    }

    constexpr bool has_value() const { return !error(); }

    constexpr T operator*() const { return value(); }
    constexpr operator bool() const { return has_value(); }

    template<typename U>
    bool operator==(result<U, E> const other) const
    {
        if (has_value()) {
            return other.has_value() ? equal(value(), other.value()) : false;
        }
        else {
            return other.has_value() ? false : error() == other.error();
        }
    }

    template<typename U>
    bool operator!=(result<U, E> const other) const
    {
        return !operator==(other);
    }

private:
    T m_value;
    E m_error;
};


} // namespace whatwg::url

#endif // URL_RESULT_HPP
