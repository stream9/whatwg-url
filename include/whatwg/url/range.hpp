#ifndef URL_RANGE_HPP
#define URL_RANGE_HPP

#include <whatwg/url/string.hpp>

#include <string>
#include <algorithm>

namespace whatwg::url {

template<typename RangeT, typename PredicateT>
bool
any_of(RangeT const s, PredicateT pred)
{
    return std::any_of(s.begin(), s.end(), pred);
}

template<typename RangeT, typename PredicateT>
bool
any_but_last_item_of(RangeT const r, PredicateT pred)
{
    if (r.empty()) return false;
    return std::any_of(r.begin(), r.end() - 1, pred);
}

template<typename RangeT, typename PredicateT>
bool
contains(RangeT const s, PredicateT pred)
{
    return any_of(s, pred);
}

template<typename RangeT>
auto
range(RangeT const& r)
{
    return std::make_pair(r.begin(), r.end());
}

} // namespace whatwg::url

#endif // URL_RANGE_HPP
