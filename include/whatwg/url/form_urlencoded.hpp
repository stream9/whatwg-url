#ifndef URL_FORM_URLENCODED_HPP
#define URL_FORM_URLENCODED_HPP

#include <whatwg/url/string.hpp>

#include <vector>
#include <utility>

namespace whatwg::encoding { class encoding; }

namespace whatwg::url {

using key_value_t = std::pair<byte_sequence_t, byte_sequence_t>;
using form_urlencoded_t = std::vector<key_value_t>;

// 5.1 application/x-www-form-urlencoded parsing
form_urlencoded_t
    parse_form_urlencoded(byte_sequence_view_t input);

// 5.2 application/x-www-form-urlencoded serializing
byte_sequence_t
    byte_serialize_form_urlencoded(byte_sequence_view_t input);

byte_sequence_t
    serialize_form_urlencoded(form_urlencoded_t const&,
                      encoding::encoding const* encoding_override = nullptr);

} // namespace whatwg::url

#endif // URL_FORM_URLENCODED_HPP
