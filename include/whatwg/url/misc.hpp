#ifndef URL_MISC_HPP
#define URL_MISC_HPP

#include <whatwg/url/string.hpp>
#include <whatwg/url/host.hpp>
#include <whatwg/url/url_record.hpp>

#include <cstdint>
#include <optional>

namespace whatwg::url {

// 1. Infrastructure
byte_sequence_t serialize_integer(int);

// 4.2 URL miscllaneous
bool is_special_scheme(string_view_t);

std::optional<uint16_t> default_port(byte_sequence_view_t scheme);

bool is_special(url_record const&);

bool includes_credentials(url_record const&);

bool cannot_have_a_username_password_port(url_record const&);

bool is_windows_drive_letter(string_view_t);
bool is_normalized_windows_drive_letter(byte_sequence_view_t);

bool starts_with_windows_drive_letter(string_view_t);

void shorten_urls_path(url_record&);

} // namespace whatwg::url

#endif // URL_MISC_HPP
