#ifndef URL_CHARACTER_ENCODING_HPP
#define URL_CHARACTER_ENCODING_HPP

#include <whatwg/url/string.hpp>

namespace whatwg::encoding { class encoding; }

namespace whatwg::url {

byte_sequence_t encode(code_point_t c, encoding::encoding const& encoding);
byte_sequence_t encode(string_t const& s, encoding::encoding const& encoding);

byte_sequence_t encode_utf8(code_point_t);
byte_sequence_t encode_utf8(string_view_t);

string_t decode_utf8_without_bom(byte_sequence_view_t);

} // namespace whatwg::url

#endif // URL_CHARACTER_ENCODING_HPP
