#ifndef URL_BASIC_URL_PARSER_HPP
#define URL_BASIC_URL_PARSER_HPP

#include <whatwg/url/string.hpp>
#include <whatwg/url/url_record.hpp>
#include <whatwg/url/error_handler.hpp>

#include <functional>
#include <iosfwd>
#include <optional>

namespace whatwg::encoding { class encoding; }

namespace whatwg::url {

enum class validation_error_code;

class basic_url_parser
{
public:
    enum class state {
        scheme_start = 0, scheme, no_scheme,
        special_relative_or_authority,
        path_or_authority,
        relative,
        relative_slash,
        special_authority_slashes,
        special_authority_ignore_slashes,
        authority, host, hostname, port,
        file, file_slash, file_host,
        path_start, path,
        cannot_be_a_base_url_path,
        query,
        fragment,
    };

    enum class result_t {
        success,
        failure,
        go_on,
    };

    using pointer_t = string_t::iterator;
    using state_handler_t = result_t (basic_url_parser::*)(code_point_t);

public:
    basic_url_parser();

    // modifier
    void set_validation_error_handler(error_handler_t);

    // operator
    std::optional<url_record> operator()(
        byte_sequence_view_t input,
        url_record const* base = nullptr,
        std::optional<byte_sequence_view_t> encoding_override = {});

    void operator()(
        byte_sequence_view_t input,
        url_record& url,
        state state_override,
        url_record const* base = nullptr,
        std::optional<byte_sequence_view_t> encoding_override = {});

private:
    // command
    result_t run();

    // state handler
    result_t scheme_start(code_point_t);
    result_t scheme(code_point_t);
    result_t no_scheme(code_point_t);
    result_t special_relative_or_authority(code_point_t);
    result_t path_or_authority(code_point_t);
    result_t relative(code_point_t);
    result_t relative_slash(code_point_t);
    result_t special_authority_slashes(code_point_t);
    result_t special_authority_ignore_slashes(code_point_t);
    result_t authority(code_point_t);
    result_t hostname(code_point_t);
    result_t port(code_point_t);
    result_t file(code_point_t);
    result_t file_slash(code_point_t);
    result_t file_host(code_point_t);
    result_t path_start(code_point_t);
    result_t path(code_point_t);
    result_t cannot_be_a_base_url_path(code_point_t);
    result_t query(code_point_t);
    result_t fragment(code_point_t);

    // utility
    string_view_t remaining() const;
    void validation_error(errc) const;

    static state_handler_t lookup_state_handler(state);

private:
    string_t m_input;
    url_record* m_url; // non-null
    url_record const* m_base;
    encoding::encoding const* m_encoding = nullptr; // non-null
    state m_state;
    std::optional<state> m_state_override;
    pointer_t m_pointer;
    string_t m_buffer;

    bool m_at_flag = false;
    bool m_bracket_flag = false;
    bool m_password_token_seen_flag = false;
    error_handler_t m_error_handler;
};

/*
 * free function
 */
void set_username(url_record&, byte_sequence_view_t);
void set_password(url_record&, byte_sequence_view_t);

std::optional<url_record>
    basic_url_parse(byte_sequence_view_t input,
                    url_record const* base = nullptr,
                    std::optional<byte_sequence_view_t> encoding_override = {});

void basic_url_parse(byte_sequence_view_t input,
                     url_record& url,
                     basic_url_parser::state state_override,
                     url_record const* base = nullptr,
                     std::optional<byte_sequence_view_t> encoding_override = {});

std::ostream& operator<<(std::ostream&, basic_url_parser::state);

} // namespace whatwg::url

#endif // URL_BASIC_URL_PARSER_HPP
