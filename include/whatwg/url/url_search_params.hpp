#ifndef URL_URL_SEARCH_PARAMS_HPP
#define URL_URL_SEARCH_PARAMS_HPP

#include <whatwg/url/form_urlencoded.hpp>

#include <map>
#include <string_view>
#include <utility>
#include <vector>

namespace whatwg::url {

class url;

// 6.2. URLSearchParams class
class url_search_params
{
public:
    using const_iterator = form_urlencoded_t::const_iterator;
    using key_value_pair_t = std::pair<byte_sequence_t, byte_sequence_t>;

public:
    url_search_params();
    url_search_params(byte_sequence_view_t init);
    url_search_params(std::vector<key_value_pair_t> const& init);
    url_search_params(std::map<byte_sequence_t, byte_sequence_t> const& init);

    // query
    std::optional<byte_sequence_t> get(byte_sequence_view_t name) const;
    std::vector<byte_sequence_t> get_all(byte_sequence_view_t name) const;
    bool has(byte_sequence_view_t name) const;
    byte_sequence_t to_string() const;

    // modifier
    void set(byte_sequence_view_t name, byte_sequence_view_t value);
    void append(byte_sequence_view_t name, byte_sequence_view_t value);
    void remove(byte_sequence_view_t name); // delete method on spec
    void sort();

    // iterator
    const_iterator begin() const { return m_list.begin(); }
    const_iterator end() const { return m_list.end(); }

private:
    void update();

    void clear();
    void set_url(url&);

private:
    form_urlencoded_t m_list;
    url* m_url = nullptr;

    friend class url;
};

} // namespace whatwg::url

#endif // URL_URL_SEARCH_PARAMS_HPP
