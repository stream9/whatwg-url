#ifndef URL_URL_RECORD_HPP
#define URL_URL_RECORD_HPP

#ifndef SLIM_URL_RECORD

#include <whatwg/url/fat/url_record.hpp>

#else

#include <whatwg/url/slim/url_record.hpp>

#endif

#endif // URL_URL_RECORD_HPP
