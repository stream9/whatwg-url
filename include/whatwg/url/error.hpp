#ifndef URL_ERROR_HPP
#define URL_ERROR_HPP

#include <string>
#include <system_error>

namespace whatwg::url {

enum class errc {
    // basic_url_parser
    input_contains_leading_or_trailing_c0_control_or_space,
    input_contains_ascii_tab_or_newline,
    illegal_character,
    file_url_doesnt_start_with_double_slash,
    no_scheme_without_base_url,
    special_relative_url,
    relative_starts_with_backslash,
    authority_doesnt_start_with_slash,
    contains_user_info,
    empty_host_with_user_info,
    empty_host_with_port,
    empty_host_with_user_info_or_port,
    empty_host_with_special_scheme,
    invalid_port,
    file_url_starts_with_backslash,
    file_url_starts_with_windows_drive_letter,
    special_url_has_backslash_in_path,
    file_url_has_host_and_windows_drive_letter,
    file_url_has_empty_path_elements,
    invalid_percent_encoded_char,
    null_in_fragment,
    // host_parser
    unmatch_bracket,
    invalid_code_point,
    invalid_ipv4_number,
    no_second_colon,
    second_compress_segment,
    invalid_period,
    invalid_ipv4_char,
    too_big_ipv4_piece,
    wrong_number_of_ipv4_pieces,
    ipv6_end_with_colon,
    premature_eof,
    wrong_number_of_ipv6_pieces,
};

std::error_category const& url_category();

inline std::error_code
    make_error_code(errc const e) noexcept
{
    return std::error_code { static_cast<int>(e), url_category() };
}

} // namespace whatwg::url

namespace std {

template<>
struct is_error_code_enum<whatwg::url::errc> : true_type {};

} // namespace std

#endif // URL_ERROR_HPP
