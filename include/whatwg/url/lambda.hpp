#ifndef URL_LAMBDA_HPP
#define URL_LAMBDA_HPP

namespace whatwg::url {

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

} // namespace whatwg::url

#endif // URL_LAMBDA_HPP
