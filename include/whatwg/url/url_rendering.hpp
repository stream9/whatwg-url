#ifndef URL_URL_RENDERING_HPP
#define URL_URL_RENDERING_HPP

#include <whatwg/url/string.hpp>
#include <whatwg/url/url_record.hpp>

namespace whatwg::url {

// 4.8 URL rendering
byte_sequence_t render_url(url_record const&, bool exclude_fragment = false);

} // namespace whatwg::url

#endif // URL_URL_RENDERING_HPP
