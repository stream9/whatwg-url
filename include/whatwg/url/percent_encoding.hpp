#ifndef URL_PERCENT_ENCODING_HPP
#define URL_PERCENT_ENCODING_HPP

#include <whatwg/url/string.hpp>

#include <whatwg/url/character_encoding.hpp>
#include <whatwg/url/conversion.hpp>
#include <whatwg/url/string.hpp>

#include <whatwg/infra/code_point.hpp>

namespace whatwg::url {

/*
 * encode
 */
inline string_t
percent_encode(byte_t const byte)
{
    auto to_digit = [](auto const byte) {
        return static_cast<char32_t>(number_to_upper_hex_digit(byte));
    };

    return {
        '%',
        to_digit((byte >> 4) & 0x0f),
        to_digit(byte & 0x0f),
    };
}

template<typename PredicateT>
string_t
utf8_percent_encode(code_point_t const c, PredicateT pred)
{
    string_t result;

    if (!pred(c)) {
        append(result, c);
    }
    else {
        auto const& bytes = encode_utf8(c);

        for (auto const byte: bytes) {
            append(result, percent_encode(byte));
        }
    }

    return result;
}

/*
 * decode
 */
byte_sequence_t percent_decode(byte_sequence_view_t);
byte_sequence_t string_percent_decode(string_view_t);

/*
 * predicates
 */
template<typename CharT>
bool
is_c0_control_percent_encode_char(CharT const c)
{
    return infra::is_c0_control(c) || c > '~';
}

inline bool
is_fragment_percent_encode_char(code_point_t const c)
{
    return is_c0_control_percent_encode_char(c) ||
           c == ' ' || c == '"' || c == '<' || c == '>' || c == '`';
}

inline bool
is_path_percent_encode_char(code_point_t const c)
{
    return is_fragment_percent_encode_char(c) ||
           c == '#' || c == '?' || c == '{' || c == '}';
}

inline bool
is_userinfo_percent_encode_char(code_point_t const c)
{
    return is_path_percent_encode_char(c) ||
        c == '/' || c == ':' || c == ';' || c == '=' || c == '@' ||
        c == '[' || c == '\\' || c == ']' || c == '^' || c == '|';
}

} // namespace whatwg::url

#endif // URL_PERCENT_ENCODING_HPP
