#ifndef URL_URL_SERIALIZING_HPP
#define URL_URL_SERIALIZING_HPP

#include <whatwg/url/string.hpp>
#include <whatwg/url/url_record.hpp>

namespace whatwg::url {

// 4.5 URL serializing
byte_sequence_t serialize_url(url_record const& url,
                             bool exclude_fragment = false);

} // namespace whatwg::url

#endif // URL_URL_SERIALIZING_HPP
