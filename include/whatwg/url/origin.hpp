#ifndef URL_ORIGIN_HPP
#define URL_ORIGIN_HPP

#include <whatwg/url/string.hpp>
#include <whatwg/url/url_record.hpp>

namespace whatwg::url {

/*
 * type
 */
class origin
{
public:
    origin() = default;
    origin(url_record const&);

    // accessor
    byte_sequence_t const& scheme() const { return m_scheme; }
    byte_sequence_t const& host() const { return m_host; }
    byte_sequence_t const& port() const { return m_port; }
    byte_sequence_t const* domain() const { return m_domain.get(); }

    // query
    bool is_opaque() const { return m_scheme.empty(); }
    byte_sequence_t serialize() const;

private:
    void init(url_record const&);

private:
    byte_sequence_t m_scheme;
    byte_sequence_t m_host;
    byte_sequence_t m_port;
    std::unique_ptr<byte_sequence_t> m_domain;
};

} // namespace whatwg::url

#endif // URL_ORIGIN_HPP
