#include "setters_tests.hpp"

#include <filesystem>
#include <iostream>
#include <tuple>

#include <json/json.hpp>
#include <json/array.hpp>
#include <json/value.hpp>
#include <json/object.hpp>

#include <whatwg/url/url.hpp>

#include "url_test.hpp"

namespace tst = boost::unit_test;
namespace fs = std::filesystem;
namespace url = whatwg::url;

namespace setters_tests {

static auto
unpack_test_case(json::object const& obj)
{
    assert(obj.contain("href"));
    auto const& href = obj.at("href").get_string();

    assert(obj.contain("new_value"));
    auto const& new_value = obj.at("new_value").get_string();

    assert(obj.contain("expected"));
    auto const& expected = obj.at("expected").get_object();

    return std::make_tuple(href, new_value, expected);
}

static std::string
get_attr(url::url const& u, std::string_view const name)
{
#define GET_ATTR(n) if (name == #n) return u.n()
    GET_ATTR(href);
    GET_ATTR(origin);
    GET_ATTR(protocol);
    GET_ATTR(username);
    GET_ATTR(password);
    GET_ATTR(host);
    GET_ATTR(hostname);
    GET_ATTR(port);
    GET_ATTR(pathname);
    GET_ATTR(search);
    GET_ATTR(hash);
#undef GET_ATTR

    std::cerr << "unknown attr: " << name << "\n";
    assert(false);
}

static bool
check_result(url::url const& u, json::object const& expected)
{
    bool ok = true;

    for (auto const& [k, v]: expected) {
        auto const& name = k.get_string();
        auto const& value = v.get_string();

        BOOST_CHECK_EQUAL(get_attr(u, name), value);
        if (get_attr(u, name) != value) {
            std::cout << "mismatch: " << name << "\n";
            ok = false;
        }
    }

    return ok;
}

template<typename SetterFn>
void
test_setter(json::object const& obj, SetterFn setter)
{
    auto const& [href, new_value, expected] = unpack_test_case(obj);

    try {
        url::url u { href };

        (u.*setter)(new_value);

        if (!check_result(u, expected)) {
            std::cout << "{ " << href << ", " << new_value << ", " << expected << " }\n";
        }
    }
    catch (url::type_error&) {
        std::cerr << "type_error: " << href << "\n";
    }
}

using setter_fn = void (url::url::*)(url::byte_sequence_view_t);

static setter_fn
get_setter(std::string_view const name)
{
#define GET_SETTER(n) if (name == #n) return static_cast<setter_fn>(&url::url::n)
    GET_SETTER(protocol);
    GET_SETTER(username);
    GET_SETTER(password);
    GET_SETTER(host);
    GET_SETTER(hostname);
    GET_SETTER(port);
    GET_SETTER(pathname);
    GET_SETTER(search);
    GET_SETTER(hash);
#undef GET_SETTER

    assert(false);
}

static void
make_test_suite(tst::test_suite& parent,
                std::string_view const name,
                json::array const& array)
{
    std::string name_ { name };
    auto* const suite = BOOST_TEST_SUITE(name_.c_str());
    assert(suite);

    auto n = 0;
    for (auto const& v: array) {
        assert(v.is_object());
        auto obj = v.get_object();

        auto const label = "no." + std::to_string(++n);

        auto case_ = new tst::test_case(
            label.data(),
            __FILE__, __LINE__,
            [obj, name]() {
                test_setter(obj, get_setter(name));
            }
        );

        suite->add(case_);
    }

    parent.add(suite);
}

bool
init_test_suite(tst::test_suite& parent)
{
    fs::path dir { DATA_DIRECTORY };
    if (!exists(dir)) {
        std::cerr << "data directory doesn't exists\n";
        return false;
    }

    auto const& path = dir / "setters_tests.json";
    if (!exists(dir)) {
        return false;
    }

    auto* const suite = BOOST_TEST_SUITE(path.stem().c_str());
    assert(suite);

    auto const& test_data = slurp_file(path);

    auto const& object = json::parse(test_data).get_object();

    for (auto const& [key, value]: object) {
        assert(key.is_string());
        auto const& name = key.get_string();

        if (name == "comment") continue;

        if (!value.is_array()) {
            std::cerr << "value for " << name << " isn't an array.\n";
            continue;
        }

        auto const& array = value.get_array();

        make_test_suite(*suite, name, array);
    }

    parent.add(suite);

    return true;
}

} // namespace setters_tests
