#include "utility.hpp"

#include <sstream>

#include <boost/test/unit_test.hpp>

#include <whatwg/url/slim/url_record.hpp>
#include <whatwg/url/slim/string_component.hpp>

namespace whatwg::url::slim {

template<typename T1, typename T2>
void type_is(T2 const&)
{
    static_assert(
        std::is_same_v<
            T1,
            std::decay_t<T2>
        >);
}

BOOST_AUTO_TEST_SUITE(const_string_component_)

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_string_component>(u.scheme());
        auto const scheme { u.scheme() };
        BOOST_CHECK_EQUAL(scheme, u.scheme());

        type_is<const_string_component>(u.username());
        auto const username { u.username() };
        BOOST_CHECK_EQUAL(username, u.username());

        type_is<const_string_component>(u.password());
        auto const password { u.password() };
        BOOST_CHECK_EQUAL(password, u.password());

        type_is<const_string_component>(u.query());
        auto const query { u.query() };
        BOOST_CHECK_EQUAL(query, u.query());

        type_is<const_string_component>(u.fragment());
        auto const fragment { u.fragment() };
        BOOST_CHECK_EQUAL(fragment, u.fragment());
    }

    BOOST_AUTO_TEST_CASE(convert_from_string_component)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<string_component>(u.scheme());
        const_string_component scheme = u.scheme();
        BOOST_CHECK_EQUAL(scheme, u.scheme());

        type_is<string_component>(u.username());
        const_string_component username = u.username();
        BOOST_CHECK_EQUAL(username, u.username());

        type_is<string_component>(u.password());
        const_string_component password = u.password();
        BOOST_CHECK_EQUAL(password, u.password());

        type_is<string_component>(u.query());
        const_string_component query = u.query();
        BOOST_CHECK_EQUAL(query, u.query());

        type_is<string_component>(u.fragment());
        const_string_component fragment = u.fragment();
        BOOST_CHECK_EQUAL(fragment, u.fragment());
    }

    BOOST_AUTO_TEST_CASE(url_accessor)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_string_component>(u.scheme());
        BOOST_CHECK_EQUAL(&u.scheme().url(), &u);

        type_is<const_string_component>(u.username());
        BOOST_CHECK_EQUAL(&u.username().url(), &u);

        type_is<const_string_component>(u.password());
        BOOST_CHECK_EQUAL(&u.password().url(), &u);

        type_is<const_string_component>(u.query());
        BOOST_CHECK_EQUAL(&u.query().url(), &u);

        type_is<const_string_component>(u.fragment());
        BOOST_CHECK_EQUAL(&u.fragment().url(), &u);
    }

    BOOST_AUTO_TEST_CASE(dereference)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        auto scheme = u.scheme();
        type_is<const_string_component>(scheme);
        BOOST_CHECK_EQUAL(*scheme, scheme);

        auto username = u.username();
        type_is<const_string_component>(username);
        BOOST_CHECK_EQUAL(*username, username);

        auto password = u.password();
        type_is<const_string_component>(password);
        BOOST_CHECK_EQUAL(*password, password);

        auto query = u.query();
        type_is<const_string_component>(query);
        BOOST_CHECK_EQUAL(*query, query);

        auto fragment = u.fragment();
        type_is<const_string_component>(fragment);
        BOOST_CHECK_EQUAL(*fragment, fragment);
    }

    BOOST_AUTO_TEST_CASE(convert_to_bool_1)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_string_component>(u.scheme());
        BOOST_CHECK(u.scheme());

        type_is<const_string_component>(u.username());
        BOOST_CHECK(u.username());

        type_is<const_string_component>(u.password());
        BOOST_CHECK(u.password());

        type_is<const_string_component>(u.query());
        BOOST_CHECK(u.query());

        type_is<const_string_component>(u.fragment());
        BOOST_CHECK(u.fragment());
    }

    BOOST_AUTO_TEST_CASE(convert_to_bool_2)
    {
        auto const u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );

        type_is<const_string_component>(u.scheme());
        BOOST_CHECK(u.scheme());

        type_is<const_string_component>(u.username());
        BOOST_CHECK(u.username());

        type_is<const_string_component>(u.password());
        BOOST_CHECK(u.password());

        type_is<const_string_component>(u.query());
        BOOST_CHECK(!u.query());

        type_is<const_string_component>(u.fragment());
        BOOST_CHECK(!u.fragment());
    }

    BOOST_AUTO_TEST_CASE(empty)
    {
        auto const u = construct_and_verify(
            "", "", "", "host", 1234,
            "/path", "", "",
            "://host:1234/path?#"
        );

        type_is<const_string_component>(u.scheme());
        BOOST_CHECK(u.scheme().empty());

        type_is<const_string_component>(u.username());
        BOOST_CHECK(u.username().empty());

        type_is<const_string_component>(u.password());
        BOOST_CHECK(u.password().empty());

        type_is<const_string_component>(u.query());
        BOOST_CHECK(u.query().empty());

        type_is<const_string_component>(u.fragment());
        BOOST_CHECK(u.fragment().empty());
    }

    BOOST_AUTO_TEST_CASE(convert_to_string_view)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_string_component>(u.scheme());
        byte_sequence_view_t const scheme = u.scheme();
        BOOST_CHECK_EQUAL(scheme, "http");

        type_is<const_string_component>(u.username());
        byte_sequence_view_t const username = u.username();
        BOOST_CHECK_EQUAL(username, "user");

        type_is<const_string_component>(u.password());
        byte_sequence_view_t const password = u.password();
        BOOST_CHECK_EQUAL(password, "pass");

        type_is<const_string_component>(u.query());
        byte_sequence_view_t const query = u.query();
        BOOST_CHECK_EQUAL(query, "query");

        type_is<const_string_component>(u.fragment());
        byte_sequence_view_t const fragment = u.fragment();
        BOOST_CHECK_EQUAL(fragment, "fragment");
    }

BOOST_AUTO_TEST_SUITE_END() // const_string_component_

BOOST_AUTO_TEST_SUITE(string_component_)

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        string_component const scheme { u.scheme() };
        BOOST_CHECK_EQUAL(scheme, u.scheme());

        string_component const username { u.username() };
        BOOST_CHECK_EQUAL(username, u.username());

        string_component const password { u.password() };
        BOOST_CHECK_EQUAL(password, u.password());

        string_component const query { u.query() };
        BOOST_CHECK_EQUAL(query, u.query());

        string_component const fragment { u.fragment() };
        BOOST_CHECK_EQUAL(fragment, u.fragment());
    }

    BOOST_AUTO_TEST_CASE(copy_assignment_normal)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u1;

        auto u2 = construct_and_verify(
            "HTTP", "USER", "PASS", "host", 1234,
            "/path", "QUERY", "FRAGMENT",
            "HTTP://USER:PASS@host:1234/path?QUERY#FRAGMENT"
        );

        u1.scheme() = u2.scheme();
        BOOST_CHECK_EQUAL(u1,
            "HTTP://user:pass@host:1234/path?query#fragment");

        u1 = orig;
        u1.username() = u2.username();
        BOOST_CHECK_EQUAL(u1,
            "http://USER:pass@host:1234/path?query#fragment");

        u1 = orig;
        u1.password() = u2.password();
        BOOST_CHECK_EQUAL(u1,
            "http://user:PASS@host:1234/path?query#fragment");

        u1 = orig;
        u1.query() = u2.query();
        BOOST_CHECK_EQUAL(u1,
            "http://user:pass@host:1234/path?QUERY#fragment");

        u1 = orig;
        u1.fragment() = u2.fragment();
        BOOST_CHECK_EQUAL(u1,
            "http://user:pass@host:1234/path?query#FRAGMENT");
    }

    BOOST_AUTO_TEST_CASE(copy_assignment_from_null)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u1;

        auto u2 = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );

        u1.query() = u2.query();
        BOOST_CHECK_EQUAL(u1,
            "http://user:pass@host:1234/path#fragment");

        u1 = orig;
        u1.fragment() = u2.fragment();
        BOOST_CHECK_EQUAL(u1,
            "http://user:pass@host:1234/path?query");
    }

    BOOST_AUTO_TEST_CASE(copy_assignment_to_null)
    {
        auto u1 = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );

        auto orig = u1;

        auto u2 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        u1.query() = u2.query();
        BOOST_CHECK_EQUAL(u1, "://host:1234/path?query");

        u1 = orig;
        u1.fragment() = u2.fragment();
        BOOST_CHECK_EQUAL(u1, "://host:1234/path#fragment");
    }

    BOOST_AUTO_TEST_CASE(assign_same_length)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme() = "HTTP";
        BOOST_CHECK_EQUAL(u,
            "HTTP://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username() = "USER";
        BOOST_CHECK_EQUAL(u,
            "http://USER:pass@host:1234/path?query#fragment");

        u = orig;
        u.password() = "PASS";
        BOOST_CHECK_EQUAL(u,
            "http://user:PASS@host:1234/path?query#fragment");

        u = orig;
        u.query() = "QUERY";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?QUERY#fragment");

        u = orig;
        u.fragment() = "FRAGMENT";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#FRAGMENT");
    }

    BOOST_AUTO_TEST_CASE(assign_longer)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme() = "httpX";
        BOOST_CHECK_EQUAL(u,
            "httpX://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username() = "userX";
        BOOST_CHECK_EQUAL(u,
            "http://userX:pass@host:1234/path?query#fragment");

        u = orig;
        u.password() = "passX";
        BOOST_CHECK_EQUAL(u,
            "http://user:passX@host:1234/path?query#fragment");

        u = orig;
        u.query() = "queryX";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?queryX#fragment");

        u = orig;
        u.fragment() = "fragmentX";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragmentX");
    }

    BOOST_AUTO_TEST_CASE(assign_shorter)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme() = "htt";
        BOOST_CHECK_EQUAL(u,
            "htt://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username() = "use";
        BOOST_CHECK_EQUAL(u,
            "http://use:pass@host:1234/path?query#fragment");

        u = orig;
        u.password() = "pas";
        BOOST_CHECK_EQUAL(u,
            "http://user:pas@host:1234/path?query#fragment");

        u = orig;
        u.query() = "quer";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?quer#fragment");

        u = orig;
        u.fragment() = "fragmen";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragmen");
    }

    BOOST_AUTO_TEST_CASE(assign_truncate)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme() = "";
        BOOST_CHECK_EQUAL(u,
            "://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username() = "";
        BOOST_CHECK_EQUAL(u,
            "http://:pass@host:1234/path?query#fragment");

        u = orig;
        u.password() = "";
        BOOST_CHECK_EQUAL(u,
            "http://user@host:1234/path?query#fragment");

        u = orig;
        u.query() = "";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?#fragment");

        u = orig;
        u.fragment() = "";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#");
    }

    BOOST_AUTO_TEST_CASE(assign32_same_length)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme() = U"HTTP";
        BOOST_CHECK_EQUAL(u,
            "HTTP://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username() = U"USER";
        BOOST_CHECK_EQUAL(u,
            "http://USER:pass@host:1234/path?query#fragment");

        u = orig;
        u.password() = U"PASS";
        BOOST_CHECK_EQUAL(u,
            "http://user:PASS@host:1234/path?query#fragment");

        u = orig;
        u.query() = U"QUERY";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?QUERY#fragment");

        u = orig;
        u.fragment() = U"FRAGMENT";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#FRAGMENT");
    }

    BOOST_AUTO_TEST_CASE(assign32_longer)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme() = U"httpX";
        BOOST_CHECK_EQUAL(u,
            "httpX://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username() = U"userX";
        BOOST_CHECK_EQUAL(u,
            "http://userX:pass@host:1234/path?query#fragment");

        u = orig;
        u.password() = U"passX";
        BOOST_CHECK_EQUAL(u,
            "http://user:passX@host:1234/path?query#fragment");

        u = orig;
        u.query() = U"queryX";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?queryX#fragment");

        u = orig;
        u.fragment() = U"fragmentX";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragmentX");
    }

    BOOST_AUTO_TEST_CASE(assign32_shorter)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme() = U"htt";
        BOOST_CHECK_EQUAL(u,
            "htt://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username() = U"use";
        BOOST_CHECK_EQUAL(u,
            "http://use:pass@host:1234/path?query#fragment");

        u = orig;
        u.password() = U"pas";
        BOOST_CHECK_EQUAL(u,
            "http://user:pas@host:1234/path?query#fragment");

        u = orig;
        u.query() = U"quer";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?quer#fragment");

        u = orig;
        u.fragment() = U"fragmen";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragmen");
    }

    BOOST_AUTO_TEST_CASE(assign32_truncate)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme() = U"";
        BOOST_CHECK_EQUAL(u,
            "://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username() = U"";
        BOOST_CHECK_EQUAL(u,
            "http://:pass@host:1234/path?query#fragment");

        u = orig;
        u.password() = U"";
        BOOST_CHECK_EQUAL(u,
            "http://user@host:1234/path?query#fragment");

        u = orig;
        u.query() = U"";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?#fragment");

        u = orig;
        u.fragment() = U"";
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#");
    }

    BOOST_AUTO_TEST_CASE(url_accessor)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<string_component>(u.scheme());
        BOOST_CHECK_EQUAL(&u.scheme().url(), &u);

        type_is<string_component>(u.username());
        BOOST_CHECK_EQUAL(&u.username().url(), &u);

        type_is<string_component>(u.password());
        BOOST_CHECK_EQUAL(&u.password().url(), &u);

        type_is<string_component>(u.query());
        BOOST_CHECK_EQUAL(&u.query().url(), &u);

        type_is<string_component>(u.fragment());
        BOOST_CHECK_EQUAL(&u.fragment().url(), &u);
    }

    BOOST_AUTO_TEST_CASE(append_char)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        append(u.scheme(), 'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://user:pass@host:1234/path?query#fragment");

        append(u.username(), 'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://userX:pass@host:1234/path?query#fragment");

        append(u.password(), 'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://userX:passX@host:1234/path?query#fragment");

        append(u.query(), 'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://userX:passX@host:1234/path?queryX#fragment");

        append(u.fragment(), 'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://userX:passX@host:1234/path?queryX#fragmentX");
    }

    BOOST_AUTO_TEST_CASE(append_char_to_null)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );
        auto orig = u;

        append(u.query(), 'X');
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path?X");

        u = orig;
        append(u.fragment(), 'X');
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path#X");
    }

    BOOST_AUTO_TEST_CASE(append_byte_sequence)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        append(u.scheme(), "XX");
        BOOST_CHECK_EQUAL(u,
            "httpXX://user:pass@host:1234/path?query#fragment");

        u = orig;
        append(u.username(), "XX");
        BOOST_CHECK_EQUAL(u,
            "http://userXX:pass@host:1234/path?query#fragment");

        u = orig;
        append(u.password(), "XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:passXX@host:1234/path?query#fragment");

        u = orig;
        append(u.query(), "XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?queryXX#fragment");

        u = orig;
        append(u.fragment(), "XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragmentXX");
    }

    BOOST_AUTO_TEST_CASE(append_empty_byte_sequence)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        append(u.scheme(), "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        append(u.username(), "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        append(u.password(), "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        append(u.query(), "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        append(u.fragment(), "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(append_byte_sequence_to_null)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );
        auto orig = u;

        append(u.query(), "XX");
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path?XX");

        u = orig;
        append(u.fragment(), "XX");
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path#XX");
    }

    BOOST_AUTO_TEST_CASE(append_char32)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        append(u.scheme(), U'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://user:pass@host:1234/path?query#fragment");

        append(u.username(), U'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://userX:pass@host:1234/path?query#fragment");

        append(u.password(), U'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://userX:passX@host:1234/path?query#fragment");

        append(u.query(), U'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://userX:passX@host:1234/path?queryX#fragment");

        append(u.fragment(), U'X');
        BOOST_CHECK_EQUAL(u,
            "httpX://userX:passX@host:1234/path?queryX#fragmentX");
    }

    BOOST_AUTO_TEST_CASE(append_char32_to_null)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );
        auto orig = u;

        append(u.query(), U'X');
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path?X");

        u = orig;
        append(u.fragment(), U'X');
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path#X");
    }

    BOOST_AUTO_TEST_CASE(append_string)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        append(u.scheme(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "httpXX://user:pass@host:1234/path?query#fragment");

        u = orig;
        append(u.username(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "http://userXX:pass@host:1234/path?query#fragment");

        u = orig;
        append(u.password(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:passXX@host:1234/path?query#fragment");

        u = orig;
        append(u.query(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?queryXX#fragment");

        u = orig;
        append(u.fragment(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragmentXX");
    }

    BOOST_AUTO_TEST_CASE(append_empty_string)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().append(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().append(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().append(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.query().append(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.fragment().append(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(append_string_to_null)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );
        auto orig = u;

        u.query().append(U"XX");
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path?XX");

        u = orig;
        u.fragment().append(U"XX");
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path#XX");
    }

    BOOST_AUTO_TEST_CASE(prepend_char)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        prepend(u.scheme(), 'X');
        BOOST_CHECK_EQUAL(u,
            "Xhttp://user:pass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.username(), 'X');
        BOOST_CHECK_EQUAL(u,
            "http://Xuser:pass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.password(), 'X');
        BOOST_CHECK_EQUAL(u,
            "http://user:Xpass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.query(), 'X');
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?Xquery#fragment");

        u = orig;
        prepend(u.fragment(), 'X');
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#Xfragment");
    }

    BOOST_AUTO_TEST_CASE(prepend_char_to_null)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );
        auto orig = u;

        u.query().prepend('X');
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path?X");

        u = orig;
        u.fragment().prepend('X');
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path#X");
    }

    BOOST_AUTO_TEST_CASE(prepend_byte_sequence)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        prepend(u.scheme(), "XX");
        BOOST_CHECK_EQUAL(u,
            "XXhttp://user:pass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.username(), "XX");
        BOOST_CHECK_EQUAL(u,
            "http://XXuser:pass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.password(), "XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:XXpass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.query(), "XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?XXquery#fragment");

        u = orig;
        prepend(u.fragment(), "XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#XXfragment");
    }

    BOOST_AUTO_TEST_CASE(prepend_empty_byte_sequence)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().prepend("");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().prepend("");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().prepend("");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.query().prepend("");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.fragment().prepend("");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(prepend_byte_sequence_to_null)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );
        auto orig = u;

        u.query().prepend("XX");
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path?XX");

        u = orig;
        u.fragment().prepend("XX");
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path#XX");
    }

    BOOST_AUTO_TEST_CASE(prepend_char32)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        prepend(u.scheme(), U'X');
        BOOST_CHECK_EQUAL(u,
            "Xhttp://user:pass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.username(), U'X');
        BOOST_CHECK_EQUAL(u,
            "http://Xuser:pass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.password(), U'X');
        BOOST_CHECK_EQUAL(u,
            "http://user:Xpass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.query(), U'X');
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?Xquery#fragment");

        u = orig;
        prepend(u.fragment(), U'X');
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#Xfragment");
    }

    BOOST_AUTO_TEST_CASE(prepend_char32_to_null)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );
        auto orig = u;

        u.query().prepend(U'X');
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path?X");

        u = orig;
        u.fragment().prepend(U'X');
        BOOST_CHECK_EQUAL(u,
            "://host:1234/path#X");
    }

    BOOST_AUTO_TEST_CASE(prepend_string)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        prepend(u.scheme(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "XXhttp://user:pass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.username(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "http://XXuser:pass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.password(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:XXpass@host:1234/path?query#fragment");

        u = orig;
        prepend(u.query(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?XXquery#fragment");

        u = orig;
        prepend(u.fragment(), U"XX");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#XXfragment");
    }

    BOOST_AUTO_TEST_CASE(prepend_empty_string)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().prepend(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().prepend(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().prepend(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.query().prepend(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.fragment().prepend(U"");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(prepend_string_to_null)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );
        auto orig = u;

        u.query().prepend(U"XX");
        BOOST_CHECK_EQUAL(u, "://host:1234/path?XX");

        u = orig;
        u.fragment().prepend(U"XX");
        BOOST_CHECK_EQUAL(u, "://host:1234/path#XX");
    }

    BOOST_AUTO_TEST_CASE(insert_to_front)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().insert(0, "X");
        BOOST_CHECK_EQUAL(u,
            "Xhttp://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().insert(0, "X");
        BOOST_CHECK_EQUAL(u,
            "http://Xuser:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().insert(0, "X");
        BOOST_CHECK_EQUAL(u,
            "http://user:Xpass@host:1234/path?query#fragment");

        u = orig;
        u.query().insert(0, "X");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?Xquery#fragment");

        u = orig;
        u.fragment().insert(0, "X");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#Xfragment");
    }

    BOOST_AUTO_TEST_CASE(insert_to_back)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().insert(4, "X");
        BOOST_CHECK_EQUAL(u,
            "httpX://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().insert(4, "X");
        BOOST_CHECK_EQUAL(u,
            "http://userX:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().insert(4, "X");
        BOOST_CHECK_EQUAL(u,
            "http://user:passX@host:1234/path?query#fragment");

        u = orig;
        u.query().insert(5, "X");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?queryX#fragment");

        u = orig;
        u.fragment().insert(8, "X");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragmentX");
    }

    BOOST_AUTO_TEST_CASE(insert_to_middle)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().insert(1, "_");
        BOOST_CHECK_EQUAL(u,
            "h_ttp://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().insert(1, "_");
        BOOST_CHECK_EQUAL(u,
            "http://u_ser:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().insert(1, "_");
        BOOST_CHECK_EQUAL(u,
            "http://user:p_ass@host:1234/path?query#fragment");

        u = orig;
        u.query().insert(1, "_");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?q_uery#fragment");

        u = orig;
        u.fragment().insert(1, "_");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#f_ragment");
    }

    BOOST_AUTO_TEST_CASE(insert_empty)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().insert(0, "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().insert(0, "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().insert(0, "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.query().insert(0, "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.fragment().insert(0, "");
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(erase_front)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().erase(0, 1);
        BOOST_CHECK_EQUAL(u,
            "ttp://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().erase(0, 1);
        BOOST_CHECK_EQUAL(u,
            "http://ser:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().erase(0, 1);
        BOOST_CHECK_EQUAL(u,
            "http://user:ass@host:1234/path?query#fragment");

        u = orig;
        u.query().erase(0, 1);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?uery#fragment");

        u = orig;
        u.fragment().erase(0, 1);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#ragment");
    }

    BOOST_AUTO_TEST_CASE(erase_back)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().erase(3, 1);
        BOOST_CHECK_EQUAL(u,
            "htt://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().erase(3, 1);
        BOOST_CHECK_EQUAL(u,
            "http://use:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().erase(3, 1);
        BOOST_CHECK_EQUAL(u,
            "http://user:pas@host:1234/path?query#fragment");

        u = orig;
        u.query().erase(4, 1);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?quer#fragment");

        u = orig;
        u.fragment().erase(7, 1);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragmen");
    }

    BOOST_AUTO_TEST_CASE(erase_middle)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().erase(1, 1);
        BOOST_CHECK_EQUAL(u,
            "htp://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().erase(1, 1);
        BOOST_CHECK_EQUAL(u,
            "http://uer:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().erase(1, 1);
        BOOST_CHECK_EQUAL(u,
            "http://user:pss@host:1234/path?query#fragment");

        u = orig;
        u.query().erase(1, 1);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?qery#fragment");

        u = orig;
        u.fragment().erase(1, 1);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fagment");
    }

    BOOST_AUTO_TEST_CASE(erase_empty)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().erase(1, 0);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().erase(1, 0);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().erase(1, 0);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.query().erase(1, 0);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.fragment().erase(1, 0);
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(clear)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().clear();
        BOOST_CHECK_EQUAL(u,
            "://user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().clear();
        BOOST_CHECK_EQUAL(u,
            "http://:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().clear();
        BOOST_CHECK_EQUAL(u,
            "http://user@host:1234/path?query#fragment");

        u = orig;
        u.query().clear();
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?#fragment");

        u = orig;
        u.fragment().clear();
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query#");
    }

    BOOST_AUTO_TEST_CASE(clear_null)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );
        auto orig = u;

        u.query().clear();
        BOOST_CHECK_EQUAL(u, "://host:1234/path?");

        u = orig;
        u.fragment().clear();
        BOOST_CHECK_EQUAL(u, "://host:1234/path#");
    }

    BOOST_AUTO_TEST_CASE(reset_1)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        u.scheme().reset();
        BOOST_CHECK_EQUAL(u,
            "//user:pass@host:1234/path?query#fragment");

        u.username().reset();
        BOOST_CHECK_EQUAL(u,
            "//:pass@host:1234/path?query#fragment");

        u.password().reset();
        BOOST_CHECK_EQUAL(u,
            "//host:1234/path?query#fragment");

        u.query().reset();
        BOOST_CHECK_EQUAL(u,
            "//host:1234/path#fragment");

        u.fragment().reset();
        BOOST_CHECK_EQUAL(u,
            "//host:1234/path");
    }

    BOOST_AUTO_TEST_CASE(reset_2)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        u.fragment().reset();
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query");

        u.query().reset();
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path");

        u.password().reset();
        BOOST_CHECK_EQUAL(u,
            "http://user@host:1234/path");

        u.username().reset();
        BOOST_CHECK_EQUAL(u,
            "http://host:1234/path");

        u.scheme().reset();
        BOOST_CHECK_EQUAL(u,
            "//host:1234/path");
    }

    BOOST_AUTO_TEST_CASE(reset_3)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        auto orig = u;

        u.scheme().reset();
        BOOST_CHECK_EQUAL(u,
            "//user:pass@host:1234/path?query#fragment");

        u = orig;
        u.username().reset();
        BOOST_CHECK_EQUAL(u,
            "http://:pass@host:1234/path?query#fragment");

        u = orig;
        u.password().reset();
        BOOST_CHECK_EQUAL(u,
            "http://user@host:1234/path?query#fragment");

        u = orig;
        u.query().reset();
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path#fragment");

        u = orig;
        u.fragment().reset();
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:1234/path?query");
    }

    BOOST_AUTO_TEST_CASE(dereference)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        auto scheme = u.scheme();
        type_is<string_component>(scheme);
        BOOST_CHECK_EQUAL(*scheme, scheme);

        auto username = u.username();
        type_is<string_component>(username);
        BOOST_CHECK_EQUAL(*username, username);

        auto password = u.password();
        type_is<string_component>(password);
        BOOST_CHECK_EQUAL(*password, password);

        auto query = u.query();
        type_is<string_component>(query);
        BOOST_CHECK_EQUAL(*query, query);

        auto fragment = u.fragment();
        type_is<string_component>(fragment);
        BOOST_CHECK_EQUAL(*fragment, fragment);
    }

    BOOST_AUTO_TEST_CASE(convert_to_bool_1)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<string_component>(u.scheme());
        BOOST_CHECK(u.scheme());

        type_is<string_component>(u.username());
        BOOST_CHECK(u.username());

        type_is<string_component>(u.password());
        BOOST_CHECK(u.password());

        type_is<string_component>(u.query());
        BOOST_CHECK(u.query());

        type_is<string_component>(u.fragment());
        BOOST_CHECK(u.fragment());
    }

    BOOST_AUTO_TEST_CASE(convert_to_bool_2)
    {
        auto u = construct_and_verify(
            {}, {}, {}, "host", 1234,
            "/path", {}, {},
            "://host:1234/path"
        );

        type_is<string_component>(u.scheme());
        BOOST_CHECK(u.scheme());

        type_is<string_component>(u.username());
        BOOST_CHECK(u.username());

        type_is<string_component>(u.password());
        BOOST_CHECK(u.password());

        type_is<string_component>(u.query());
        BOOST_CHECK(!u.query());

        type_is<string_component>(u.fragment());
        BOOST_CHECK(!u.fragment());
    }

    BOOST_AUTO_TEST_CASE(empty)
    {
        auto u = construct_and_verify(
            "", "", "", "host", 1234,
            "/path", "", "",
            "://host:1234/path?#"
        );

        type_is<string_component>(u.scheme());
        BOOST_CHECK(u.scheme().empty());

        type_is<string_component>(u.username());
        BOOST_CHECK(u.username().empty());

        type_is<string_component>(u.password());
        BOOST_CHECK(u.password().empty());

        type_is<string_component>(u.query());
        BOOST_CHECK(u.query().empty());

        type_is<string_component>(u.fragment());
        BOOST_CHECK(u.fragment().empty());
    }

    BOOST_AUTO_TEST_CASE(convert_to_string_view)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<string_component>(u.scheme());
        byte_sequence_view_t const scheme = u.scheme();
        BOOST_CHECK_EQUAL(scheme, "http");

        type_is<string_component>(u.username());
        byte_sequence_view_t const username = u.username();
        BOOST_CHECK_EQUAL(username, "user");

        type_is<string_component>(u.password());
        byte_sequence_view_t const password = u.password();
        BOOST_CHECK_EQUAL(password, "pass");

        type_is<string_component>(u.query());
        byte_sequence_view_t const query = u.query();
        BOOST_CHECK_EQUAL(query, "query");

        type_is<string_component>(u.fragment());
        byte_sequence_view_t const fragment = u.fragment();
        BOOST_CHECK_EQUAL(fragment, "fragment");
    }

    BOOST_AUTO_TEST_CASE(stream_operator)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        std::ostringstream oss;
        oss << u.scheme();
        BOOST_CHECK_EQUAL(oss.str(), "http");
    }

BOOST_AUTO_TEST_SUITE_END() // string_component

} // namespace whatwg::url::slim
