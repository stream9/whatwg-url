#include "utility.hpp"

#include <sstream>
#include <type_traits>

#include <boost/test/unit_test.hpp>

#include <whatwg/url/slim/host_component.hpp>

namespace whatwg::url::slim {

template<typename T1, typename T2>
void type_is(T2 const&)
{
    static_assert(
        std::is_same_v<
            T1,
            std::decay_t<T2>
        >);
}

BOOST_AUTO_TEST_SUITE(const_host_component_)

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "", 1234,
            "/path", "query", "fragment",
            "http://user:pass@:1234/path?query#fragment"
        );

        type_is<const_host_component>(u.host());
        const_host_component host { u.host() };
        BOOST_CHECK_EQUAL(host, u.host());
    }

    BOOST_AUTO_TEST_CASE(conversion_from_host_component)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "", 1234,
            "/path", "query", "fragment",
            "http://user:pass@:1234/path?query#fragment"
        );

        type_is<host_component>(u.host());
        const_host_component host { u.host() };
        BOOST_CHECK_EQUAL(host, u.host());
    }

    BOOST_AUTO_TEST_CASE(is_empty_1)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "", 1234,
            "/path", "query", "fragment",
            "http://user:pass@:1234/path?query#fragment"
        );

        type_is<const_host_component>(u.host());
        BOOST_CHECK(u.host().is_empty());
    }

    BOOST_AUTO_TEST_CASE(is_empty_2)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_host_component>(u.host());
        BOOST_CHECK(!u.host().is_empty());
    }

    BOOST_AUTO_TEST_CASE(is_null_1)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", {}, 1234,
            "/path", "query", "fragment",
            "http://user:pass@:1234/path?query#fragment"
        );

        type_is<const_host_component>(u.host());
        BOOST_CHECK(u.host().is_null());
    }

    BOOST_AUTO_TEST_CASE(is_null_2)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_host_component>(u.host());
        BOOST_CHECK(!u.host().is_null());
    }

    BOOST_AUTO_TEST_CASE(conversion_to_bool_1)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_host_component>(u.host());
        BOOST_CHECK(u.host());
    }

    BOOST_AUTO_TEST_CASE(conversion_to_bool_2)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", {}, 1234,
            "/path", "query", "fragment",
            "http://user:pass@:1234/path?query#fragment"
        );

        type_is<const_host_component>(u.host());
        BOOST_CHECK(!u.host());
    }

    BOOST_AUTO_TEST_CASE(conversion_to_string_view)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_host_component>(u.host());
        byte_sequence_view_t const host = u.host();
        BOOST_CHECK_EQUAL(host, "host");
    }

BOOST_AUTO_TEST_SUITE_END() // const_host_component_

BOOST_AUTO_TEST_SUITE(host_component_)

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<host_component>(u.host());
        host_component host { u.host() };
        BOOST_CHECK_EQUAL(host, u.host());
    }

    BOOST_AUTO_TEST_CASE(copy_assignment)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host1", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host1:1234/path?query#fragment"
        );
        type_is<host_component>(u1.host());

        auto u2 = construct_and_verify(
            "http", "user", "pass", "host2", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host2:1234/path?query#fragment"
        );
        type_is<host_component>(u2.host());

        u1.host() = u2.host();
        BOOST_CHECK_EQUAL(u1.host(), u2.host());
    }

    BOOST_AUTO_TEST_CASE(assign_from_const_host_component)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host1", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host1:1234/path?query#fragment"
        );
        type_is<host_component>(u1.host());

        auto const u2 = construct_and_verify(
            "http", "user", "pass", "host2", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host2:1234/path?query#fragment"
        );
        type_is<const_host_component>(u2.host());

        u1.host() = u2.host();
        BOOST_CHECK_EQUAL(u1.host(), u2.host());
    }

    BOOST_AUTO_TEST_CASE(assignment_from_host_t)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        auto const host = parse_host(U"www.google.com");
        BOOST_REQUIRE(host);

        u.host() = *host;

        BOOST_CHECK_EQUAL(u,
            "http://user:pass@www.google.com:1234/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(is_empty)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "", 1234,
            "/path", "query", "fragment",
            "http://user:pass@:1234/path?query#fragment"
        );

        BOOST_CHECK(u.host().is_empty());
    }

    BOOST_AUTO_TEST_CASE(is_null)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", std::nullopt, 1234,
            "/path", "query", "fragment",
            "http://user:pass@:1234/path?query#fragment"
        );

        BOOST_CHECK(u.host().is_null());
    }

    BOOST_AUTO_TEST_CASE(set_empty_string)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        u.host().set_empty_string();
        BOOST_CHECK(u.host());
        BOOST_CHECK(u.host().is_empty());
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@:1234/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(stream_operator)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        std::ostringstream oss;
        oss << u.host();

        BOOST_CHECK_EQUAL(oss.str(), "host");
    }

BOOST_AUTO_TEST_SUITE_END() // host_component_

} // namespace whatwg::url::slim
