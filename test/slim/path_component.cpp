#include "utility.hpp"

#include <type_traits>
#include <sstream>

#include <boost/test/unit_test.hpp>

#include <whatwg/url/slim/path_component.hpp>

namespace whatwg::url::slim {

template<typename T1, typename T2>
void type_is(T2 const&)
{
    static_assert(
        std::is_same_v<
            T1,
            std::decay_t<T2>
        >);
}

BOOST_AUTO_TEST_SUITE(const_path_component_)

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_path_component>(u.path());
        const_path_component path { u.path() };
        BOOST_CHECK_EQUAL(path, u.path());
    }

    BOOST_AUTO_TEST_CASE(conversion_from_path_component)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<path_component>(u.path());
        const_path_component path { u.path() };
        BOOST_CHECK_EQUAL(path, u.path());
    }

    BOOST_AUTO_TEST_CASE(size_empty)
    {
        url_record u;
        auto const& const_u = u;

        auto const path = const_u.path();
        type_is<const_path_component>(path);

        BOOST_CHECK_EQUAL(path.size(), 0);
    }

    BOOST_AUTO_TEST_CASE(size_hierarchical)
    {
        url_record u;
        auto const& const_u = u;

        auto const path = const_u.path();
        type_is<const_path_component>(path);

        BOOST_CHECK(path.is_hierarchical());

        u.path() = "/";
        BOOST_CHECK_EQUAL(path.size(), 1);

        u.path() = "/for";
        BOOST_CHECK_EQUAL(path.size(), 1);
    }

    BOOST_AUTO_TEST_CASE(size_not_hierarchical)
    {
        url_record u;
        auto const& const_u = u;

        auto const path = const_u.path();
        type_is<const_path_component>(path);

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!path.is_hierarchical());

        u.path() = "foo";
        BOOST_CHECK_EQUAL(path.size(), 1);

        u.path() = "foo/bar";
        BOOST_CHECK_EQUAL(path.size(), 1);
    }

    BOOST_AUTO_TEST_CASE(size_two)
    {
        url_record u;
        auto const& const_u = u;

        auto const path = const_u.path();
        type_is<const_path_component>(path);

        u.path() = "/foo/bar";
        BOOST_CHECK_EQUAL(path.size(), 2);

        u.path() = "/foo/";
        BOOST_CHECK_EQUAL(path.size(), 2);

        u.path() = "//";
        BOOST_CHECK_EQUAL(path.size(), 2);
    }

    BOOST_AUTO_TEST_CASE(empty_hierarchical)
    {
        url_record u;
        auto const& const_u = u;

        auto const path = const_u.path();
        type_is<const_path_component>(path);

        BOOST_CHECK(path.empty());

        u.path() = "/foo";
        BOOST_CHECK(!path.empty());

        u.path() = "";
        BOOST_CHECK(path.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_not_hierarchical)
    {
        url_record u;
        auto const& const_u = u;

        auto const path = const_u.path();
        type_is<const_path_component>(path);

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!path.is_hierarchical());
        BOOST_CHECK(!path.empty());
    }

    BOOST_AUTO_TEST_CASE(is_hierarchical)
    {
        url_record u;
        auto const& const_u = u;

        auto const path = const_u.path();
        type_is<const_path_component>(path);

        BOOST_CHECK(!u.cannot_be_a_base_url());
        BOOST_CHECK(path.is_hierarchical());

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!path.is_hierarchical());

        //TODO what shuold happen to path after cannot_be_a_base_url changed
    }

BOOST_AUTO_TEST_SUITE_END() // const_path_component_

BOOST_AUTO_TEST_SUITE(path_component_)

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<path_component>(u.path());
        path_component path { u.path() };
        BOOST_CHECK_EQUAL(path, u.path());
    }

    BOOST_AUTO_TEST_CASE(copy_assignment)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path1", "query", "fragment",
            "http://user:pass@host:1234/path1?query#fragment"
        );
        type_is<path_component>(u1.path());

        auto u2 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path2", "query", "fragment",
            "http://user:pass@host:1234/path2?query#fragment"
        );
        type_is<path_component>(u2.path());

        u1.path() = u2.path();
        BOOST_CHECK_EQUAL(u1.path(), u2.path());
    }

    BOOST_AUTO_TEST_CASE(assign_from_const_path_component)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path1", "query", "fragment",
            "http://user:pass@host:1234/path1?query#fragment"
        );
        type_is<path_component>(u1.path());

        auto const u2 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path2", "query", "fragment",
            "http://user:pass@host:1234/path2?query#fragment"
        );
        type_is<const_path_component>(u2.path());

        u1.path() = u2.path();
        BOOST_CHECK_EQUAL(u1.path(), u2.path());
    }

    BOOST_AUTO_TEST_CASE(size_empty)
    {
        url_record u;
        path_component p { u };

        BOOST_CHECK_EQUAL(p.size(), 0);
    }

    BOOST_AUTO_TEST_CASE(size_hierarchical)
    {
        url_record u;
        path_component p { u };

        BOOST_CHECK(p.is_hierarchical());

        u.path() = "/foo";
        BOOST_CHECK_EQUAL(p.size(), 1);

        u.path() = "/";
        BOOST_CHECK_EQUAL(p.size(), 1);
    }

    BOOST_AUTO_TEST_CASE(size_not_hierarchical)
    {
        url_record u;
        path_component p { u };

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!p.is_hierarchical());

        u.path() = "foo";
        BOOST_CHECK_EQUAL(p.size(), 1);

        u.path() = "foo/bar";
        BOOST_CHECK_EQUAL(p.size(), 1);
    }

    BOOST_AUTO_TEST_CASE(size_two)
    {
        url_record u;
        path_component p { u };

        u.path() = "/foo/bar";
        BOOST_CHECK_EQUAL(p.size(), 2);

        u.path() = "/foo/";
        BOOST_CHECK_EQUAL(p.size(), 2);

        u.path() = "//";
        BOOST_CHECK_EQUAL(p.size(), 2);
    }

    BOOST_AUTO_TEST_CASE(empty_hierarchical)
    {
        url_record u;
        path_component p { u };

        BOOST_CHECK(p.empty());

        u.path() = "/foo";
        BOOST_CHECK(!p.empty());

        u.path() = "";
        BOOST_CHECK(p.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_not_hierarchical)
    {
        url_record u;
        path_component p { u };

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!p.is_hierarchical());
        BOOST_CHECK(!p.empty());
    }

    BOOST_AUTO_TEST_CASE(is_hierarchical)
    {
        url_record u;
        path_component p { u };

        BOOST_CHECK(!u.cannot_be_a_base_url());
        BOOST_CHECK(p.is_hierarchical());

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!p.is_hierarchical());

        //TODO what shuold happen to path after cannot_be_a_base_url change
    }

    BOOST_AUTO_TEST_CASE(pop_front_hierarchical)
    {
        url_record u;
        path_component p { u };

        BOOST_CHECK(p.is_hierarchical());

        u.path() = "/foo/bar";
        p.pop_front();
        BOOST_CHECK_EQUAL(u.path(), "/bar");

        u.path() = "/foo";
        p.pop_front();
        BOOST_CHECK_EQUAL(u.path(), "");

        u.path() = "";
        p.pop_front();
        BOOST_CHECK_EQUAL(u.path(), "");
    }

    BOOST_AUTO_TEST_CASE(pop_front_not_hierarchical)
    {
        url_record u;
        path_component p { u };

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!p.is_hierarchical());

        u.path() = "non_hierachical";
        p.pop_front();
        BOOST_CHECK_EQUAL(u.path(), "");

        u.path() = "abc/def";
        p.pop_front();
        BOOST_CHECK_EQUAL(u.path(), "");
    }

    BOOST_AUTO_TEST_CASE(pop_back_hierarchical)
    {
        url_record u;
        path_component p { u };

        BOOST_CHECK(p.is_hierarchical());

        u.path() = "/foo/bar";
        p.pop_back();
        BOOST_CHECK_EQUAL(u.path(), "/foo");

        u.path() = "/foo";
        p.pop_back();
        BOOST_CHECK_EQUAL(u.path(), "");

        u.path() = "";
        p.pop_back();
        BOOST_CHECK_EQUAL(u.path(), "");
    }

    BOOST_AUTO_TEST_CASE(pop_back_not_hierarchical)
    {
        url_record u;
        path_component p { u };

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!p.is_hierarchical());

        u.path() = "non_hierachical";
        p.pop_back();
        BOOST_CHECK_EQUAL(u.path(), "");

        u.path() = "abc/def";
        p.pop_back();
        BOOST_CHECK_EQUAL(u.path(), "");
    }

    BOOST_AUTO_TEST_CASE(append_hierarchical)
    {
        url_record u;
        path_component p { u };

        BOOST_CHECK(p.is_hierarchical());

        p.append("foo");
        BOOST_CHECK_EQUAL(p.size(), 1);
        BOOST_CHECK_EQUAL(u.path(), "/foo");

        p.append("bar");
        BOOST_CHECK_EQUAL(p.size(), 2);
        BOOST_CHECK_EQUAL(u.path(), "/foo/bar");
    }

    BOOST_AUTO_TEST_CASE(append_not_hierarchical)
    {
        url_record u;
        path_component p { u };

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!p.is_hierarchical());

        u.path() = "abc";
        BOOST_CHECK_EQUAL(p.size(), 1);
        BOOST_CHECK_EQUAL(u.path(), "abc");

        p.append("def");
        BOOST_CHECK_EQUAL(p.size(), 1);
        BOOST_CHECK_EQUAL(u.path(), "abcdef");

        p.append("/ghi");
        BOOST_CHECK_EQUAL(p.size(), 1);
        BOOST_CHECK_EQUAL(u.path(), "abcdef/ghi");
    }

    BOOST_AUTO_TEST_CASE(subscript_hierarchical)
    {
        url_record u;
        path_component p { u };

        BOOST_CHECK(p.is_hierarchical());

        u.path() = "/path1/path2/path3";
        BOOST_CHECK_EQUAL(p[0], "path1");
        BOOST_CHECK_EQUAL(p[1], "path2");
        BOOST_CHECK_EQUAL(p[2], "path3");
    }

    BOOST_AUTO_TEST_CASE(subscript_not_hierarchical)
    {
        url_record u;
        path_component p { u };

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(!p.is_hierarchical());

        u.path() = "foo/bar";
        BOOST_CHECK_EQUAL(p[0], "foo/bar");
    }

    BOOST_AUTO_TEST_CASE(stream_operator)
    {
        url_record u;
        u.path() = "/foo/bar";

        std::ostringstream oss;
        oss << u.path();

        BOOST_CHECK_EQUAL(oss.str(), "/foo/bar");
    }

BOOST_AUTO_TEST_SUITE_END() // path_component_

} // namespace whatwg::url::slim
