#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/character_encoding.hpp>
#include <whatwg/url/misc.hpp>
#include <whatwg/url/slim/string_component.hpp>
#include <whatwg/url/slim/host_component.hpp>
#include <whatwg/url/slim/port_component.hpp>
#include <whatwg/url/slim/path_component.hpp>

namespace whatwg::url::slim {

url_record
construct_and_verify(std::optional<byte_sequence_view_t> const scheme,
                     std::optional<byte_sequence_view_t> const username,
                     std::optional<byte_sequence_view_t> const password,
                     std::optional<byte_sequence_view_t> const host,
                     std::optional<uint16_t> const port,
                     std::optional<byte_sequence_view_t> const path,
                     std::optional<byte_sequence_view_t> const query,
                     std::optional<byte_sequence_view_t> const fragment,
                     byte_sequence_view_t const expected
                    )
{
    url_record u;
    test_harness h { u };
    auto const& segments = h.segments();
    std::string spec;
    int32_t next_idx = 0;

    // scheme
    if (scheme) {
        assign(u.scheme(), *scheme);

        auto const& s = segments[0];

        BOOST_CHECK_EQUAL(u.scheme(), *scheme);
        BOOST_CHECK_EQUAL(s.index, next_idx);
        BOOST_CHECK_EQUAL(s.length, scheme->size());

        spec += *scheme;
        BOOST_CHECK_EQUAL(h.spec(), spec);

        next_idx = s.index + s.length;
    }

    // username
    if (username) {
        assign(u.username(), *username);

        auto const& s = segments[1];

        BOOST_CHECK_EQUAL(u.username(), *username);
        BOOST_CHECK_EQUAL(s.index, next_idx);
        BOOST_CHECK_EQUAL(s.length, username->size());

        spec += *username;
        BOOST_CHECK_EQUAL(h.spec(), spec);

        next_idx = s.index + s.length;
    }

    // password
    if (password) {
        assign(u.password(), *password);

        auto const& s = segments[2];

        BOOST_CHECK_EQUAL(u.password(), *password);
        BOOST_CHECK_EQUAL(s.index, next_idx);
        BOOST_CHECK_EQUAL(s.length, password->size());

        spec += *password;
        BOOST_CHECK_EQUAL(h.spec(), spec);

        next_idx = s.index + s.length;
    }

    // host
    if (host) {
        auto const& host_ = parse_host(
            decode_utf8_without_bom(*host),
            host->empty()
        );
        BOOST_REQUIRE(host_);

        assign(u.host(), *host_);

        auto const& s = segments[3];

        BOOST_CHECK_EQUAL(u.host(), *host);
        BOOST_CHECK_EQUAL(s.index, next_idx);
        BOOST_CHECK_EQUAL(s.length, host->size());

        spec += *host;
        BOOST_CHECK_EQUAL(h.spec(), spec);

        next_idx = s.index + s.length;
    }

    // port
    if (port) {
        u.port() = *port;

        auto const& s = segments[4];

        BOOST_CHECK_EQUAL(*u.port(), *port);
        BOOST_CHECK_EQUAL(s.index, next_idx);

        auto const serialized_port = serialize_integer(*port);
        BOOST_CHECK_EQUAL(s.length, serialized_port.size());

        spec += serialized_port;
        BOOST_CHECK_EQUAL(h.spec(), spec);

        next_idx = s.index + s.length;
    }

    // path
    if (path) {
        assign(u.path(), *path);

        auto const& s = segments[5];

        BOOST_CHECK_EQUAL(u.path(), *path);
        BOOST_CHECK_EQUAL(s.index, next_idx);
        BOOST_CHECK_EQUAL(s.length, path->size());

        spec += *path;
        BOOST_CHECK_EQUAL(h.spec(), spec);

        next_idx = s.index + s.length;
    }

    // query
    if (query) {
        assign(u.query(), *query);

        auto const& s = segments[6];

        BOOST_CHECK_EQUAL(u.query(), *query);
        BOOST_CHECK_EQUAL(s.index, next_idx);
        BOOST_CHECK_EQUAL(s.length, query->size());

        spec += *query;
        BOOST_CHECK_EQUAL(h.spec(), spec);

        next_idx = s.index + s.length;
    }

    // fragment
    if (fragment) {
        assign(u.fragment(), *fragment);

        auto const& s = segments[7];

        BOOST_CHECK_EQUAL(u.fragment(), *fragment);
        BOOST_CHECK_EQUAL(s.index, next_idx);
        BOOST_CHECK_EQUAL(s.length, fragment->size());

        spec += *fragment;
        BOOST_CHECK_EQUAL(h.spec(), spec);
    }

    std::string_view sv = u;
    BOOST_CHECK_EQUAL(h.spec(), expected);

    return u;
}


} // namespace whatwg::url::slim
