#include "utility.hpp"

#include <type_traits>
#include <sstream>

#include <boost/test/unit_test.hpp>

#include <whatwg/url/slim/port_component.hpp>

namespace whatwg::url::slim {

template<typename T1, typename T2>
void type_is(T2 const&)
{
    static_assert(
        std::is_same_v<
            T1,
            std::decay_t<T2>
        >);
}

BOOST_AUTO_TEST_SUITE(const_port_component_)

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_port_component>(u.port());
        const_port_component port { u.port() };
        BOOST_CHECK_EQUAL(port, u.port());
    }

    BOOST_AUTO_TEST_CASE(conversion_from_port_component)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<port_component>(u.port());
        const_port_component port { u.port() };
        BOOST_CHECK_EQUAL(port, u.port());
    }

    BOOST_AUTO_TEST_CASE(convert_to_bool_1)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", std::nullopt,
            "/path", "query", "fragment",
            "http://user:pass@host/path?query#fragment"
        );

        type_is<const_port_component>(u.port());
        BOOST_CHECK(!u.port());
    }

    BOOST_AUTO_TEST_CASE(convert_to_bool_2)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_port_component>(u.port());
        BOOST_CHECK(u.port());
    }

    BOOST_AUTO_TEST_CASE(dereference)
    {
        auto const u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<const_port_component>(u.port());
        BOOST_CHECK_EQUAL(*u.port(), 1234);
    }

BOOST_AUTO_TEST_SUITE_END() // const_port_component_

BOOST_AUTO_TEST_SUITE(port_component_)

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<port_component>(u.port());
        port_component port { u.port() };
        BOOST_CHECK_EQUAL(port, u.port());
    }

    BOOST_AUTO_TEST_CASE(copy_assignment)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        type_is<port_component>(u1.port());

        auto u2 = construct_and_verify(
            "http", "user", "pass", "host", 5678,
            "/path", "query", "fragment",
            "http://user:pass@host:5678/path?query#fragment"
        );
        type_is<port_component>(u2.port());

        u1.port() = u2.port();
        BOOST_CHECK_EQUAL(u1.port(), 5678);
    }

    BOOST_AUTO_TEST_CASE(copy_assignment_from_null)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
        type_is<port_component>(u1.port());

        auto u2 = construct_and_verify(
            "http", "user", "pass", "host", {},
            "/path", "query", "fragment",
            "http://user:pass@host/path?query#fragment"
        );
        type_is<port_component>(u2.port());

        u1.port() = u2.port();
        BOOST_CHECK_EQUAL(u1.port(), u2.port());
    }

    BOOST_AUTO_TEST_CASE(copy_assignment_to_null)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host", {},
            "/path", "query", "fragment",
            "http://user:pass@host/path?query#fragment"
        );
        type_is<port_component>(u1.port());

        auto u2 = construct_and_verify(
            "http", "user", "pass", "host", 5678,
            "/path", "query", "fragment",
            "http://user:pass@host:5678/path?query#fragment"
        );
        type_is<port_component>(u2.port());

        u1.port() = u2.port();
        BOOST_CHECK_EQUAL(u1.port(), u2.port());
    }

    BOOST_AUTO_TEST_CASE(assign_from_int)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        u.port() = 0;
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:0/path?query#fragment");

        u.port() = UINT16_MAX;
        BOOST_CHECK_EQUAL(u,
            "http://user:pass@host:65535/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(convert_to_bool)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", std::nullopt,
            "/path", "query", "fragment",
            "http://user:pass@host/path?query#fragment"
        );

        BOOST_CHECK(!u.port());

        u.port() = 1234;

        BOOST_CHECK(u.port());
    }

    BOOST_AUTO_TEST_CASE(convert_to_const_port_component)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        type_is<port_component>(u.port());
        const_port_component port { u.port() };

        BOOST_CHECK_EQUAL(port, u.port());
    }

    BOOST_AUTO_TEST_CASE(dereference)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        BOOST_CHECK_EQUAL(*u.port(), 1234);
    }

    BOOST_AUTO_TEST_CASE(reset)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        BOOST_CHECK(u.port());

        u.port().reset();

        BOOST_CHECK(!u.port());
    }

    BOOST_AUTO_TEST_CASE(stream_operator)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        BOOST_CHECK(u.port());

        std::ostringstream oss;
        oss << u.port();
        BOOST_CHECK_EQUAL(oss.str(), "1234");

        u.port().reset();
        oss.str("");
        oss << u.port();
        BOOST_CHECK_EQUAL(oss.str(), "null");
    }

    BOOST_AUTO_TEST_CASE(equal_operator)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        auto u2 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        BOOST_CHECK_EQUAL(u1.port(), u2.port());

        auto u3 = construct_and_verify(
            "http", "user", "pass", "host", 2345,
            "/path", "query", "fragment",
            "http://user:pass@host:2345/path?query#fragment"
        );

        BOOST_CHECK_NE(u1.port(), u3.port());

        auto u4 = construct_and_verify(
            "http", "user", "pass", "host", {},
            "/path", "query", "fragment",
            "http://user:pass@host/path?query#fragment"
        );

        BOOST_CHECK_NE(u1.port(), u4.port());

        auto u5 = construct_and_verify(
            "http", "user", "pass", "host", {},
            "/path", "query", "fragment",
            "http://user:pass@host/path?query#fragment"
        );

        BOOST_CHECK_EQUAL(u4.port(), u5.port());
    }

    BOOST_AUTO_TEST_CASE(equal_to_int_operator)
    {
        auto u1 = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        BOOST_CHECK_EQUAL(u1.port(), 1234);
        BOOST_CHECK_NE(u1.port(), 2345);

        auto u2 = construct_and_verify(
            "http", "user", "pass", "host", {},
            "/path", "query", "fragment",
            "http://user:pass@host/path?query#fragment"
        );

        BOOST_CHECK_NE(u2.port(), 1234);
    }

BOOST_AUTO_TEST_SUITE_END() // port_component_

} // namespace whatwg::url::slim
