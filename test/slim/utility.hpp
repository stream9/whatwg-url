#ifndef URL_TEST_SLIM_UTILITY_HPP
#define URL_TEST_SLIM_UTILITY_HPP

#include <whatwg/url/slim/url_record.hpp>

#include <iostream>

namespace whatwg::url::slim {

class test_harness
{
public:
    test_harness(url_record const& r)
        : m_url_record { r }
    {}

    auto const& spec() const { return m_url_record.m_spec; }
    auto const& segments() const { return m_url_record.m_segments; }

    void dump_segments()
    {
        auto const& segments = this->segments();

        for (auto const& s: segments) {
            std::cout << "{" << s.index
                      << ", " << s.length
                      << " }, ";
        }
        std::cout << std::endl;
    }

private:
    url_record const& m_url_record;
};

url_record
construct_and_verify(std::optional<byte_sequence_view_t> scheme,
                     std::optional<byte_sequence_view_t> username,
                     std::optional<byte_sequence_view_t> password,
                     std::optional<byte_sequence_view_t> host,
                     std::optional<uint16_t>            port,
                     std::optional<byte_sequence_view_t> path,
                     std::optional<byte_sequence_view_t> query,
                     std::optional<byte_sequence_view_t> fragment,
                     byte_sequence_view_t                expected
                    );

} // namespace whatwg::url::slim

#endif // URL_TEST_SLIM_UTILITY_HPP
