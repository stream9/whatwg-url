#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/slim/string_component.hpp>

namespace whatwg::url::slim {

BOOST_AUTO_TEST_SUITE(url_record2_)

    BOOST_AUTO_TEST_CASE(initial_state)
    {
        url_record u;
        test_harness h { u };
        auto const& segments = h.segments();

        BOOST_CHECK(h.spec().empty());

        BOOST_CHECK(segments[segment_id::scheme].index == 0);
        BOOST_CHECK(segments[segment_id::scheme].length == 0);

        BOOST_CHECK(segments[segment_id::username].index == 0);
        BOOST_CHECK(segments[segment_id::username].length == 0);

        BOOST_CHECK(segments[segment_id::password].index == 0);
        BOOST_CHECK(segments[segment_id::password].length == 0);

        BOOST_CHECK(segments[segment_id::host].index == 0);
        BOOST_CHECK(segments[segment_id::host].length == -1);

        BOOST_CHECK(segments[segment_id::port].index == 0);
        BOOST_CHECK(segments[segment_id::port].length == -1);

        BOOST_CHECK(segments[segment_id::path].index == 0);
        BOOST_CHECK(segments[segment_id::path].length == -1);

        BOOST_CHECK(segments[segment_id::query].index == 0);
        BOOST_CHECK(segments[segment_id::query].length == -1);

        BOOST_CHECK(segments[segment_id::fragment].index == 0);
        BOOST_CHECK(segments[segment_id::fragment].length == -1);
    }

    BOOST_AUTO_TEST_CASE(elementary)
    {
        construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );
    }

    BOOST_AUTO_TEST_CASE(flag_proxy_)
    {
        url_record u;

        BOOST_CHECK(!u.cannot_be_a_base_url());

        u.cannot_be_a_base_url() = true;
        BOOST_CHECK(u.cannot_be_a_base_url());

        u.cannot_be_a_base_url() = false;
        BOOST_CHECK(!u.cannot_be_a_base_url());
    }

    BOOST_AUTO_TEST_CASE(serialize_omit_fragment_1)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        BOOST_CHECK_EQUAL(
            serialize(u, true), "http://user:pass@host:1234/path?query");
    }

    BOOST_AUTO_TEST_CASE(serialize_omit_fragment_2)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", {},
            "http://user:pass@host:1234/path?query"
        );

        BOOST_CHECK_EQUAL(
            serialize(u, true), "http://user:pass@host:1234/path?query");
    }

    BOOST_AUTO_TEST_CASE(stream_operator)
    {
        auto u = construct_and_verify(
            "http", "user", "pass", "host", 1234,
            "/path", "query", "fragment",
            "http://user:pass@host:1234/path?query#fragment"
        );

        std::ostringstream oss;
        oss << u;
        BOOST_CHECK_EQUAL(oss.str(), static_cast<byte_sequence_view_t>(u));
    }

BOOST_AUTO_TEST_SUITE_END() // url_record2_

} // namespace whatwg::url::slim

