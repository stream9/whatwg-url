#include <whatwg/url/result.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(result_)

    BOOST_AUTO_TEST_CASE(constructor)
    {
        url::result<int> const a { 100 };

        BOOST_CHECK(!a.error());
        BOOST_CHECK(a.has_value());
        BOOST_CHECK(a);
        BOOST_CHECK_EQUAL(a.value(), 100);
        BOOST_CHECK_EQUAL(a.value_or(200), 100);
    }

BOOST_AUTO_TEST_SUITE_END() // result_

} // namespace whatwg::url::testing
