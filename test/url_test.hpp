#ifndef URL_TEST_UTILITY_HPP
#define URL_TEST_UTILITY_HPP

#include <filesystem>
#include <string>

std::string slurp_file(std::filesystem::path const&);

#endif // URL_TEST_UTILITY_HPP
