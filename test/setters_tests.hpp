#ifndef URL_TEST_SETTERS_TESTS_HPP
#define URL_TEST_SETTERS_TESTS_HPP

#include <boost/test/unit_test.hpp>

namespace setters_tests {

bool init_test_suite(boost::unit_test::test_suite& parent);

} // namespace setters_tests

#endif // URL_TEST_SETTERS_TESTS_HPP
