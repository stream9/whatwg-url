#include <whatwg/url/basic_url_parser.hpp>
#include <whatwg/url/url_equivalence.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(url_equivalence_)

    BOOST_AUTO_TEST_CASE(equals_)
    {
        auto const u1 = basic_url_parse("http://www.google.com");
        BOOST_REQUIRE(u1);

        auto const u2 = basic_url_parse("http://www.google.com");
        BOOST_REQUIRE(u2);

        BOOST_CHECK(equals(*u1, *u2));

        auto const u3 = basic_url_parse("http://www.facebook.com");
        BOOST_REQUIRE(u3);

        BOOST_CHECK(!equals(*u1, *u3));
    }

    BOOST_AUTO_TEST_CASE(equals_omit_fragment)
    {
        auto const u1 = basic_url_parse("http://www.google.com");
        BOOST_REQUIRE(u1);

        auto const u2 = basic_url_parse("http://www.google.com#foo");
        BOOST_REQUIRE(u2);

        BOOST_CHECK(!equals(*u1, *u2, false));
        BOOST_CHECK(equals(*u1, *u2, true));
    }

BOOST_AUTO_TEST_SUITE_END() // url_equivalence_

} // namespace whatwg::url::testing
