#include <boost/test/unit_test.hpp>

#include <whatwg/url/string.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(string_)

    BOOST_AUTO_TEST_CASE(end_with_string)
    {
        byte_sequence_t s = "12345";

        BOOST_CHECK(ends_with(s, "45"));
    }

BOOST_AUTO_TEST_SUITE_END() // string_

} // namespace whatwg::url::testing
