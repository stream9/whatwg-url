#include "toascii.hpp"

#include <filesystem>
#include <iostream>

#include <json/json.hpp>
#include <json/array.hpp>
#include <json/value.hpp>
#include <json/object.hpp>

#include "url_test.hpp"

#include <whatwg/url/character_encoding.hpp>
#include <whatwg/url/idna.hpp>
#include <whatwg/url/string.hpp>

namespace toascii_test {

namespace tst = boost::unit_test;
namespace fs = std::filesystem;
namespace url = whatwg::url;

static void
test_toascii(json::object const& obj)
{
    auto const& input = obj.at("input").get_string();
    auto const& expected = obj.at("output");

    auto const& u32input = url::decode_utf8_without_bom(input);

    auto const& output = url::domain_to_ascii(u32input);
#if 0
    using namespace url;
    std::cout << "input: " << input
              << ", expected: " << expected
              << ", u32input: " << u32input
              //<< ", output: " << *output
              << "\n";
#endif

    if (expected.is_string()) {
        BOOST_REQUIRE(!!output);
        BOOST_CHECK_EQUAL(*output, expected.get_string());
    }
    else if (expected.is_null()) {
        BOOST_CHECK(!output);
    }
    else {
        assert(false);
    }
}

bool
init_test_suite(tst::test_suite& parent)
{
    fs::path dir { DATA_DIRECTORY };
    if (!exists(dir)) {
        std::cerr << "data directory doesn't exists\n";
        return false;
    }

    auto const& path = dir / "toascii.json";
    if (!exists(dir)) {
        return false;
    }

    auto* const suite = BOOST_TEST_SUITE(path.stem().c_str());
    assert(suite);

    auto const& test_data = slurp_file(path);

    auto const& array = json::parse(test_data).get_array();

    auto n = 0;
    for (auto const& v: array) {
        if (!v.is_object()) continue;
        auto const& obj = v.get_object();

        auto const label = "no." + std::to_string(++n);

        auto case_ = new tst::test_case(
            label.data(),
            __FILE__, __LINE__,
            [obj]() {
                test_toascii(obj);
            }
        );

        suite->add(case_);
    }

    parent.add(suite);

    return true;
}

} // namespace toascii_test
