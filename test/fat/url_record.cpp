#include <iostream>

#include <boost/test/unit_test.hpp>

#include <whatwg/url/basic_url_parser.hpp>
#include <whatwg/url/url_serializing.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(url_record_)

BOOST_AUTO_TEST_SUITE(parser_)

    BOOST_AUTO_TEST_CASE(elementary)
    {
        basic_url_parser parser;
        parser.set_validation_error_handler(
            [](auto const& err, auto const/*input*/, auto const index) {
                std::cerr << "validation error: "
                          << err.message()
                          << " at index " << index
                          << "\n";
            });

        auto const& url = parser(
            "http://user:pass@www.google.com:8080/dir/file.txt?query#fragment");

        BOOST_REQUIRE(url);
        BOOST_CHECK(url->scheme() == "http");

        BOOST_CHECK(url->username() == "user");
        BOOST_CHECK(url->password() == "pass");

        auto const& host = url->host();
        BOOST_REQUIRE(host.is_domain());
        BOOST_CHECK(host.domain() == "www.google.com");

        BOOST_REQUIRE(url->port());
        BOOST_CHECK(*url->port() == 8080);

        BOOST_REQUIRE(url->path().size() == 2);
        BOOST_CHECK(url->path()[0] == "dir");
        BOOST_CHECK(url->path()[1] == "file.txt");

        BOOST_REQUIRE(url->query());
        BOOST_CHECK(*url->query() == "query");

        BOOST_REQUIRE(url->fragment());
        BOOST_CHECK(*url->fragment() == "fragment");
    }

BOOST_AUTO_TEST_SUITE_END() // parser_

BOOST_AUTO_TEST_SUITE(serializer_)

    BOOST_AUTO_TEST_SUITE(serializer)

        BOOST_AUTO_TEST_CASE(url_record_)
        {
            auto const u = basic_url_parse("http://www.google.com");
            BOOST_REQUIRE(u);

            BOOST_CHECK_EQUAL(serialize_url(*u), "http://www.google.com/");
        }

    BOOST_AUTO_TEST_SUITE_END() // serializer

BOOST_AUTO_TEST_SUITE_END() // serializer_

BOOST_AUTO_TEST_SUITE_END() // url_record_

} // namespace whatwg::url::testing
