#include <whatwg/url/basic_url_parser.hpp>
#include <whatwg/url/error.hpp>

#include <algorithm>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(basic_url_parser_)

    BOOST_AUTO_TEST_CASE(empty_host_with_userinfo)
    {
        basic_url_parser parse;

        auto u1 = parse("scheme://user@host/path");
        BOOST_REQUIRE(u1);

        std::vector<std::error_code> errors;
        parse.set_validation_error_handler(
            [&](auto const& ec, auto const input, auto const index) {
                errors.push_back(ec);
            });

        parse("", *u1, basic_url_parser::state::hostname);

        BOOST_CHECK(std::any_of(
            errors.begin(), errors.end(),
            [](auto const& ec) {
                ec.message(); // for coverage only
                return ec == errc::empty_host_with_user_info_or_port;
            }));
    }

    BOOST_AUTO_TEST_CASE(empty_host_with_port)
    {
        basic_url_parser parse;

        auto u1 = parse("scheme://host:1234/path");
        BOOST_REQUIRE(u1);

        std::vector<std::error_code> errors;
        parse.set_validation_error_handler(
            [&](auto const& ec, auto const input, auto const index) {
                errors.push_back(ec);
            });

        parse("", *u1, basic_url_parser::state::hostname);

        BOOST_CHECK(std::any_of(
            errors.begin(), errors.end(),
            [](auto const& ec) {
                ec.message(); // for coverage only
                return ec == errc::empty_host_with_user_info_or_port;
            }));
    }

BOOST_AUTO_TEST_SUITE_END() // basic_url_parser

} // namespace whatwg::url::testing
