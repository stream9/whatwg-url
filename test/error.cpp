#include <whatwg/url/error.hpp>

#include <system_error>

#include <boost/test/unit_test.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(url_category_)

    BOOST_AUTO_TEST_CASE(unknown_error)
    {
        std::error_code ec { -1, url_category() };

        BOOST_CHECK_EQUAL(ec.message(), "unknown error: -1");
    }

    BOOST_AUTO_TEST_CASE(name)
    {
        BOOST_CHECK_EQUAL(url_category().name(), "URL");
    }

BOOST_AUTO_TEST_SUITE_END() //

} // namespace whatwg::url::testing
