#include <boost/test/unit_test.hpp>

#include <whatwg/url/host.hpp>
#include <whatwg/url/error.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(host_)

BOOST_AUTO_TEST_SUITE(parser_)

    BOOST_AUTO_TEST_CASE(null)
    {
        url::host_t host;
        BOOST_TEST(host.is_null());
    }

    BOOST_AUTO_TEST_CASE(empty_string)
    {
        url::host_t host;
        host.set_empty_string();

        BOOST_TEST(host.is_empty());
    }

    BOOST_AUTO_TEST_CASE(domain)
    {
        auto const host = url::parse_host(U"www.google.com");
        BOOST_REQUIRE(!!host);
        BOOST_REQUIRE(host->is_domain());
        BOOST_TEST(host->domain() == "www.google.com");
    }

    BOOST_AUTO_TEST_CASE(ipv4_address)
    {
        auto const host = url::parse_host(U"255.255.255.255");
        BOOST_REQUIRE(!!host);
        BOOST_REQUIRE(host->is_ipv4_address());
        BOOST_CHECK(host->ipv4_address() == 0xffffffff);
    }

    BOOST_AUTO_TEST_CASE(ipv6_address)
    {
        auto const host = url::parse_host(U"[::1]");
        BOOST_REQUIRE(!!host);
        BOOST_REQUIRE(host->is_ipv6_address());

        url::ipv6_address_t expected { 0, 0, 0, 0, 0, 0, 0, 1 };

        auto const& addr = host->ipv6_address();
        BOOST_CHECK(addr == expected);
    }

    BOOST_AUTO_TEST_CASE(opaque_host)
    {
        auto const host = url::parse_host(U"opaque_host", true);
        BOOST_REQUIRE(!!host);
        BOOST_REQUIRE(host->is_opaque_host());
        BOOST_CHECK(host->opaque_host() == "opaque_host");
    }

BOOST_AUTO_TEST_SUITE_END() // parser_

BOOST_AUTO_TEST_SUITE(serializer_)

    void test_ipv6(url::ipv6_address_t const& addr,
                   url::byte_sequence_view_t const expected)
    {
        BOOST_TEST(url::serialize_ipv6(addr) == expected);
    }

    void test_host(url::string_view_t const input,
                   url::byte_sequence_view_t const expected,
                   bool const is_not_special = false)
    {
        auto const& host = url::parse_host(input, is_not_special);
        BOOST_REQUIRE(host);
        BOOST_TEST(url::serialize_host(*host) == expected);
    }

    BOOST_AUTO_TEST_CASE(ipv4)
    {
        BOOST_TEST(url::serialize_ipv4(0x00000000) == "0.0.0.0");
        BOOST_TEST(url::serialize_ipv4(0x10101010) == "16.16.16.16");
        BOOST_TEST(url::serialize_ipv4(0xffffffff) == "255.255.255.255");
        BOOST_TEST(url::serialize_ipv4(0xff00ff00) == "255.0.255.0");
    }

    BOOST_AUTO_TEST_CASE(ipv6)
    {
        test_ipv6({ 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8 }, "1:2:3:4:5:6:7:8");
        test_ipv6({ 0x0, 0x0, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8 }, "::3:4:5:6:7:8");
        test_ipv6({ 0x0, 0x0, 0x0, 0x4, 0x5, 0x6, 0x7, 0x8 }, "::4:5:6:7:8");
        test_ipv6({ 0x0, 0x0, 0x0, 0x0, 0x5, 0x6, 0x7, 0x8 }, "::5:6:7:8");
        test_ipv6({ 0x0, 0x0, 0x0, 0x0, 0x0, 0x6, 0x7, 0x8 }, "::6:7:8");
        test_ipv6({ 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x7, 0x8 }, "::7:8");
        test_ipv6({ 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8 }, "::8");
        test_ipv6({ 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 }, "::");
        test_ipv6({ 0x1, 0x0, 0x0, 0x4, 0x5, 0x6, 0x7, 0x8 }, "1::4:5:6:7:8");
        test_ipv6({ 0x1, 0x0, 0x0, 0x0, 0x5, 0x6, 0x7, 0x8 }, "1::5:6:7:8");
        test_ipv6({ 0x1, 0x0, 0x0, 0x0, 0x0, 0x6, 0x7, 0x8 }, "1::6:7:8");
        test_ipv6({ 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x7, 0x8 }, "1::7:8");
        test_ipv6({ 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8 }, "1::8");
        test_ipv6({ 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 }, "1::");
        test_ipv6({ 0x1, 0x2, 0x0, 0x0, 0x5, 0x6, 0x7, 0x8 }, "1:2::5:6:7:8");
        test_ipv6({ 0x1, 0x2, 0x0, 0x0, 0x0, 0x6, 0x7, 0x8 }, "1:2::6:7:8");
        test_ipv6({ 0x1, 0x2, 0x0, 0x0, 0x0, 0x0, 0x7, 0x8 }, "1:2::7:8");
        test_ipv6({ 0x1, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8 }, "1:2::8");
        test_ipv6({ 0x1, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 }, "1:2::");
        test_ipv6({ 0x1, 0x2, 0x3, 0x0, 0x0, 0x6, 0x7, 0x8 }, "1:2:3::6:7:8");
        test_ipv6({ 0x1, 0x2, 0x3, 0x0, 0x0, 0x0, 0x7, 0x8 }, "1:2:3::7:8");
        test_ipv6({ 0x1, 0x2, 0x3, 0x0, 0x0, 0x0, 0x0, 0x8 }, "1:2:3::8");
        test_ipv6({ 0x1, 0x2, 0x3, 0x0, 0x0, 0x0, 0x0, 0x0 }, "1:2:3::");
        test_ipv6({ 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x7, 0x8 }, "1:2:3:4::7:8");
        test_ipv6({ 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x8 }, "1:2:3:4::8");
        test_ipv6({ 0x1, 0x2, 0x3, 0x4, 0x0, 0x0, 0x0, 0x0 }, "1:2:3:4::");
        test_ipv6({ 0x1, 0x2, 0x3, 0x4, 0x5, 0x0, 0x0, 0x8 }, "1:2:3:4:5::8");
        test_ipv6({ 0x1, 0x2, 0x3, 0x4, 0x5, 0x0, 0x0, 0x0 }, "1:2:3:4:5::");
        test_ipv6({ 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x0, 0x0 }, "1:2:3:4:5:6::");
        test_ipv6({ 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xffff }, "::ffff");
    }

    BOOST_AUTO_TEST_CASE(host)
    {
        test_host(U"", "", true);
        test_host(U"localhost", "localhost");
        test_host(U"www.google.com", "www.google.com");
        test_host(U"192.168.1.1", "192.168.1.1");
        test_host(U"[::1]", "[::1]");
        test_host(U"[::127.0.0.1]", "[::7f00:1]");
    }


BOOST_AUTO_TEST_SUITE_END() // serializer_

BOOST_AUTO_TEST_SUITE_END() // host_

} // namespace whatwg::url::testing

