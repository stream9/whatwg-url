#include <whatwg/url/basic_url_parser.hpp>
#include <whatwg/url/url_rendering.hpp>
#include <whatwg/url/url_serializing.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(url_rendering_)

    BOOST_AUTO_TEST_CASE(basic)
    {
        auto const u1 =
            basic_url_parse("scheme://host:1234/path?query#fragment");
        BOOST_REQUIRE(u1);

        BOOST_CHECK_EQUAL(
            render_url(*u1), "scheme://host:1234/path?query#fragment");
    }

    BOOST_AUTO_TEST_CASE(credential_has_to_be_omited)
    {
        auto const u1 = basic_url_parse(
            "scheme://user:pass@host/"
        );
        BOOST_REQUIRE(u1);

        BOOST_CHECK_EQUAL(render_url(*u1),
            "scheme://host/"
        );
    }

    BOOST_AUTO_TEST_CASE(idn)
    {
        auto const u1 = basic_url_parse(
            "http://味の素.jp/"
        );
        BOOST_REQUIRE(u1);

        BOOST_CHECK_EQUAL(serialize_url(*u1),
            "http://xn--u9j479hu21a.jp/"
        );
        BOOST_CHECK_EQUAL(render_url(*u1),
            "http://味の素.jp/"
        );
    }

    BOOST_AUTO_TEST_CASE(file_without_host)
    {
        auto const u1 = basic_url_parse(
            "file:/dir/file.ext"
        );
        BOOST_REQUIRE(u1);

        BOOST_CHECK_EQUAL(render_url(*u1),
            "file:///dir/file.ext"
        );
    }

    BOOST_AUTO_TEST_CASE(non_hierarchical_path)
    {
        auto const u1 = basic_url_parse(
            "mailto:%E5%91%B3%E3%81%AE%E7%B4%A0@aol.com"
        );
        BOOST_REQUIRE(u1);

        BOOST_CHECK_EQUAL(render_url(*u1),
            "mailto:味の素@aol.com"
        );
    }

    BOOST_AUTO_TEST_CASE(hierarchical_path)
    {
        auto const u1 = basic_url_parse(
            "http://host/%E5%91%B3%E3%81%AE%E7%B4%A0/file.txt"
        );
        BOOST_REQUIRE(u1);

        BOOST_CHECK_EQUAL(render_url(*u1),
            "http://host/味の素/file.txt"
        );
    }

    BOOST_AUTO_TEST_CASE(percent_encoded_query)
    {
        auto const u1 = basic_url_parse(
            "http://host/path?q=%E5%91%B3%E3%81%AE%E7%B4%A0"
        );
        BOOST_REQUIRE(u1);

        BOOST_CHECK_EQUAL(render_url(*u1),
            "http://host/path?q=味の素"
        );
    }

    BOOST_AUTO_TEST_CASE(percent_encoded_fragment)
    {
        auto const u1 = basic_url_parse(
            "http://host/path#%E5%91%B3%E3%81%AE%E7%B4%A0"
        );
        BOOST_REQUIRE(u1);

        BOOST_CHECK_EQUAL(render_url(*u1),
            "http://host/path#味の素"
        );
    }

BOOST_AUTO_TEST_SUITE_END() // url_equivalence_

} // namespace whatwg::url::testing
