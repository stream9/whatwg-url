#include <whatwg/url/basic_url_parser.hpp>
#include <whatwg/url/origin.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(origin_)

    BOOST_AUTO_TEST_CASE(file_url)
    {
        auto const u = basic_url_parse("file:///dir/file.ext");
        BOOST_REQUIRE(u);

        origin o { *u };

        BOOST_CHECK(o.is_opaque());
    }

    BOOST_AUTO_TEST_CASE(blob_normal)
    {
        auto const u = basic_url_parse("blob:http://www.google.com:8080");
        BOOST_REQUIRE(u);

        origin o { *u };

        BOOST_CHECK_EQUAL(o.scheme(), "http");
        BOOST_CHECK_EQUAL(o.host(), "www.google.com");
        BOOST_CHECK_EQUAL(o.port(), "8080");
        BOOST_CHECK(!o.domain());
        BOOST_CHECK(!o.is_opaque());
    }

    BOOST_AUTO_TEST_CASE(blob_opaque_on_failure)
    {
        auto const u = basic_url_parse("blob://invalid.com");
        BOOST_REQUIRE(u);

        origin o { *u };

        BOOST_CHECK(o.is_opaque());
    }

BOOST_AUTO_TEST_SUITE_END() // origin_

} // namespace whatwg::url::testing
