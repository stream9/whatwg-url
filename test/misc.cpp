#include <whatwg/url/misc.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(default_port_)

    BOOST_AUTO_TEST_CASE(file)
    {
        BOOST_CHECK(!default_port("file"));
    }

BOOST_AUTO_TEST_SUITE_END() // misc_

} // namespace whatwg::url::testing
