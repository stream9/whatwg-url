#include <boost/test/unit_test.hpp>

#include <whatwg/url/url.hpp>

namespace whatwg::url::testing {

BOOST_AUTO_TEST_SUITE(url_)

    BOOST_AUTO_TEST_CASE(type_error_)
    {
        type_error e1;
        BOOST_CHECK_EQUAL(e1.what(), "type_error");

        type_error e2 { "foo" };
        BOOST_CHECK_EQUAL(e2.what(), "foo");
    }

    BOOST_AUTO_TEST_CASE(constructor_base_error)
    {
        auto construct = []() {
                url u { "foo.html", "//schemeless.com" };
            };

        BOOST_CHECK_THROW(
            construct(),
            type_error
        );
    }

    BOOST_AUTO_TEST_CASE(href_setter)
    {
        url u { "http://www.google.com" };

        u.href("http://www.altavista.com?q=google");

        BOOST_CHECK_EQUAL(u.href(), "http://www.altavista.com/?q=google");
    }

    BOOST_AUTO_TEST_CASE(href_setter_error)
    {
        url u { "http://www.google.com" };

        BOOST_CHECK_THROW(
            u.href("//error.com"),
            type_error
        );
    }

BOOST_AUTO_TEST_SUITE_END() // string_

} // namespace whatwg::url::testing

