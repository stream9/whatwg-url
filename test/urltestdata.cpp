#include "urltestdata.hpp"

#include <filesystem>
#include <iostream>

#include <boost/test/unit_test.hpp>

#include <json/json.hpp>
#include <json/array.hpp>
#include <json/value.hpp>
#include <json/object.hpp>

#include <whatwg/url/url.hpp>

#include "url_test.hpp"

namespace tst = boost::unit_test;
namespace fs = std::filesystem;
namespace url = whatwg::url;

namespace urltestdata {

namespace url = whatwg::url;

static bool
is_supposed_to_failure(json::object const& obj)
{
    if (!obj.contain("failure")) return false;

    auto const& v = obj.at("failure");
    if (!v.is_bool()) return false;

    return v.get_bool();
}

static void
check_attributes(url::url const& u, json::object const& obj)
{
    if (obj.contain("href")) {
        if (u.href() != obj.at("href").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.href() == obj.at("href").get_string());
    }

    if (obj.contain("origin")) {
        if (u.origin() != obj.at("origin").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.origin() == obj.at("origin").get_string());
    }

    if (obj.contain("protocol")) {
        if (u.protocol() != obj.at("protocol").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.protocol() == obj.at("protocol").get_string());
    }

    if (obj.contain("username")) {
        if (u.username() != obj.at("username").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.username() == obj.at("username").get_string());
    }

    if (obj.contain("password")) {
        if (u.password() != obj.at("password").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.password() == obj.at("password").get_string());
    }

    if (obj.contain("host")) {
        if (u.host() != obj.at("host").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.host() == obj.at("host").get_string());
    }

    if (obj.contain("hostname")) {
        if (u.hostname() != obj.at("hostname").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.hostname() == obj.at("hostname").get_string());
    }

    if (obj.contain("port")) {
        if (u.port() != obj.at("port").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.port() == obj.at("port").get_string());
    }

    if (obj.contain("pathname")) {
        if (u.pathname() != obj.at("pathname").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.pathname() == obj.at("pathname").get_string());
    }

    if (obj.contain("search")) {
        if (u.search() != obj.at("search").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.search() == obj.at("search").get_string());
    }

    if (obj.contain("hash")) {
        if (u.hash() != obj.at("hash").get_string()) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_TEST(u.hash() == obj.at("hash").get_string());
    }
}

static void
test_main(json::object const& obj)
{
    auto const& input = obj.at("input").get_string();
    auto const& base = obj.at("base").get_string();

    auto const failure = is_supposed_to_failure(obj);

    try {
        url::url u { input, base };

        if (failure) {
            std::cout << json::stringify(obj) << "\n";
        }
        BOOST_REQUIRE(!failure);

        check_attributes(u, obj);
    }
    catch (url::type_error const&) {
        if (!failure) {
            std::cout << json::stringify(obj) << "\n";
            throw;
        }
    }

    if (failure) {
        auto input_with_blank = [&]() {
            url::url u { input, "about:blank" };
        };

        BOOST_REQUIRE_THROW(
            input_with_blank(),
            url::type_error
        );
    }
}

bool
init_test_suite(tst::test_suite& parent)
{
    fs::path dir { DATA_DIRECTORY };
    if (!exists(dir)) {
        std::cerr << "data directory doesn't exists";
        return false;
    }

    auto const& path = dir / "urltestdata.json";
    if (!exists(dir)) {
        return false;
    }

    auto* const suite = BOOST_TEST_SUITE(path.stem().c_str());
    assert(suite);

    auto const& test_data = slurp_file(path);

    auto const& array = json::parse(test_data).get_array();

    auto n = 0;
    for (auto const& v: array) {
        if (!v.is_object()) continue;

        auto obj = v.get_object();
        auto const name = "no." + std::to_string(++n);

        auto case_ = new tst::test_case(
            name.data(),
            __FILE__, __LINE__,
            [obj]() { test_main(obj); }
        );

        suite->add(case_);
    }

    parent.add(suite);

    return true;
}

} // namespace urltestdata
