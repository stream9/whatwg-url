#ifndef URL_TEST_TOASCII_HPP
#define URL_TEST_TOASCII_HPP

#include <boost/test/unit_test.hpp>

namespace toascii_test {

bool init_test_suite(boost::unit_test::test_suite& parent);

} // namespace toascii

#endif // URL_TEST_TOASCII_HPP
