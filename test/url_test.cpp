#include "url_test.hpp"

#include <fstream>
#include <sstream>
#include <cassert>

std::string
slurp_file(std::filesystem::path const& path)
{
    std::ifstream ifs { path };
    assert(ifs.is_open());

    std::ostringstream oss;
    oss << ifs.rdbuf();

    return oss.str();
}
