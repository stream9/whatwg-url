#include "urltestdata.hpp"
#include "setters_tests.hpp"
#include "toascii.hpp"

#include <iostream>

#include <boost/test/unit_test.hpp>

namespace tst = boost::unit_test;

static bool
init_test_suite()
{
    auto& master = tst::framework::master_test_suite();

    if (!urltestdata::init_test_suite(master)) {
        std::cerr << "fail to initialize urltestdata test suite";
        return false;
    }

    if (!setters_tests::init_test_suite(master)) {
        std::cerr << "fail to initialize setters_tests test suite";
        return false;
    }

    if (!toascii_test::init_test_suite(master)) {
        std::cerr << "fail to initialize setters_tests test suite";
        return false;
    }

    return true;
}

int main(int argc, char* argv[])
{
    return tst::unit_test_main(&init_test_suite, argc, argv);
}
