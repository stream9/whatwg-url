#ifndef URL_TEST_URLTESTDATA_HPP
#define URL_TEST_URLTESTDATA_HPP

#include <boost/test/unit_test.hpp>

namespace urltestdata {

bool init_test_suite(boost::unit_test::test_suite&);

} // namespace urltestdata


#endif // URL_TEST_URLTESTDATA_HPP
