#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/url_search_params.hpp>

using namespace std::literals;

namespace url = whatwg::url;

BOOST_AUTO_TEST_SUITE(url_search_params_)

// based on https://github.com/web-platform-tests/wpt/blob/master/url/urlsearchparams-get.html
BOOST_AUTO_TEST_SUITE(get_)

    BOOST_AUTO_TEST_CASE(basics_1)
    {
        url::url_search_params const params { "a=b&c=d" };

        BOOST_CHECK_EQUAL(params.get("a"), "b");
        BOOST_CHECK_EQUAL(params.get("c"), "d");
        BOOST_CHECK_EQUAL(params.get("e"), std::nullopt);
    }

    BOOST_AUTO_TEST_CASE(basics_2)
    {
        url::url_search_params const params { "a=b&c=d&a=e" };

        BOOST_CHECK_EQUAL(params.get("a"), "b");
    }

    BOOST_AUTO_TEST_CASE(basics_3)
    {
        url::url_search_params const params { "=b&c=d" };

        BOOST_CHECK_EQUAL(params.get(""), "b");
    }

    BOOST_AUTO_TEST_CASE(basics_4)
    {
        url::url_search_params const params { "a=&c=d&a=e" };

        BOOST_CHECK_EQUAL(params.get("a"), "");
    }

    BOOST_AUTO_TEST_CASE(basics_5)
    {
        url::url_search_params const params { "first=second&third&&" };

        BOOST_CHECK(params.has("first"));
        BOOST_CHECK_EQUAL(params.get("first"), "second");
        BOOST_CHECK_EQUAL(params.get("third"), "");
        BOOST_CHECK_EQUAL(params.get("fourth"), std::nullopt);
    }

BOOST_AUTO_TEST_SUITE_END() // get_

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
