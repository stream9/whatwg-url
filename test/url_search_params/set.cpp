#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/url_search_params.hpp>

using namespace std::literals;

namespace url = whatwg::url;

BOOST_AUTO_TEST_SUITE(url_search_params_)

// based on https://github.com/web-platform-tests/wpt/blob/master/url/urlsearchparams-set.html
BOOST_AUTO_TEST_SUITE(set_)

    BOOST_AUTO_TEST_CASE(basics_1)
    {
        url::url_search_params params { "a=b&c=d" };

        params.set("a", "B");
        BOOST_CHECK_EQUAL(params.to_string(), "a=B&c=d");
    }

    BOOST_AUTO_TEST_CASE(basics_2)
    {
        url::url_search_params params { "a=b&c=d&a=e" };

        params.set("a", "B");
        BOOST_CHECK_EQUAL(params.to_string(), "a=B&c=d");

        params.set("e", "f");
        BOOST_CHECK_EQUAL(params.to_string(), "a=B&c=d&e=f");
    }

    BOOST_AUTO_TEST_CASE(basics_3)
    {
        url::url_search_params params { "a=1&a=2&a=3" };

        BOOST_REQUIRE(params.has("a"));
        BOOST_CHECK_EQUAL(params.get("a"), "1");

        params.set("first", "4");
        BOOST_REQUIRE(params.has("a"));
        BOOST_CHECK_EQUAL(params.get("a"), "1");

        params.set("a", "4");
        BOOST_REQUIRE(params.has("a"));
        BOOST_CHECK_EQUAL(params.get("a"), "4");
    }

BOOST_AUTO_TEST_SUITE_END() // set_

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
