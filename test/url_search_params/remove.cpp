#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/url.hpp>
#include <whatwg/url/url_search_params.hpp>

using namespace std::literals;

namespace url = whatwg::url;

BOOST_AUTO_TEST_SUITE(url_search_params_)

// base on https://github.com/web-platform-tests/wpt/blob/master/url/urlsearchparams-delete.html
BOOST_AUTO_TEST_SUITE(remove_)

    BOOST_AUTO_TEST_CASE(basic_1)
    {
        url::url_search_params params { "a=b&c=d" };

        params.remove("a");
        BOOST_CHECK_EQUAL(params.to_string(), "c=d");
    }

    BOOST_AUTO_TEST_CASE(basic_2)
    {
        url::url_search_params params { "a=a&b=b&a=a&c=c" };

        params.remove("a");
        BOOST_CHECK_EQUAL(params.to_string(), "b=b&c=c");
    }

    BOOST_AUTO_TEST_CASE(basic_3)
    {
        url::url_search_params params { "a=a&b=b&c=c" };

        params.remove("");
        BOOST_CHECK_EQUAL(params.to_string(), "a=a&b=b&c=c");
    }

    BOOST_AUTO_TEST_CASE(basic_4)
    {
        url::url_search_params params { "a=a&b=b&c=c" };

        params.remove("");
        BOOST_CHECK_EQUAL(params.to_string(), "a=a&b=b&c=c");
    }

    BOOST_AUTO_TEST_CASE(basic_5)
    {
        // it seems meaningless but for the sake of compatibility
        url::url_search_params params { "a=a&null=null&b=b" };

        params.remove("null");
        BOOST_CHECK_EQUAL(params.to_string(), "a=a&b=b");
    }

    BOOST_AUTO_TEST_CASE(basic_6)
    {
        // it seems meaningless but for the sake of compatibility
        url::url_search_params params { "a=a&undefined=undefined&b=b" };

        params.remove("undefined");
        BOOST_CHECK_EQUAL(params.to_string(), "a=a&b=b");
    }

    BOOST_AUTO_TEST_CASE(multiply_appended_items)
    {
        url::url_search_params params;

        params.append("first", "1");
        BOOST_REQUIRE(params.has("first"));
        BOOST_CHECK_EQUAL(params.get("first"), "1");

        params.remove("first");
        BOOST_REQUIRE(!params.has("first"));

        params.append("first", "1");
        params.append("first", "10");
        params.remove("first");
        BOOST_REQUIRE(!params.has("first"));
    }

    BOOST_AUTO_TEST_CASE(remove_all_remove_question_mark_too)
    {
        url::url u { "http://example.com/?param1&param2" };

        u.search_params().remove("param1");
        u.search_params().remove("param2");
        BOOST_CHECK_EQUAL(u.href(), "http://example.com/");
        BOOST_CHECK_EQUAL(u.search(), "");
    }

    BOOST_AUTO_TEST_CASE(remove_non_existent_param_remove_question_mark_too)
    {
        url::url u { "http://example.com/?" };

        u.search_params().remove("param1");
        BOOST_CHECK_EQUAL(u.href(), "http://example.com/");
        BOOST_CHECK_EQUAL(u.search(), "");
    }
BOOST_AUTO_TEST_SUITE_END() // delete_

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
