#include <whatwg/url/url_search_params.hpp>

#include "utility.hpp"

#include <boost/test/unit_test.hpp>

using namespace std::literals;

namespace url = whatwg::url;

BOOST_AUTO_TEST_SUITE(url_search_params_)

// base-on https://github.com/web-platform-tests/wpt/blob/master/url/urlsearchparams-constructor.html
BOOST_AUTO_TEST_SUITE(construction)

    BOOST_AUTO_TEST_CASE(basic_construction_1)
    {
        url::url_search_params const params;

        BOOST_TEST(params.to_string() == "");
    }

    BOOST_AUTO_TEST_CASE(basic_construction_2)
    {
        url::url_search_params const params { "" };

        BOOST_TEST(params.to_string() == "");
    }

    BOOST_AUTO_TEST_CASE(basic_construction_3)
    {
        url::url_search_params const params { "a=b" };

        BOOST_TEST(params.to_string() == "a=b");

        auto const copy = url::url_search_params(params);

        BOOST_TEST(copy.to_string() == "a=b");
    }

    BOOST_AUTO_TEST_CASE(string_1)
    {
        url::url_search_params const params { "a=b" };

        BOOST_CHECK(params.has("a"));
        BOOST_CHECK(!params.has("b"));
    }

    BOOST_AUTO_TEST_CASE(string_2)
    {
        url::url_search_params const params { "a=b&c" };

        BOOST_CHECK(params.has("a"));
        BOOST_CHECK(params.has("c"));
    }

    BOOST_AUTO_TEST_CASE(string_3)
    {
        url::url_search_params const params {
                                "&a&&& &&&&&a+b=& c&m%c3%b8%c3%b8" };

        BOOST_CHECK(params.has("a"));
        BOOST_CHECK(params.has("a b"));
        BOOST_CHECK(params.has(" "));
        BOOST_CHECK(!params.has("c"));
        BOOST_CHECK(params.has(" c"));
        BOOST_CHECK(params.has("møø"));
    }

    BOOST_AUTO_TEST_CASE(object_1)
    {
        url::url_search_params seed { "a=b&c=d" };
        url::url_search_params params { seed };

        BOOST_CHECK_EQUAL(params.get("a"), "b");
        BOOST_CHECK_EQUAL(params.get("c"), "d");

        seed.append("e", "f");
        BOOST_CHECK(!params.has("e"));
        params.append("g", "h");
        BOOST_CHECK(!seed.has("g"));
    }

    BOOST_AUTO_TEST_CASE(plus_1)
    {
        url::url_search_params const params { "a=b+c" };

        BOOST_CHECK_EQUAL(params.get("a"), "b c");
    }

    BOOST_AUTO_TEST_CASE(plus_2)
    {
        url::url_search_params const params { "a+b=c" };

        BOOST_CHECK_EQUAL(params.get("a b"), "c");
    }

    BOOST_AUTO_TEST_CASE(encoded_plus)
    {
        auto const test_value = "+15555555555";
        url::url_search_params params;
        params.set("query", test_value);

        url::url_search_params new_params { params.to_string() };

        BOOST_CHECK_EQUAL(params.to_string(), "query=%2B15555555555");

        BOOST_CHECK_EQUAL(params.get("query"), test_value);
        BOOST_CHECK_EQUAL(new_params.get("query"), test_value);
    }

    BOOST_AUTO_TEST_CASE(space_1)
    {
        url::url_search_params const params { "a=b c" };

        BOOST_CHECK_EQUAL(params.get("a"), "b c");
    }

    BOOST_AUTO_TEST_CASE(space_2)
    {
        url::url_search_params const params { "a b=c" };

        BOOST_CHECK_EQUAL(params.get("a b"), "c");
    }

    BOOST_AUTO_TEST_CASE(encoded_space_1)
    {
        url::url_search_params const params { "a=b%20c" };

        BOOST_CHECK_EQUAL(params.get("a"), "b c");
    }

    BOOST_AUTO_TEST_CASE(encoded_space_2)
    {
        url::url_search_params const params { "a%20b=c" };

        BOOST_CHECK_EQUAL(params.get("a b"), "c");
    }

    BOOST_AUTO_TEST_CASE(null_1)
    {
        url::url_search_params const params { "a=b\0c"sv };

        BOOST_CHECK_EQUAL(params.get("a"), "b\0c"sv);
    }

    BOOST_AUTO_TEST_CASE(null_2)
    {
        url::url_search_params const params { "a\0b=c"sv };

        BOOST_CHECK_EQUAL(params.get("a\0b"sv), "c");
    }

    BOOST_AUTO_TEST_CASE(encoded_null_1)
    {
        url::url_search_params const params { "a=b%00c" };

        BOOST_CHECK_EQUAL(params.get("a"), "b\0c"sv);
    }

    BOOST_AUTO_TEST_CASE(encoded_null_2)
    {
        url::url_search_params const params { "a%00b=c" };

        BOOST_CHECK_EQUAL(params.get("a\0b"sv), "c");
    }

    BOOST_AUTO_TEST_CASE(unicode_composition_symbol_1)
    {
        url::url_search_params const params { "a=b\u2384" };

        BOOST_CHECK_EQUAL(params.get("a"), "b\u2384");
    }

    BOOST_AUTO_TEST_CASE(unicode_composition_symbol_2)
    {
        url::url_search_params const params { "a\u2384b=c" };

        BOOST_CHECK_EQUAL(params.get("a\u2384b"), "c");
    }

    BOOST_AUTO_TEST_CASE(encoded_composition_symbol_1)
    {
        url::url_search_params const params { "a=b%e2%8e%84" };

        BOOST_CHECK_EQUAL(params.get("a"), "b\u2384");
    }

    BOOST_AUTO_TEST_CASE(encoded_composition_symbol_2)
    {
        url::url_search_params const params { "a%e2%8e%84b=c" };

        BOOST_CHECK_EQUAL(params.get("a\u2384b"), "c");
    }

    BOOST_AUTO_TEST_CASE(pile_of_poo_1)
    {
        url::url_search_params const params { "a=b\U0001f4a9c" };

        BOOST_CHECK_EQUAL(params.get("a"), "b\U0001f4a9c");
    }

    BOOST_AUTO_TEST_CASE(pile_of_poo_2)
    {
        url::url_search_params const params { "a\U0001f4a9=c" };

        BOOST_CHECK_EQUAL(params.get("a\U0001f4a9"), "c");
    }

    BOOST_AUTO_TEST_CASE(encoded_pile_of_poo_1)
    {
        url::url_search_params const params { "a=b%f0%9f%92%a9c" };

        BOOST_CHECK_EQUAL(params.get("a"), "b\U0001f4a9c");
    }

    BOOST_AUTO_TEST_CASE(encoded_pile_of_poo_2)
    {
        url::url_search_params const params { "a%f0%9f%92%a9b=c" };

        BOOST_CHECK_EQUAL(params.get("a\U0001f4a9b"), "c");
    }

    BOOST_AUTO_TEST_CASE(vector_of_key_value_pair)
    {
        std::vector<std::pair<url::byte_sequence_t, url::byte_sequence_t>> const seq {
            { "a", "b" }, { "c", "d" }
        };
        url::url_search_params const params { seq };

        BOOST_CHECK_EQUAL(params.get("a"), "b");
        BOOST_CHECK_EQUAL(params.get("c"), "d");
    }

    BOOST_AUTO_TEST_CASE(map)
    {
        std::map<url::byte_sequence_t, url::byte_sequence_t> const m {
            { "a", "b" }, { "c", "d" }
        };
        url::url_search_params const params { m };

        BOOST_CHECK_EQUAL(params.get("a"), "b");
        BOOST_CHECK_EQUAL(params.get("c"), "d");
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
