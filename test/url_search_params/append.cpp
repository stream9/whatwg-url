#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/url_search_params.hpp>

using namespace std::literals;

namespace url = whatwg::url;

BOOST_AUTO_TEST_SUITE(url_search_params_)

// base on https://github.com/web-platform-tests/wpt/blob/master/url/urlsearchparams-append.html
BOOST_AUTO_TEST_SUITE(append)

    BOOST_AUTO_TEST_CASE(same_name)
    {
        url::url_search_params params;

        params.append("a", "b");
        BOOST_CHECK_EQUAL(params.to_string(), "a=b");
        params.append("a", "b");
        BOOST_CHECK_EQUAL(params.to_string(), "a=b&a=b");
        params.append("a", "c");
        BOOST_CHECK_EQUAL(params.to_string(), "a=b&a=b&a=c");
    }

    BOOST_AUTO_TEST_CASE(empty_string)
    {
        url::url_search_params params;

        params.append("", "");
        BOOST_CHECK_EQUAL(params.to_string(), "=");
        params.append("", "");
        BOOST_CHECK_EQUAL(params.to_string(), "=&=");
    }

    BOOST_AUTO_TEST_CASE(multiple)
    {
        url::url_search_params params;

        params.append("first", "1");
        params.append("second", "2");
        params.append("third", "");
        params.append("first", "10");

        BOOST_REQUIRE(params.has("first"));
        BOOST_CHECK_EQUAL(params.get("first"), "1");
        BOOST_CHECK_EQUAL(params.get("second"), "2");
        BOOST_CHECK_EQUAL(params.get("third"), "");

        params.append("first", "10");
        BOOST_CHECK_EQUAL(params.get("first"), "1");
    }

BOOST_AUTO_TEST_SUITE_END() // append

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
