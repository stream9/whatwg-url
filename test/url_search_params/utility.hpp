#ifndef URL_TEST_URL_SEARCH_PARAMS_UTILITY_HPP
#define URL_TEST_URL_SEARCH_PARAMS_UTILITY_HPP

#include <optional>
#include <ostream>
#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

inline bool
operator==(std::optional<std::string> const& lhs, std::string_view const rhs)
{
    return lhs && *lhs == rhs;
}

template<typename Range1T>
void
check_equal_range(Range1T const& lhs,
                  std::initializer_list<std::string_view> const& rhs)
{
    BOOST_CHECK_EQUAL_COLLECTIONS(
        lhs.begin(), lhs.end(),
        rhs.begin(), rhs.end()
    );
}

namespace std {

/*
 * !!!! Caution: illegal hack !!!!
 */
template<typename T>
std::ostream&
operator<<(std::ostream& os, std::optional<T> const& v)
{
    if (v) {
        os << *v;
    }
    else {
        os << "null";
    }

    return os;
}

inline std::ostream&
operator<<(std::ostream& os, std::nullopt_t)
{
    os << "null";
    return os;
}

} // namespace std

#endif // URL_TEST_URL_SEARCH_PARAMS_UTILITY_HPP
