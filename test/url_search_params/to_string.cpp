#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/url.hpp>
#include <whatwg/url/url_search_params.hpp>

using namespace std::literals;

namespace url = whatwg::url;

BOOST_AUTO_TEST_SUITE(url_search_params_)

BOOST_AUTO_TEST_SUITE(to_string_)

    BOOST_AUTO_TEST_CASE(space)
    {
        url::url_search_params params;

        params.append("a", "b c");
        BOOST_CHECK_EQUAL(params.to_string(), "a=b+c");

        params.remove("a");
        params.append("a b", "c");
        BOOST_CHECK_EQUAL(params.to_string(), "a+b=c");
    }

    BOOST_AUTO_TEST_CASE(empty_value)
    {
        url::url_search_params params;

        params.append("a", "");
        BOOST_CHECK_EQUAL(params.to_string(), "a=");
        params.append("a", "");
        BOOST_CHECK_EQUAL(params.to_string(), "a=&a=");
        params.append("", "b");
        BOOST_CHECK_EQUAL(params.to_string(), "a=&a=&=b");
        params.append("", "");
        BOOST_CHECK_EQUAL(params.to_string(), "a=&a=&=b&=");
        params.append("", "");
        BOOST_CHECK_EQUAL(params.to_string(), "a=&a=&=b&=&=");
    }

    BOOST_AUTO_TEST_CASE(empty_name)
    {
        url::url_search_params params;

        params.append("", "");
        BOOST_CHECK_EQUAL(params.to_string(), "=");
        params.append("", "");
        BOOST_CHECK_EQUAL(params.to_string(), "=&=");
    }

    BOOST_AUTO_TEST_CASE(plus)
    {
        url::url_search_params params;

        params.append("a", "b+c");
        BOOST_CHECK_EQUAL(params.to_string(), "a=b%2Bc");

        params.remove("a");
        params.append("a+b", "c");
        BOOST_CHECK_EQUAL(params.to_string(), "a%2Bb=c");
    }

    BOOST_AUTO_TEST_CASE(equal)
    {
        url::url_search_params params;

        params.append("=", "a");
        BOOST_CHECK_EQUAL(params.to_string(), "%3D=a");

        params.append("b", "=");
        BOOST_CHECK_EQUAL(params.to_string(), "%3D=a&b=%3D");
    }

    BOOST_AUTO_TEST_CASE(ampersand)
    {
        url::url_search_params params;

        params.append("&", "a");
        BOOST_CHECK_EQUAL(params.to_string(), "%26=a");

        params.append("b", "&");
        BOOST_CHECK_EQUAL(params.to_string(), "%26=a&b=%26");
    }

    BOOST_AUTO_TEST_CASE(asterisk_hyphen_period_underscore)
    {
        url::url_search_params params;

        params.append("a", "*-._");
        BOOST_CHECK_EQUAL(params.to_string(), "a=*-._");

        params.remove("a");
        params.append("*-._", "c");
        BOOST_CHECK_EQUAL(params.to_string(), "*-._=c");
    }

    BOOST_AUTO_TEST_CASE(percent)
    {
        url::url_search_params params;

        params.append("a", "b%c");
        BOOST_CHECK_EQUAL(params.to_string(), "a=b%25c");

        params.remove("a");
        params.append("a%b", "c");
        BOOST_CHECK_EQUAL(params.to_string(), "a%25b=c");
    }

    BOOST_AUTO_TEST_CASE(null_)
    {
        url::url_search_params params;

        params.append("a", "b\0c"sv);
        BOOST_CHECK_EQUAL(params.to_string(), "a=b%00c");

        params.remove("a");
        params.append("a\0b"sv, "c");
        BOOST_CHECK_EQUAL(params.to_string(), "a%00b=c");
    }

    BOOST_AUTO_TEST_CASE(pile_of_poo)
    {
        url::url_search_params params;

        params.append("a", "b\U0001F4A9c"sv);
        BOOST_CHECK_EQUAL(params.to_string(), "a=b%F0%9F%92%A9c");

        params.remove("a");
        params.append("a\U0001F4A9b"sv, "c");
        BOOST_CHECK_EQUAL(params.to_string(), "a%F0%9F%92%A9b=c");
    }

    BOOST_AUTO_TEST_CASE(connection_with_url)
    {
        url::url u { "http://www.example.com/?a=b,c" };
        auto& params = u.search_params();

        BOOST_CHECK_EQUAL(u.to_string(), "http://www.example.com/?a=b,c");
        BOOST_CHECK_EQUAL(params.to_string(), "a=b%2Cc");

        params.append("x", "y");
        BOOST_CHECK_EQUAL(u.to_string(), "http://www.example.com/?a=b%2Cc&x=y");
        BOOST_CHECK_EQUAL(params.to_string(), "a=b%2Cc&x=y");
    }

BOOST_AUTO_TEST_SUITE_END() // to_string_

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
