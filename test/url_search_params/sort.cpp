#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/url_search_params.hpp>

using namespace std::literals;

namespace url = whatwg::url;

using key_value_pair_t = std::pair<url::byte_sequence_t, url::byte_sequence_t>;
BOOST_TEST_DONT_PRINT_LOG_VALUE(key_value_pair_t);

static void
check_params(url::url_search_params const& params,
             std::initializer_list<key_value_pair_t> const& expected)
{
    BOOST_CHECK_EQUAL_COLLECTIONS(
        params.begin(), params.end(),
        expected.begin(), expected.end()
    );
}

BOOST_AUTO_TEST_SUITE(url_search_params_)

// based on https://github.com/web-platform-tests/wpt/blob/master/url/urlsearchparams-sort.html
BOOST_AUTO_TEST_SUITE(sort)

    BOOST_AUTO_TEST_CASE(sort_1)
    {
        url::url_search_params params { "z=b&a=b&z=a&a=a" };

        params.sort();

        check_params(params, {
            { "a", "b" }, { "a", "a" }, { "z", "b" }, { "z", "a" }
        });
    }

    BOOST_AUTO_TEST_CASE(sort_2)
    {
        url::url_search_params params { "\uFFFD=x&\uFFFC&\uFFFD=a" };

        params.sort();

        check_params(params, {
            { "\uFFFC", "" }, { "\uFFFD", "x" }, { "\uFFFD", "a" }
        });
    }

    BOOST_AUTO_TEST_CASE(sort_3)
    {
        url::url_search_params params { "é&e\uFFFD&e\u0301" };

        params.sort();

        check_params(params, {
            { "e\u0301", "" }, { "e\uFFFD", "" }, { "é", "" }
        });
    }

    BOOST_AUTO_TEST_CASE(sort_4)
    {
        url::url_search_params params {
            "z=z&a=a&z=y&a=b&z=x&a=c&z=w&a=d&z=v&a=e&z=u&a=f&z=t&a=g" };

        params.sort();

        check_params(params, {
            { "a", "a" }, { "a", "b" }, { "a", "c" }, { "a", "d" },
            { "a", "e" }, { "a", "f" }, { "a", "g" }, { "z", "z" },
            { "z", "y" }, { "z", "x" }, { "z", "w" }, { "z", "v" },
            { "z", "u" }, { "z", "t" }
        });
    }

BOOST_AUTO_TEST_SUITE_END() // sort

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
