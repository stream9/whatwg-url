#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/url_search_params.hpp>

using namespace std::literals;

namespace url = whatwg::url;

BOOST_AUTO_TEST_SUITE(url_search_params_)

// based on https://github.com/web-platform-tests/wpt/blob/master/url/urlsearchparams-has.html
BOOST_AUTO_TEST_SUITE(has_)

    BOOST_AUTO_TEST_CASE(basics_1)
    {
        url::url_search_params const params { "a=b&c=d" };

        BOOST_CHECK(params.has("a"));
        BOOST_CHECK(params.has("c"));
        BOOST_CHECK(!params.has("e"));
    }

    BOOST_AUTO_TEST_CASE(basics_2)
    {
        url::url_search_params const params { "a=b&c=d&a=e" };

        BOOST_CHECK(params.has("a"));
    }

    BOOST_AUTO_TEST_CASE(basics_3)
    {
        url::url_search_params const params { "=b&c=d" };

        BOOST_CHECK(params.has(""));
    }

    BOOST_AUTO_TEST_CASE(basics_4)
    {
        url::url_search_params const params { "null=a" };

        BOOST_CHECK(params.has("null"));
    }

    BOOST_AUTO_TEST_CASE(following_remove)
    {
        url::url_search_params params { "a=b&c=d&&" };

        params.append("first", "1");
        params.append("first", "2");

        BOOST_CHECK(params.has("a"));
        BOOST_CHECK(params.has("c"));
        BOOST_CHECK(params.has("first"));
        BOOST_CHECK(!params.has("d"));

        params.remove("first");
        BOOST_CHECK(!params.has("first"));
    }

BOOST_AUTO_TEST_SUITE_END() // has_

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
