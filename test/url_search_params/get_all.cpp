#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/url_search_params.hpp>

using namespace std::literals;

namespace url = whatwg::url;

BOOST_AUTO_TEST_SUITE(url_search_params_)

// based on https://github.com/web-platform-tests/wpt/blob/master/url/urlsearchparams-getall.html
BOOST_AUTO_TEST_SUITE(get_all)

    BOOST_AUTO_TEST_CASE(basics_1)
    {
        url::url_search_params const params { "a=b&c=d" };

        check_equal_range(params.get_all("a"), { "b" });
        check_equal_range(params.get_all("c"), { "d" });
        check_equal_range(params.get_all("e"), { });
    }

    BOOST_AUTO_TEST_CASE(basics_2)
    {
        url::url_search_params const params { "a=b&c=d&a=e" };

        check_equal_range(params.get_all("a"), { "b", "e" });
    }

    BOOST_AUTO_TEST_CASE(basics_3)
    {
        url::url_search_params const params { "=b&c=d" };

        check_equal_range(params.get_all(""), { "b" });
    }

    BOOST_AUTO_TEST_CASE(basics_4)
    {
        url::url_search_params const params { "a=&c=d&a=e" };

        check_equal_range(params.get_all("a"), { "", "e" });
    }

    BOOST_AUTO_TEST_CASE(multiple_1)
    {
        url::url_search_params params { "a=1&a=2&a=3&a" };

        BOOST_REQUIRE(params.has("a"));

        auto const& matches = params.get_all("a");
        BOOST_CHECK_EQUAL(matches.size(), 4);
        check_equal_range(matches, { "1", "2", "3", "" });

        params.set("a", "one");
        BOOST_CHECK_EQUAL(params.get("a"), "one");

        auto const& matches2 = params.get_all("a");
        BOOST_CHECK_EQUAL(matches2.size(), 1);
        check_equal_range(matches2, { "one" });
    }

BOOST_AUTO_TEST_SUITE_END() // get_all

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
