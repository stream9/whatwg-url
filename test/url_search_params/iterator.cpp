#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <whatwg/url/url.hpp>
#include <whatwg/url/url_search_params.hpp>

using namespace std::literals;

namespace url = whatwg::url;

BOOST_AUTO_TEST_SUITE(url_search_params_)

BOOST_AUTO_TEST_SUITE(iterator_)

    BOOST_AUTO_TEST_CASE(range_for)
    {
        url::url_search_params const params { "a=1&b=2&c=3" };

        std::vector<std::string_view> keys, values;

        for (auto const& [key, value]: params) {
            keys.push_back(key);
            values.push_back(value);
        }

        std::vector<std::string_view> const expected_keys {
            "a", "b", "c"
        };
        std::vector<std::string_view> const expected_values {
            "1", "2", "3"
        };

        BOOST_CHECK_EQUAL_COLLECTIONS(
            keys.begin(), keys.end(),
            expected_keys.begin(), expected_keys.end());
        BOOST_CHECK_EQUAL_COLLECTIONS(
            values.begin(), values.end(),
            expected_values.begin(), expected_values.end());
    }

    BOOST_AUTO_TEST_CASE(empty)
    {
        url::url const u { "http://a.b/c" };

        auto const& params = u.search_params();
        BOOST_CHECK((params.begin() == params.end()));
    }

BOOST_AUTO_TEST_SUITE_END() // iterator_

BOOST_AUTO_TEST_SUITE_END() // url_search_params_
